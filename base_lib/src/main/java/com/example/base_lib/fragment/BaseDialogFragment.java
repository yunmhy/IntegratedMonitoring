package com.example.base_lib.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import static android.widget.ListPopupWindow.MATCH_PARENT;
import static android.widget.ListPopupWindow.WRAP_CONTENT;

/**
 * @创建时间：2019/10/24 13:41
 * @描述：
 */
public abstract class BaseDialogFragment extends DialogFragment {
    private int mWidth = MATCH_PARENT;
    private int mHeight = WRAP_CONTENT;
    protected Context mContext;

    protected View roomView;
    protected boolean lowerBackground = false;// 是否降级背景，例如图片预览的时候不可以降级（设置Activity的透明度）
    protected Boolean touchOutside = false;
    protected int mAnimation = 0;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setStyle(Gravity.CENTER);
        roomView = inflater.inflate(getContentLayout(), container, false);
        initView(roomView);
        initData();
        initBeforeCreateView(savedInstanceState);
        return roomView;
    }

    protected abstract void initData();

    /**
     * 需要在onCreateView中调用的方法
     */
    protected void initBeforeCreateView(Bundle savedInstanceState) {

    }

    protected abstract void initView(View roomView);

    protected void setStyle(int gravity) {
        Dialog dialog = getDialog();
        //获取Window
        Window window = dialog.getWindow();
        //无标题
        dialog.requestWindowFeature(STYLE_NO_TITLE);
        setCancelable(touchOutside);
        dialog.setCanceledOnTouchOutside(touchOutside);
        // 透明背景
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        if (lowerBackground) window.setDimAmount(0F);
        else window.setDimAmount(0.5F);// 去除 dialog 弹出的阴影
//        dialog.setCanceledOnTouchOutside(touchOutside);
        //设置宽高
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams wlp = window.getAttributes();
        int width = wlp.width;
        int height = wlp.height;
        wlp.width=mWidth;
        wlp.height=mHeight;

        //设置对齐方式
        wlp.gravity = gravity;
//        wlp.y=180;
        if (mAnimation != 0) {
            window.setWindowAnimations(mAnimation);
        }
        window.setAttributes(wlp);
    }

    /**
     * 内容布局的ResId
     */
    protected abstract int getContentLayout();


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public int getAnimation() {
        return mAnimation;
    }

}
