package com.example.base_lib.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.base_lib.MyApplication;
import com.example.base_lib.activity.BaseActivity;
import com.tencent.mmkv.MMKV;

/**
 * @创建时间：2019/10/24 10:35
 * @描述：
 */
public abstract class BaseFragment extends Fragment {

    protected Context mContext;
    protected Activity mActivity;
    private boolean isFragmentVisible = true;
    private boolean isPrepared = false;
    private boolean isFirst = true;
    private boolean isInViewPager = false;
    private View rootView;
    public MMKV mmkv;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        this.mContext = context;
        this.mActivity = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        initBeforeCreateView(savedInstanceState);
        int contentLayout = contentLayout();
        if (contentLayout != 0) {
            rootView = setLayoutView(inflater, container);
        }
        MMKV.initialize(this.mContext);
        mmkv= MMKV.defaultMMKV();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        isPrepared = true;
        lazyLoad();
    }

    private void lazyLoad() {
        if (!isInViewPager) {
            isFirst = false;
            initData();
            return;
        } else {
            if (!isPrepared || !isFragmentVisible || !isFirst) {
                return;
            }
            isFirst = false;
            initData();
        }
    }

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 初始化View
     *
     * @param view
     */
    protected abstract void initView(View view);

    protected View setLayoutView(LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(contentLayout(), container, false);
    }

    protected void initBeforeCreateView(Bundle savedInstanceState) {

    }

    /**
     * 内容布局Id2
     *
     * @return
     */
    protected abstract int contentLayout();
}
