package com.example.base_lib.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import androidx.core.content.FileProvider;

public class FileUtil {

    public static final File facefile(Context context,Bitmap bitmap, String username) {
        File file = null;
        try {
            if (bitmap != null) {
                String fileName=username+".jpg";
                file = FileUtil.createFile(context,fileName);
                BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                bos.flush();
                bos.close();

            }
        } catch (IOException e) {
            e.printStackTrace();
        }


        return file;
    }

    private static File createFile(Context context,String filename) {
        File file=context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File rootFile =new File(file,filename);
        if (rootFile.exists()){
            rootFile.delete();
            try {
                rootFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return rootFile;
    }

    public static void deletfile(File file){
        file.delete();
        //Todo 删除照片
    }

    public static Uri getFileUri(Context context, File file) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {// sdk >= 24  android7.0以上
            Uri contentUri = FileProvider.getUriForFile(context,
                    "com.example.IntegratedMonitoring.provider",//与清单文件中android:authorities的值保持一致
                    file);//FileProvider方式或者ContentProvider。也可使用VmPolicy方式
            return contentUri;

        } else {
            return Uri.fromFile(file);//或者 Uri.isPaise("file://"+file.toString()
        }
    }

}
