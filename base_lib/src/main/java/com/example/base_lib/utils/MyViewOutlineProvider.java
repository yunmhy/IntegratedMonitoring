package com.example.base_lib.utils;

import android.graphics.Outline;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewOutlineProvider;

public class MyViewOutlineProvider extends ViewOutlineProvider {
    @Override
    public void getOutline(View view, Outline outline) {
        Rect rect = new Rect();
        view.getGlobalVisibleRect(rect);
//        int Margin = Math.min(view.getWidth(), view.getHeight()) / 10;

        Rect selfRect = new Rect(0, 0, view.getWidth(), view.getHeight());
        outline.setRoundRect(selfRect, view.getWidth()/2);
//        outline.setOval(view.getWidth()/2,view.getHeight()/2,view.getWidth()/2,view.getHeight()/2);

    }
}
