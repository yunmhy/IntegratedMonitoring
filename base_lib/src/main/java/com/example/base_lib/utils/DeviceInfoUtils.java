package com.example.base_lib.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.telephony.TelephonyManager;

import androidx.core.app.ActivityCompat;

import com.example.base_lib.baen.DeviceInfo;

import me.jessyan.autosize.utils.LogUtils;

public class DeviceInfoUtils {


    public static DeviceInfo getSIM(Context context){
        DeviceInfo deviceInfo=new DeviceInfo();
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            deviceInfo.imei = telephonyManager.getDeviceId();
            LogUtils.d("imei ->" + deviceInfo.imei);

            deviceInfo.imsi = telephonyManager.getSubscriberId();
            LogUtils.d("imsi ->" + deviceInfo.imsi);

            deviceInfo.phoneNumber = telephonyManager.getLine1Number();
            LogUtils.d("SIM卡中存储本机号码 phoneNumber ->" +  deviceInfo.phoneNumber );

//            voiceMail = telephonyManager.getVoiceMailNumber();
//            LogUtils.i("语音邮件号码 voiceMail ->" + voiceMail);

            deviceInfo.simSerial = telephonyManager.getSimSerialNumber();
            LogUtils.d("SIM卡序列号 simSerial ->" +   deviceInfo.simSerial );

            deviceInfo.countryIso = telephonyManager.getNetworkCountryIso();
            LogUtils.d("SIM卡提供商的国家代码 countryIso ->" + deviceInfo.countryIso);

//            carrier = telephonyManager.getNetworkOperatorName();
//            LogUtils.i("当前移动网络运营商 carrier ->" + carrier);

//            if (!StringUtils.isNull(imsi) && imsi.length() == 15) {
//                mcc = imsi.substring(0, 3);
//                mnc = imsi.substring(3, 5);
//            }
//            LogUtils.i("mcc ->" + mcc);
//            LogUtils.i("mnc ->" + mnc);
//
//            simOperator = telephonyManager.getSimOperator();
//            LogUtils.i("SIM的移动运营商的名称 simOperator ->" + simOperator);
//
//            phoneType = telephonyManager.getPhoneType();
//            LogUtils.i("移动终端的类型 phoneType ->" + phoneType);
//
//            radioType = telephonyManager.getNetworkType();
//            LogUtils.i("当前使用的网络制式 radioType ->" + radioType);

        }catch (Exception e){
            e.printStackTrace();
        }
        return deviceInfo;
    }




}
