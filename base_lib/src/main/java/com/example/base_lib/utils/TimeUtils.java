package com.example.base_lib.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {
    public static String realTimeFormat_from = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS";
    public static String alarmTimeFormat_from = "yyyy-MM-dd'T'HH:mm:ss";
    public static String timeFormat_to = "yyyy-MM-dd HH:mm:ss";
    public static String chartFormat_to = "HH:mm:ss";
    public static String dateFormat_month = "MM-dd";

    //实时表格的时间格式化;
    public static  String  CurveTimeFarmat(String time){
        if(time==null){
            return null;
        }
        String formatTime = null;
        try {
            Date  date =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS").parse(time);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(timeFormat_to);
            formatTime = simpleDateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  formatTime;
    }

    //报警fragment的时间格式化;
    public static  String  AlarmTimeFarmat(String time){
        String formatTime = null;
        try {
            Date  date =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(time);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");
            formatTime = simpleDateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  formatTime;
    }

    //实时曲线的时间转化;
    public static  String  chartTimeFarmat(String time){
        String formatTime = null;
        try {
            Date  date =  new SimpleDateFormat(realTimeFormat_from).parse(time);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(chartFormat_to);
            formatTime = simpleDateFormat.format(date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return  formatTime;
    }

    //时间转毫秒;
    public static long dateToMillSec(String date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(realTimeFormat_from);
        long time1 = 0;
        try {
            if (null!=date&&date.length()>0)
             time1 = simpleDateFormat.parse(date).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return time1;
    }





    /**
     * 毫秒时间转换成字符串,默认为"yyyy-MM-dd HH:mm:ss"
     *
     * @param time 时间
     */
    public static String dateToString(long time) {
        return dateToString(time, "yyyy.MM.dd HH:mm");
    }

    /**
     * 毫秒时间转换成字符串,指定格式
     *
     * @param time   时间
     * @param format 时间格式
     */
    public static String dateToString(long time, String format) {
        Date date = new Date(time);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
        return simpleDateFormat.format(date);
    }

    public static String TimeAdd(int type,String startTime,int addtime){
        String endTime = null;
        Calendar calendar=Calendar.getInstance();
        try {
            calendar.setTime(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS").parse(startTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        calendar.add(type,addtime);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(timeFormat_to);
        endTime = simpleDateFormat.format(calendar.getTime());
        return endTime;
    }
}
