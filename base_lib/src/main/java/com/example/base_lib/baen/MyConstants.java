package com.example.base_lib.baen;

/**
 * Created by Android Studio.
 * User: mhy
 * Date: 2019/12/20
 * Time: 16:22
 * Description
 */

public class MyConstants {
    public static final long DEFAULT_TIME = 30000;
//    public static String TestUrl ="http://101.200.132.50:8078/";
    public  static String TestUrl ="http://192.168.14.20:8078/";
    public final static String BASERURL = "";
    public static final String SUCCESS = "success";
    public static final String FAILED = "failed";
    //BTN_NAME
    public static final int SAFE = 1;
    public static final int PERSON = 2;
    public static final int PRODUCT = 3;
    public static final int VIDEO = 4;
    public static final int RESCUE = 5;
    public static final int DEVICE = 6;
    public static final int WHATE = 7;
    public static final int KZDYL = 8;
}
