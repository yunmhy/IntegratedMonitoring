package com.example.base_lib.baen;

import com.contrarywind.interfaces.IPickerViewData;

import java.util.List;

public class PickerData implements IPickerViewData {
    private String firstname;
    private List<ToPickerData>seconddata;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public List<ToPickerData> getSeconddata() {
        return seconddata;
    }

    public void setSeconddata(List<ToPickerData> seconddata) {
        this.seconddata = seconddata;
    }

    @Override
    public String getPickerViewText() {
        return null;
    }

    public static class ToPickerData {
        private String secondname;
        private List<String> threedata;

        public String getSecondname() {
            return secondname;
        }

        public void setSecondname(String secondname) {
            this.secondname = secondname;
        }

        public List<String> getThreedata() {
            return threedata;
        }

        public void setThreedata(List<String> threedata) {
            this.threedata = threedata;
        }
    }
}
