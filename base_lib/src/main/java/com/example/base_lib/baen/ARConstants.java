package com.example.base_lib.baen;

public class ARConstants {

    public static final String APP_MAIN="/integrated_app/MainActivity";
    public static final String APP_Login="/login_lib/LoginActivity";
    public static final String APP_SAFE="/safe_lib/SafeMainActivity";
    public static final String APP_RYDW="/rydw_lib/RydwMainActivity";
    public static final String APP_VIDEO="/video_lib/ViedoMainActivity";
}
