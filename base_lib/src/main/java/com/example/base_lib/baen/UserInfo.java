package com.example.base_lib.baen;


import java.io.Serializable;
import java.util.List;

public class UserInfo implements Serializable {


    /**
     * code : 140121B0014000000001
     * comename : admin
     * post : null
     * unitinfo : [{"id":1,"citycode":"1401","areacode":"140121","unitcode":"140121B0014000000001","unitname":"太原市梗阳实业集团有限公司麦地掌煤矿","nickname":"麦地掌煤矿","capacity":null}]
     * roleid : 1
     * usertype : 1
     * tel : null
     * useridname : 麦地掌煤矿
     * userid : 1
     * username : 超级管理员
     * token : eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJodHRwczovL2FuYWx5c2lzLnN4eWdzai5jb20vY2FvbHNhbGUvYXBpIiwidmVyIjoidjEuMCIsImNvZGUiOiIxNDAxMjFCMDAxNDAwMDAwMDAwMSIsIm5iZiI6MTU4MzgyNDMxMCwiY29tZW5hbWUiOiJhZG1pbiIsInJvbGVpZCI6IjEiLCJpc3MiOiJjb2FsIHNhbGVAc3h5Z3NqIiwidXNlcnR5cGUiOiIxIiwidW5pdGlkIjoiMSIsInR5cGUiOiJ0b2tlbiIsInVzZXJpZCI6IjEiLCJ1c2VybmFtZSI6Iui2hee6p-euoeeQhuWRmCJ9.UNnrpKztldGSieKHCrZiMb1hcIKSGAU5UsB4VWTox8M
     */

    private String code;
    private String comename;
    private String post;
    private String roleid;
    private int usertype;
    private String tel;
    private String useridname;
    private int userid;
    private String username;
    private String token;
    private List<UnitinfoBean> unitinfo;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getComename() {
        return comename;
    }

    public void setComename(String comename) {
        this.comename = comename;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    public int getUsertype() {
        return usertype;
    }

    public void setUsertype(int usertype) {
        this.usertype = usertype;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getUseridname() {
        return useridname;
    }

    public void setUseridname(String useridname) {
        this.useridname = useridname;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<UnitinfoBean> getUnitinfo() {
        return unitinfo;
    }

    public void setUnitinfo(List<UnitinfoBean> unitinfo) {
        this.unitinfo = unitinfo;
    }

    public static class UnitinfoBean {
        /**
         * id : 1
         * citycode : 1401
         * areacode : 140121
         * unitcode : 140121B0014000000001
         * unitname : 太原市梗阳实业集团有限公司麦地掌煤矿
         * nickname : 麦地掌煤矿
         * capacity : null
         */

        private int id;
        private String citycode;
        private String areacode;
        private String unitcode;
        private String unitname;
        private String nickname;
        private Object capacity;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getCitycode() {
            return citycode;
        }

        public void setCitycode(String citycode) {
            this.citycode = citycode;
        }

        public String getAreacode() {
            return areacode;
        }

        public void setAreacode(String areacode) {
            this.areacode = areacode;
        }

        public String getUnitcode() {
            return unitcode;
        }

        public void setUnitcode(String unitcode) {
            this.unitcode = unitcode;
        }

        public String getUnitname() {
            return unitname;
        }

        public void setUnitname(String unitname) {
            this.unitname = unitname;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public Object getCapacity() {
            return capacity;
        }

        public void setCapacity(Object capacity) {
            this.capacity = capacity;
        }
    }
}
