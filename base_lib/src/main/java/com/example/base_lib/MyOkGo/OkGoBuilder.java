package com.example.base_lib.MyOkGo;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;


import com.example.base_lib.MyCallback;
import com.example.base_lib.R;
import com.example.base_lib.baen.MyConstants;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.cache.CacheMode;
import com.lzy.okgo.https.HttpsUtils;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
import com.lzy.okgo.model.Response;
import com.lzy.okgo.request.base.Request;

import java.lang.reflect.Type;

public class OkGoBuilder<T> {
    /**
     * get请求
     */
    public static final int GET = 1;
    /**
     * post请求
     */
    public static final int PSOT = 2;


    private Activity context;
    /**
     * 请求网址
     */
    private String baseurl;
    /**
     * 请求网址接口
     */
    private String inurl;
    /**
     * 参数
     */
    private HttpParams params;
    /**
     * headers
     */
    private HttpHeaders headers;
    /**
     * 实体类
     */
    private Class<T> clazz;

    /**
     * 类型
     */
    private Type type;
    /**
     * 回调
     */
    private MyCallback<T> callback;

    /**
     * dialog 是否显示
     */
    private boolean isdialog=true;
    private int methodType;

    /**
     * 单列模式
     **/
    private static OkGoBuilder mOkGoBuilder = null;



    /**
     * 构造函数私有化
     **/
    private OkGoBuilder() {
    }


    /**
     * 公有的静态函数，对外暴露获取单例对象的接口
     **/
    public static OkGoBuilder getInstance() {
        if (mOkGoBuilder == null) {
            synchronized (OkGoBuilder.class) {
                if (mOkGoBuilder == null) {
                    mOkGoBuilder = new OkGoBuilder();
                }
//                return mOkGoBuilder;
            }
        }
        return mOkGoBuilder;
    }


    public OkGoBuilder Builder(Activity context) {
        this.context = context;
        return this;
    }

    public OkGoBuilder baseurl(String baseurl) {
        this.baseurl = baseurl;
        return this;
    }
    public OkGoBuilder inurl(String inurl) {
        this.inurl = inurl;
        return this;
    }

    public OkGoBuilder method(int methodType) {
        this.methodType = methodType;
        return this;
    }

    public OkGoBuilder params(HttpParams params) {
        this.params = params;
        return this;
    }
    public OkGoBuilder headers(HttpHeaders headers){
        this.headers=headers;
        return this;
    }
    public OkGoBuilder cls(Class<T> clazz) {
        this.clazz = clazz;
        return this;
    }
    public OkGoBuilder type(Type type) {
        this.type = type;
        return this;
    }
    public  OkGoBuilder dialogshow(boolean b){
        this.isdialog=b;
        return this;
    }
    public OkGoBuilder callback(MyCallback<T> callback) {
        this.callback = callback;
        return this;
    }


    public OkGoBuilder build() {
        OkGo.getInstance().cancelAll();
        if (methodType == 1) {
            getRequest();
        } else {
            postRequest();
        }
        return this;
    }

    /**
     * post请求
     */
    private void postRequest() {
        OkGo
                // 请求方式和请求url
                .<T>post(baseurl+inurl)
                .headers(headers)
                .params(params)
                // 请求的 tag, 主要用于取消对应的请求
                .tag(this)
                // 设置当前请求的缓存key,建议每个不同功能的请求设置一个
                .cacheKey("cacheKey")
                // 缓存模式，详细请看缓存介绍
                .cacheMode(CacheMode.DEFAULT)
                .execute( new DialogCallback<T>(context, clazz) {
                    @Override
                    public void onSuccess(Response<T> response) {
                        callback.onSuccess(response.body(), MyConstants.SUCCESS);
                    }

                    @Override
                    public void onError(Response<T> response) {
                        super.onError(response);
                        Throwable throwable = response.getException();
                        if (response.body()!=null){
                            String res = response.body().toString();
                            callback.onError(res,MyConstants.FAILED);
                        }
                        else if (throwable != null) {
                            throwable.printStackTrace();
                            callback.onError(throwable.getMessage(), MyConstants.FAILED);
                        }
                    }

                    @Override
                    public void onStart(Request<T, ? extends Request> request) {
                        super.onStart(request);
                        if (isdialog){
                            loadingView.findViewById(R.id.probar).setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }
                });
    }

    /**
     * get请求
     */
    private void getRequest() {
        OkGo
                // 请求方式和请求url
                .<T>get(baseurl+inurl)
                .headers(headers)
                .params(params)
                // 请求的 tag, 主要用于取消对应的请求
                .tag(this)
                // 设置当前请求的缓存key,建议每个不同功能的请求设置一个
                .cacheKey("cacheKey")
                // 缓存模式，详细请看缓存介绍
                .cacheMode(CacheMode.DEFAULT)
                .execute(new DialogCallback<T>(context, clazz) {
                    @Override
                    public void onSuccess(Response<T> response) {
                        callback.onSuccess(response.body(), MyConstants.SUCCESS);
                    }

                    @Override
                    public void onError(Response<T> response) {
                        super.onError(response);
                        Throwable throwable = response.getException();
                        if (response.body()!=null){
                            String res = response.body().toString();
                            callback.onError(res,MyConstants.FAILED);
                        }
                       else if (throwable != null) {
                            throwable.printStackTrace();
                            callback.onError(throwable.getMessage(), MyConstants.FAILED);
                        }
                    }

                    @Override
                    public void onStart(Request<T, ? extends Request> request) {
                        super.onStart(request);
                        if (!isdialog){
                            loadingView.findViewById(R.id.probar).setVisibility(View.GONE);
                        }
                    }

                    @Override
                    public void onFinish() {
                        super.onFinish();
                    }
                });

    }

}
