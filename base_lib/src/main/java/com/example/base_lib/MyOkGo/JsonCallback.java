package com.example.base_lib.MyOkGo;
import android.widget.Toast;

import com.example.base_lib.MyApplication;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.lzy.okgo.callback.AbsCallback;
import com.lzy.okgo.model.Response;

import java.lang.reflect.Type;
import okhttp3.ResponseBody;


public abstract class JsonCallback<T> extends AbsCallback<T> {
    private Type mType;
    private Class<T> clazz;
    private String res_str=null;

    public JsonCallback() {
    }

    public JsonCallback(Type type) {
        mType = type;
    }

    public JsonCallback(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public T convertResponse(okhttp3.Response response) throws Throwable {
        ResponseBody body = response.body();
        if (body == null) {
            return null;
        }
        T data = null;
        Gson gson = new Gson();
        String str = response.body().string();
        try {
            if (mType != null) {
                data = gson.fromJson(str, mType);
            }
            if (clazz != null) {
                data = gson.fromJson(str, clazz);

            }
            return data;
        } catch (JsonSyntaxException e) {
            res_str=str;
            throw e;
        }



    }

    @Override
    public void onError(Response<T> response) {
        super.onError(response);
        if (res_str!=null){
            response.setBody((T) res_str);
        }


    }
}
