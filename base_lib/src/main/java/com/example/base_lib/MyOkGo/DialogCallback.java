package com.example.base_lib.MyOkGo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.example.base_lib.R;
import com.lzy.okgo.model.Response;
import com.lzy.okgo.request.base.Request;

import java.lang.reflect.Type;

public abstract class DialogCallback<T> extends JsonCallback<T> {
    private ProgressDialog mProgressDialog;
    protected View loadingView;
    FrameLayout root;
    public DialogCallback(Activity activity, Class<T> tClass) {
        super(tClass);
        initDialog(activity);
    }
    public DialogCallback(Activity activity, Type type) {
        super(type);
        initDialog(activity);
    }

    public void initDialog(Activity activity) {

         root = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        if (root != null){
            loadingView= LayoutInflater.from(activity).inflate(R.layout.lyt_loading, null);

                //遮罩层设置点击事件，拦截底层视图的点击事件
                loadingView.findViewById(R.id.cover).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });


        }
    }

    @Override
    public void onStart(Request<T, ? extends Request> request) {
        loadingView.findViewById(R.id.cover).setVisibility( View.VISIBLE);
        loadingView.findViewById(R.id.probar).setVisibility(View.VISIBLE);
        root.addView(loadingView);
    }

    @Override
    public void onFinish() {
        loadingView.findViewById(R.id.cover).setVisibility( View.GONE);
        loadingView.findViewById(R.id.probar).setVisibility(View.GONE);
        root.removeView(loadingView);

    }


}
