package com.example.base_lib;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDex;

import com.alibaba.android.arouter.launcher.ARouter;
import com.example.base_lib.MyOkGo.OkGoInit;
import com.example.base_lib.baen.MyConstants;
import com.hikvision.open.hikvideoplayer.HikVideoPlayerFactory;

import java.util.ArrayList;
import java.util.List;

import me.jessyan.autosize.AutoSizeConfig;

public class MyApplication extends Application implements Application.ActivityLifecycleCallbacks{
    int countActivity = 0;
    boolean mCountFlag=false;
    private boolean isDebugARouter = true;
    List<Activity> activities  = new ArrayList<Activity>();
    private static MyApplication instance;
    private CountDownTimer mCountDownTimer = null;
    public static synchronized MyApplication getInstance(){
        if (null == instance){
            instance = new MyApplication();
        }
        return instance;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        OkGoInit.okGoinit(this);//OKGO 初始化
        if (isDebugARouter) {           // 这两行必须写在init之前，否则这些配置在init过程中将无效
            ARouter.openLog();     // 打印日志
            ARouter.openDebug();   // 开启调试模式(如果在InstantRun模式下运行，必须开启调试模式！线上版本需要关闭,否则有安全风险)
        }
        ARouter.init(MyApplication.this);
        AutoSizeConfig.getInstance().getUnitsManager();
        //全局管理Activity生命周期
        registerActivityLifecycleCallbacks(this);
        HikVideoPlayerFactory.initLib("",true);
        mCountDownTimer = new CountDownTimer(60 * 1000, 1 * 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                finishAllActivity();
                mCountFlag=false;

            }
        };
    }

    public void addActivity(Activity activity) {
        // 判断当前集合中不存在该Activity
        if (!activities.contains(activity)) {
            activities.add(activity);//把当前Activity添加到集合中
        }
    }

    public void removeActivity(Activity activity) {
        //判断当前集合中存在该Activity
        if (activities.contains(activity)) {
            activities.remove(activity);//从集合中移除
            activity.finish();//销毁当前Activity
        }
    }
    public void finishAllActivity() {
        //判断当前集合中存在该Activity
        for (Activity activity : activities) {
            activity.finish();
        }
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
        //ARouter inject 释放
        ARouter.getInstance().destroy();
    }


    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
        addActivity(activity);
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {
        if (mCountFlag){
            mCountDownTimer.cancel();
            mCountFlag=false;
        }
        countActivity++;

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {
        Log.i("MyApplication","onActivityResumed");
    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {
        Log.i("MyApplication","onActivityResumed"+activities.size());
    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

        countActivity--;
        if (countActivity<=0){
            Log.i("MyApplication","退出到后台"+ activities.size());
            Toast.makeText(getApplicationContext(),"后台运行，60秒后关闭程序",Toast.LENGTH_LONG).show();
            mCountDownTimer.start();
            mCountFlag=true;

        }
    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        removeActivity(activity);
    }
}
