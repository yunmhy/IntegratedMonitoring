package com.example.base_lib;

public interface MyCallback<T> {
    void onSuccess(T t, String code);
    void onError (String e, String code);
}
