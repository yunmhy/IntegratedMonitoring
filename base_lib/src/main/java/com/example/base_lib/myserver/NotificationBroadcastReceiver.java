package com.example.base_lib.myserver;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class NotificationBroadcastReceiver extends BroadcastReceiver {
    public static final String TYPE = "type";


    @Override
    public void onReceive(Context context, Intent intent) {

        String action = intent.getAction();
        int type = intent.getIntExtra("type", -1);

        if (type != -1) {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(type);
        }

        if (action.equals("notification_clicked")) {
            //处理点击事件
//            onClick(context);
        }
        if (action.equals("notification_cancelled")) {
            //处理滑动清除和点击删除事件
            context.stopService(new Intent(context,MessageService.class));
        }

    }

    private void onClick(Context context) {


        //启动应用
        Intent launchIntent = context.getPackageManager().
                getLaunchIntentForPackage("com.example.integrated_app.LeadActivity");
        launchIntent.setFlags(
                Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        Bundle args = new Bundle();
        context.startActivity(launchIntent);

    }
}