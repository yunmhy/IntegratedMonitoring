package com.example.base_lib.activity;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.alibaba.android.arouter.launcher.ARouter;
import com.example.base_lib.MyApplication;
import com.example.base_lib.MyCallback;
import com.example.base_lib.R;
import com.example.base_lib.baen.ARConstants;
import com.example.base_lib.baen.UserInfo;
import com.tencent.mmkv.MMKV;

/**
 * @创建时间：2019/10/9 16:22
 * @描述：
 */
public abstract class BaseActivity extends AppCompatActivity {
    protected static UserInfo baseuserInfo;
    private LinearLayout parentLinearLayout;
    private TextView mTvTitle;
    private Toolbar toolbar;
    private Boolean menub = true;
    private int menuid;
    public MMKV mmkv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);//禁止截屏
        this.onCreateBefore();
        this.initStatus();
        initBeforeCreateView(savedInstanceState);
        super.onCreate(savedInstanceState);
        //ARouter inject 注入
        ARouter.getInstance().inject(this);
        setLayoutView();
        initView();
        initData();
//        MyApplication.getInstance().addActivity(this);
    }

    /**
     * 加载布局..
     */
    protected void setLayoutView() {
        initContentView(R.layout.activity_base);
        setContentView(layoutResId());
        setSupportActionBar(getToolbar());
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    public void setBarColor(boolean isDark){
        if (isDark) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//黑色
        } else {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);//白色
        }
    }

    /**
     * @Description: [封装toolbar标题栏]
     * @Param:
     * @Return:
     */
    public void initContentView(@LayoutRes int layoutResID) {
        ViewGroup viewGroup = (ViewGroup) findViewById(android.R.id.content);
        viewGroup.removeAllViews();
        parentLinearLayout = new LinearLayout(this);
        parentLinearLayout.setOrientation(LinearLayout.VERTICAL);
        viewGroup.addView(parentLinearLayout);
        //  add the layout of BaseActivity in parentLinearLayout
        LayoutInflater.from(this).inflate(layoutResID, parentLinearLayout, true);
        toolbar = findViewById(R.id.toolbar);

    }
    @Override
    public void setContentView(int layoutResID) {
        LayoutInflater.from(this).inflate(layoutResID, parentLinearLayout, true);

    }

    /**
     * 这里可以做一些setContentView之前的操作,如全屏、常亮、设置Navigation颜色、状态栏颜色等
     */
    protected void onCreateBefore() {

    }

    protected abstract void initView();

    protected abstract void initData();

    /**
     * 页面内容布局resId
     */
    protected abstract int layoutResId();

    /**
     * 需要在onCreateView中调用的方法
     */
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        MMKV.initialize(this.getApplicationContext());
        mmkv = MMKV.defaultMMKV();
    }

    /**
     * 适配状态栏
     */
    protected void initStatus() {
        Window window = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_STABLE );
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    //是否显示menu
    protected void setTbM(boolean b, int id) {
        this.menub = b;
        this.menuid = id;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu, menu);
        if (!menub) {
            MenuItem menuItem = menu.findItem(menuid);
            menuItem.setVisible(menub);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_home) {
            finish();
        }
        if (item.getItemId() == R.id.menu_safe) {
            ARouter.getInstance().build(ARConstants.APP_SAFE)
//                    .withSerializable("userinfo", baseuserInfo)
                    .navigation();
            finish();
        }
        if (item.getItemId() == R.id.menu_rydw) {
            ARouter.getInstance().build(ARConstants.APP_RYDW)
//                    .withSerializable("userinfo", baseuserInfo)
                    .navigation();
            finish();
        }
        if (item.getItemId() == R.id.menu_video) {
            ARouter.getInstance().build(ARConstants.APP_VIDEO)
                    .navigation();
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
//        MyApplication.getInstance().removeActivity(this);
        super.onDestroy();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (ev.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideInput(v, ev)) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    assert v != null;
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
            }
            return super.dispatchTouchEvent(ev);
        }
        // 必不可少，否则所有的组件都不会有TouchEvent了
        return getWindow().superDispatchTouchEvent(ev) || onTouchEvent(ev);
    }

    public boolean isShouldHideInput(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] leftTop = {0, 0};
            //获取输入框当前的location位置
            v.getLocationInWindow(leftTop);
            int left = leftTop[0];
            int top = leftTop[1];
            int bottom = top + v.getHeight();
            int right = left + v.getWidth();
            return !(event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom);
        }
        return false;
    }
//    private void setBackIcon(){
//        if (null != getToolbar() && isShowBacking()) {
//            getToolbar().setNavigationIcon(R.drawable.back);
////            getToolbar().setNavigationOnClickListener((v) -> onBackPressed());
//        }
//    }

    /**
     * @return TextView in center
     */
    public TextView getToolbarTitle() {
        mTvTitle = (TextView) findViewById(R.id.toolbar_msg);
        return mTvTitle;
    }


    /**
     * the toolbar of this Activity
     *
     * @return support.v7.widget.Toolbar.
     */
    public Toolbar getToolbar() {
        return toolbar;
    }

    /**
     * is show back icon,default is none。
     * you can override the function in subclass and return to true show the back icon
     * @return
     */
//    protected boolean isShowBacking() {
//        return true;
//    }


}
