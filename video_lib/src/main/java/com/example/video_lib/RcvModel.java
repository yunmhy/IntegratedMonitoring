package com.example.video_lib;

import android.app.Activity;
import android.content.Context;
import android.util.Log;


import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.video_lib.bean.PlayData;
import com.example.video_lib.bean.RcvData;
import com.example.video_lib.bean.Test;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;


public class RcvModel {
    String TAG="RcvModel";
    Context context;
    static Activity activity;
    private MutableLiveData<String> errMsg;
    private MutableLiveData<List<Test>> deviceItemList;
    private MutableLiveData<PlayData>playdata;
    private  RcvModel rcvModel;
    private  UserInfo userInfo;

//    public static RcvModel getInstance(Activity activity1,UserInfo userInfo1){
//        if (userInfo1!=null) userInfo=userInfo1;
//        activity=activity1;
//        if (rcvModel==null){
//            rcvModel=new RcvModel(activity);
//        }
//        return rcvModel;
//    }
    public RcvModel(Activity activity,UserInfo userInfo1){
        if (userInfo1!=null) userInfo=userInfo1;
        this.activity=activity;
        this.context=activity.getApplicationContext();
        deviceItemList=new MutableLiveData<>();
        playdata=new MutableLiveData<>();
        errMsg=new MutableLiveData<>();


    }

    public LiveData<List<Test>> getmAllRcv() {

        return deviceItemList;
    }
    public LiveData<String>getErrMsg(){
        return errMsg;
    }

    public void getData() {
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/video/index")
                .cls(RcvData.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .dialogshow(true)
                .callback(new MyCallback<RcvData>() {
                    @Override
                    public void onSuccess(RcvData dataList, String code) {
                        if(dataList.getData()!=null&&dataList.getData().size()>0){
                            List<Test> tests=new ArrayList<>();
                            for (RcvData.DataBean dataBean:dataList.getData()) {
                                tests.add(new Test(dataBean));
                            }
//                        RcvData.DataBean dataBean = dataList.getData().get(1);
//                            tests.add(new Test(dataBean));
                            deviceItemList.postValue(tests);
                            errMsg.postValue(code);
                        }else {
                            deviceItemList.postValue(null);
                            errMsg.postValue(code);
                        }
                    }

                    @Override
                    public void onError(String e, String code) {
                        Log.e(TAG, "onError: "+e);
                        errMsg.postValue(e);
//                        deviceItemList.postValue(null);
                    }
                }).build();
    }


    public void getPlayUrl(String cameraIndexCode,int streamType) {
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        paramsGet.put("cameraIndexCode",cameraIndexCode);
        paramsGet.put("streamType",streamType);
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/video/camera/url")
                .cls(PlayData.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .callback(new MyCallback<PlayData>() {
                    @Override
                    public void onSuccess(PlayData data, String code) {
                        playdata.postValue(data);
                        errMsg.postValue(code);

                    }

                    @Override
                    public void onError(String e, String code) {
                        Log.e(TAG, "onError: "+e);
                        errMsg.postValue(e);
//                        deviceItemList.postValue(null);
                    }
                }).build();
    }

    public LiveData<PlayData>getUrl(){
        return playdata;
    }
}
