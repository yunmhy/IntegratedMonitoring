package com.example.video_lib;

import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.example.base_lib.activity.BaseActivity;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.ARConstants;
import com.example.video_lib.bean.PlayData;
import com.example.video_lib.bean.PlayerStatus;
import com.example.video_lib.bean.RcvData;
import com.example.video_lib.bean.Test;
import com.example.video_lib.widget.AutoHideView;
import com.hikvision.open.hikvideoplayer.HikVideoPlayer;
import com.hikvision.open.hikvideoplayer.HikVideoPlayerCallback;
import com.hikvision.open.hikvideoplayer.HikVideoPlayerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;


// 在支持路由的页面上添加注解(必选)
// 这里的路径需要注意的是至少需要有两级，/xx/xx
@Route(path = ARConstants.APP_VIDEO)
public class ViedoMainActivity extends BaseActivity implements TextureView.SurfaceTextureListener, HikVideoPlayerCallback {
    private static final String TAG = "ViedoMainActivity";
    private RcvModel rcvModel;
    private RecyclerView recyclerView;
    private List<Test> list;
    private RcvDemoAdapter adapter;
    private HikVideoPlayer mPlayer;
    private TextureView textureView;
    private TextView viedo_t;
    private PlayerStatus mPlayerStatus = PlayerStatus.IDLE;//默认闲置
    private ProgressBar rcvProgress;
    private TextView rcvErr;
    private String mUri;
    private int position = 0;
    private String cameraIndexCode;
    protected ConstraintLayout frameLayout;
    protected AutoHideView autoHideView;

    @Override
    protected void initView() {
        findViewById(R.id.back_back).setBackground(new WaterMarkBg(baseuserInfo.getUseridname()));
        getToolbarTitle().setText("工业视频");
        getToolbarTitle().setTextColor(Color.WHITE);
        getToolbar().setBackground(getResources().getDrawable(R.color.blue));
//        setBarColor(true);
        setTbM(false, R.id.menu_video);
//        frameLayout=(ConstraintLayout)findViewById(R.id.frame_layout);
//        autoHideView = findViewById(R.id.auto_hide_view);
//        frameLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (autoHideView.isVisible()) {
//                    autoHideView.hide();
//                } else {
//                    autoHideView.show();
//                }
//            }
//        });
        recyclerView = (RecyclerView) findViewById(R.id.rcv_main);
        viedo_t = (TextView) findViewById(R.id.video_t);
        mPlayer = HikVideoPlayerFactory.provideHikVideoPlayer();
//        mPlayer = new IjkMediaPlayer();
//        textureView = findViewById(R.id.vide_sur);
//        textureView.setSurfaceTextureListener(this);
//        rcvProgress = (ProgressBar) findViewById(R.id.rcv_progress);
//        rcvErr = (TextView) findViewById(R.id.rcv_err);
//        textureView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (mPlayerStatus!=PlayerStatus.SUCCESS){
//                    return;
//                }
//                Intent intent = new Intent(ViedoMainActivity.this, StartPlayActivity.class);
//                intent.putExtra("cameraIndexCode", cameraIndexCode);
////                intent.putExtra("name",list.get(position).getCameraName());
//                startActivity(intent);
//            }
//        });
//        rcvErr.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                rcvModel.getPlayUrl(list.get(position).getCameraIndexCode(),1);
//            }
//        });
    }


    @Override
    protected int layoutResId() {
        return R.layout.activity_viedo_main;
    }


    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        super.initBeforeCreateView(savedInstanceState);
        rcvModel = new RcvModel(this, baseuserInfo);


    }


    @Override
    protected void initData() {
        viedo_t.setText(baseuserInfo.getUnitinfo().get(0).getNickname());
        rcvModel.getData();
//        RcvData sfc1 = new RcvData("测试", "admin", "Hik12345", "192.168.7.53", "8000");
        list = new ArrayList<>();
        adapter = new RcvDemoAdapter(R.layout.rcv_item);
        View empty = LayoutInflater.from(this).inflate(R.layout.empty_item, null, false);
        adapter.setEmptyView(empty);
        final TextView textView = empty.findViewById(R.id.txt_errmsg);
        final ConstraintLayout errlay = empty.findViewById(R.id.err_lay);
        final TextView msg = empty.findViewById(R.id.text_empty);
        final ImageView imageView = empty.findViewById(R.id.empty_img);
        final Button btn_rest = empty.findViewById(R.id.btn_rest);
        errlay.setVisibility(View.INVISIBLE);
        btn_rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rcvModel.getData();
            }
        });
        rcvModel.getErrMsg().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                errlay.setVisibility(View.VISIBLE);
                textView.setText(s);
            }
        });
        rcvModel.getmAllRcv().observe(this, new Observer<List<Test>>() {
            @Override
            public void onChanged(List<Test> deviceItems) {
                if(deviceItems!=null){
                    list.addAll(deviceItems);
                    adapter.setNewData(deviceItems);
                }else {
                    errlay.setVisibility(View.VISIBLE);
                    btn_rest.setVisibility(View.INVISIBLE);
                    textView.setVisibility(View.INVISIBLE);
                    imageView.setVisibility(View.INVISIBLE);
                    msg.setText("无视频信息");
                }


//                if (list.get(0).getCameraIndexCode() != null && list.get(0).getUrl_rtsp() != null) {
//                    cameraIndexCode = list.get(0).getCameraIndexCode();
//                    mUri = list.get(0).getUrl_rtsp();
////                    startRealPlay(textureView.getSurfaceTexture(),0);
//                } else {
//                    rcvErr.setVisibility(View.VISIBLE);
//                    rcvErr.setText("无视频资源");
//                    rcvProgress.setVisibility(View.GONE);
//                }


//                     createPlayer(0);
            }
        });
        rcvModel.getUrl().observe(this, new Observer<PlayData>() {
            @Override
            public void onChanged(PlayData data) {
                if (data.getData() != null) {
                    mUri = data.getData();
//                    startRealPlay(textureView.getSurfaceTexture(),position);
                } else {

                    rcvErr.setVisibility(View.VISIBLE);
                    rcvErr.setText("无视频资源");
                    rcvProgress.setVisibility(View.GONE);
                }
            }
        });
        recyclerView.setAdapter(adapter);
        adapter.bindToRecyclerView(recyclerView);
//        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,2);
        LinearLayoutManager linearLayout = new LinearLayoutManager(this);
        linearLayout.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayout);


    }

    @Override
    public void onResume() {
        super.onResume();
        //TODO 注意:APP前后台切换时 SurfaceTextureListener可能在有某些 华为手机 上不会回调，例如：华为P20，所以我们在这里手动调用
//        if (textureView.isAvailable()) {
//            Log.e(TAG, "onResume: onSurfaceTextureAvailable");
//            onSurfaceTextureAvailable(textureView.getSurfaceTexture(), textureView.getWidth(), textureView.getHeight());
//        }
    }

    @Override
    public void onDestroy() {
//         adapter.release();
//        rcvModel.cleanSdk();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        if (textureView.isAvailable()) {
//            Log.e(TAG, "onPause: onSurfaceTextureDestroyed");
//            onSurfaceTextureDestroyed(textureView.getSurfaceTexture());
//        }
    }


    public void release() {

//            if (mPlayer != null) {
//                mPlayer.stop();
//                mPlayer.release();
//                mPlayer = null;
//            }
//            IjkMediaPlayer.native_profileEnd();

    }


    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        mPlayer.setSurfaceTexture(surface);
        if (mPlayerStatus == PlayerStatus.STOPPING) {
            //恢复处于暂停播放状态的窗口
            startRealPlay(textureView.getSurfaceTexture(), position);
            Log.d(TAG, "onSurfaceTextureAvailable: startRealPlay");
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        if (mPlayerStatus == PlayerStatus.SUCCESS) {
            mPlayerStatus = PlayerStatus.STOPPING;//暂停播放，再次进入时恢复播放
            mPlayer.stopPlay();
            Log.d(TAG, "onSurfaceTextureDestroyed: stopPlay");
        }
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    /**
     * 开始播放
     *
     * @param surface 渲染画面
     */

    private void startRealPlay(SurfaceTexture surface, int position) {
        mPlayerStatus = PlayerStatus.LOADING;
        rcvProgress.setVisibility(View.VISIBLE);
        rcvErr.setVisibility(View.GONE);

        //TODO 注意: startRealPlay() 方法会阻塞当前线程，需要在子线程中执行,建议使用RxJava
        new Thread() {
            @Override
            public void run() {
                //TODO 注意: 不要通过判断 startRealPlay() 方法返回 true 来确定播放成功，播放成功会通过HikVideoPlayerCallback回调，startRealPlay() 方法返回 false 即代表 播放失败;
                if (!mPlayer.startRealPlay(mUri, ViedoMainActivity.this))
                    onPlayerStatus(Status.FAILED, mPlayer.getLastError());
                else {
                    onPlayerStatus(Status.SUCCESS, mPlayer.getLastError());
                }

            }
        }.start();
    }

    /**
     * 播放结果回调
     *
     * @param status    共四种状态：SUCCESS（播放成功）、FAILED（播放失败）、EXCEPTION（取流异常）、FINISH（回放结束）
     * @param errorCode 错误码，只有 FAILED 和 EXCEPTION 才有值
     */
    @Override
    @WorkerThread
    public void onPlayerStatus(@NonNull final Status status, final int errorCode) {
        //TODO 注意: 由于 HikVideoPlayerCallback 是在子线程中进行回调的，所以一定要切换到主线程处理UI
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < list.size(); i++) {
//
                }
//

                rcvProgress.setVisibility(View.GONE);
                //只有播放成功时，才允许开启电子放大
                switch (status) {
                    case SUCCESS:
                        //播放成功
                        mPlayerStatus = PlayerStatus.SUCCESS;
                        textureView.setKeepScreenOn(true);//保持亮屏
                        break;
                    case FAILED:
                        //播放失败
                        mPlayerStatus = PlayerStatus.FAILED;
                        rcvErr.setVisibility(View.VISIBLE);
                        rcvErr.setText(MessageFormat.format("预览失败，错误码：{0}", Integer.toHexString(errorCode)) + "\n" + "点击重试");
                        break;
                    case EXCEPTION:
                        //取流异常
                        mPlayerStatus = PlayerStatus.EXCEPTION;
                        mPlayer.stopPlay();//TODO 注意:异常时关闭取流
                        rcvErr.setVisibility(View.VISIBLE);
                        rcvErr.setText(MessageFormat.format("取流发生异常，错误码：{0}", Integer.toHexString(errorCode)) + "\n" + "点击重试");
                        break;
                }
            }
        });
    }


}
