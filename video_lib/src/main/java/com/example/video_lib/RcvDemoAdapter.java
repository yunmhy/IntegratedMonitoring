package com.example.video_lib;


import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.media.JetPlayer;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;


import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.video_lib.bean.PlayerStatus;
import com.example.video_lib.bean.RcvData;
import com.example.video_lib.bean.Test;
import com.hikvision.open.hikvideoplayer.HikVideoPlayer;
import com.hikvision.open.hikvideoplayer.HikVideoPlayerCallback;
import com.hikvision.open.hikvideoplayer.HikVideoPlayerFactory;

import org.w3c.dom.Text;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


public class RcvDemoAdapter extends BaseQuickAdapter<Test, BaseViewHolder> {
    ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
    android.os.Handler handler;
    private TextView t_err;
    //    private List<HikVideoPlayer>playlist=new ArrayList<>();
    private PlayerStatus mPlayerStatus = PlayerStatus.IDLE;//默认闲置
    private HikVideoPlayer mPlayer;

    public RcvDemoAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, final Test item) {
        TextureView textureView = (TextureView) helper.getView(R.id.rcv_sfv);
        item.play(helper,textureView);


//        handler = new Handler() {
//            @Override
//            public void handleMessage(@NonNull Message msg) {
//                ProgressBar bar = (ProgressBar) msg.obj;
//                bar.setVisibility(View.GONE);
//                if (msg.what == 1) {
//                    t_err.setText("播放失败" + (int) msg.arg1);
//                    t_err.setVisibility(View.VISIBLE);
//                }
//            }
//        };
//        final ProgressBar progress = helper.getView(R.id.item_progress);
//        helper.setText(R.id.tv_chanel, item.getCameraName());
//        t_err = (TextView) helper.getView(R.id.t_err);
//        t_err.setVisibility(View.GONE);
//        if (item.getUrl_rtsp() != null &&item.playerStatus==PlayerStatus.IDLE) {
//            mPlayerStatus=PlayerStatus.LOADING;
//            mPlayer = HikVideoPlayerFactory.provideHikVideoPlayer();
//
////        playlist.add(mPlayer);
//            TextureView textureView = (TextureView) helper.getView(R.id.rcv_sfv);
//            mPlayer.setSurfaceTexture(textureView.getSurfaceTexture());
//            mPlayer.startRealPlay(item.getUrl_rtsp(), new HikVideoPlayerCallback() {
//                @Override
//                public void onPlayerStatus(Status status, int i) {
//                    switch (status){
//                        case FAILED:
//                            Log.e("TAG","FAILED");
//                            break;
//                        case SUCCESS:
//                            Log.e("TAG","SUCCESS");
//                            break;
//                        case EXCEPTION:
//                            Log.e("TAG","EXCEPTION");
//                            break;
//
//                    }
//                }
//            });
//
//        } else {
//            progress.setVisibility(View.GONE);
//            t_err.setText("无视频资源");
//            t_err.setVisibility(View.VISIBLE);
//            return;
//        }
//
////
//////            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
////
////        if (helper.getLayoutPosition()==0){
////            helper.setTextColor(R.id.device_name, Color.parseColor("#4472fa"));
////        }
////        helper.setText(R.id.device_name, item.getCameraName());
//    }
//
////    public void release(){
////        for (HikVideoPlayer player:playlist){
////            player.stopPlay();
////        }
////    }
    }
}
