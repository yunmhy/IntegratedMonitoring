package com.example.video_lib;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.SurfaceTexture;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;

import com.example.base_lib.activity.BaseActivity;
import com.example.video_lib.bean.PlayData;
import com.example.video_lib.bean.Test;
import com.hikvision.open.hikvideoplayer.HikVideoPlayer;
import com.hikvision.open.hikvideoplayer.HikVideoPlayerCallback;
import com.hikvision.open.hikvideoplayer.HikVideoPlayerFactory;

import java.text.MessageFormat;


public class StartPlayActivity extends BaseActivity implements HikVideoPlayerCallback{

    private HikVideoPlayer mPlayer;
    private TextureView textureView;
    //    private TextView t_name;
    private ConstraintLayout startPlay_cl;
    private TextView playTErr;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        rcvModel = new RcvModel(this, baseuserInfo);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initView() {
        getToolbar().setVisibility(View.GONE);

        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            getWindow().getDecorView().setSystemUiVisibility(View.INVISIBLE);
        }
        mPlayer = HikVideoPlayerFactory.provideHikVideoPlayer();
        textureView = findViewById(R.id.sfv_play);
        playTErr = (TextView) findViewById(R.id.play_t_err);
        progressBar = findViewById(R.id.loading_progress);
        startPlay_cl = findViewById(R.id.startPlay_cl);
        ImageView back = (ImageView) findViewById(R.id.video_back);
//        t_name = (TextView) findViewById(R.id.video_t_name);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        textureView.setSurfaceTextureListener(new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
                mPlayer.setSurfaceTexture(surface);
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surface) {

            }
        });

        textureView.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {
                if(isResume){
                    if (getWindow().getDecorView().getSystemUiVisibility() == View.INVISIBLE){
//                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);

                    }
                    isResume=false;
                }
                else {
                    getWindow().getDecorView().setSystemUiVisibility(View.INVISIBLE);
                    isResume=true;


                }
            }
        });
    }


    private RcvModel rcvModel;
    private SurfaceView surfaceView;
    private int iPreviewID = -1;
    private int chanel;
    private ProgressBar progressBar;

    @Override
    protected void initData() {
        Intent intent = getIntent();
        final String cameraIndexCode = intent.getStringExtra("cameraIndexCode");
//        String creamname = intent.getStringExtra("name");
//        t_name.setText(creamname);
        rcvModel.getPlayUrl(cameraIndexCode,0);
        rcvModel.getUrl().observe(this, new Observer<PlayData>() {
            @Override
            public void onChanged(PlayData data) {
                startplay(data.getData());
            }
        });

    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_start_play;
    }

    private void startplay(final String url) {
//        mPlayer.stopPlay();
//            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        new Thread() {
            @Override
            public void run() {
                if (!mPlayer.startRealPlay(url, StartPlayActivity.this)) {
                    onPlayerStatus(HikVideoPlayerCallback.Status.FAILED, mPlayer.getLastError());
                }

            }
        }.start();
    }

    private boolean isResume = true;

    @Override
    public void onResume() {
        isResume = true;
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {


            }
        }, 1000);
        super.onResume();
    }

    @Override
    public void onPause() {
        new Thread(){
            @Override
            public void run() {
                mPlayer.stopPlay();
            }
        }.start();

        super.onPause();
    }

    @Override
    public void onPlayerStatus(final Status status, final int i) {
        if (status == HikVideoPlayerCallback.Status.FAILED || status == Status.EXCEPTION) {
            Log.e("FAILED", i + "---");
            mPlayer.stopPlay();
        }
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                if (status == HikVideoPlayerCallback.Status.FAILED || status == Status.EXCEPTION) {
                    Log.e("FAILED", i + "---");
                   playTErr.setText(MessageFormat.format("播放异常，错误码：{0}", Integer.toHexString(i)));
                }
            }
        });
    }
}
