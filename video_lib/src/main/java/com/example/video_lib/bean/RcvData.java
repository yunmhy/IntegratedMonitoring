package com.example.video_lib.bean;


import com.google.gson.annotations.SerializedName;

import java.util.List;


public class RcvData  {


    /**
     * msg : 查询成功
     * code : 0
     * data : [{"cameraIndexCode":"954ae6b8341c4aa7b0ccada79466119f","cameraName":"保利铁新煤业通风机房","regionIndexCode":"d23619f8-dd34-4491-a85e-11c25a818ada","url":"http://172.17.144.3:83/openUrl/k61gbLy/live.m3u8"},{"cameraIndexCode":"f5f86f3d32114d878e1884bee43180c5","cameraName":"保利铁新煤业西皮机头","regionIndexCode":"d23619f8-dd34-4491-a85e-11c25a818ada","url":"http://172.17.144.3:83/openUrl/k60GZvq/live.m3u8"},{"cameraIndexCode":"fa651fb3d00c4e5a950c0a996be528a8","cameraName":"保利铁新煤业三井皮带","regionIndexCode":"d23619f8-dd34-4491-a85e-11c25a818ada","url":"http://172.17.144.3:83/openUrl/k6jZIlO/live.m3u8"},{"cameraIndexCode":"fa70adf173b04c13a962b3bd864837ce","cameraName":"保利铁新煤业压风机房","regionIndexCode":"d23619f8-dd34-4491-a85e-11c25a818ada","url":"http://172.17.144.3:83/openUrl/k6l86S4/live.m3u8"},{"cameraIndexCode":"fb29b800f15c4f1d8d0566f576f30f37","cameraName":"保利铁新煤业410变电所","regionIndexCode":"d23619f8-dd34-4491-a85e-11c25a818ada","url":"http://172.17.144.3:83/openUrl/k60GZvr/live.m3u8"}]
     */

    private String msg;
    private int code;
    private List<DataBean> data;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * cameraIndexCode : 954ae6b8341c4aa7b0ccada79466119f
         * cameraName : 保利铁新煤业通风机房
         * regionIndexCode : d23619f8-dd34-4491-a85e-11c25a818ada
         * url_hls : http://172.17.144.3:83/openUrl/e5YhMpr/live.m3u8
         * url_rtmp : rtmp://172.17.144.3:1935/live/openUrl/e67mWzu
         * url_rtsp : rtsp://172.17.144.3:554/openUrl/e6gJHRC
         */

        private String cameraIndexCode;
        private String cameraName;
        private String regionIndexCode;
        private String url_hls;
        private String url_rtmp;
        private String url_rtsp;
        public PlayerStatus playerStatus=PlayerStatus.IDLE;
        public String getCameraIndexCode() {
            return cameraIndexCode;
        }

        public void setCameraIndexCode(String cameraIndexCode) {
            this.cameraIndexCode = cameraIndexCode;
        }

        public String getCameraName() {
            return cameraName;
        }

        public void setCameraName(String cameraName) {
            this.cameraName = cameraName;
        }

        public String getRegionIndexCode() {
            return regionIndexCode;
        }

        public void setRegionIndexCode(String regionIndexCode) {
            this.regionIndexCode = regionIndexCode;
        }

        public String getUrl_hls() {
            return url_hls;
        }

        public void setUrl_hls(String url_hls) {
            this.url_hls = url_hls;
        }

        public String getUrl_rtmp() {
            return url_rtmp;
        }

        public void setUrl_rtmp(String url_rtmp) {
            this.url_rtmp = url_rtmp;
        }

        public String getUrl_rtsp() {
            return url_rtsp;
        }

        public void setUrl_rtsp(String url_rtsp) {
            this.url_rtsp = url_rtsp;
        }
    }
}
