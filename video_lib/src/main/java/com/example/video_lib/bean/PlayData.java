package com.example.video_lib.bean;

public class PlayData {

    /**
     * msg : 查询成功
     * code : 0
     * data : rtsp://172.17.144.3:554/openUrl/HjvovwQ
     */

    private String msg;
    private int code;
    private String data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
