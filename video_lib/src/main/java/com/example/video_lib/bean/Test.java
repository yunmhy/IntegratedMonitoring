package com.example.video_lib.bean;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.SurfaceTexture;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.TextureView;
import android.view.View;

import androidx.annotation.NonNull;

import com.chad.library.adapter.base.BaseViewHolder;
import com.example.video_lib.R;
import com.example.video_lib.StartPlayActivity;
import com.example.video_lib.ViedoMainActivity;
import com.example.video_lib.widget.AutoHideView;
import com.hikvision.open.hikvideoplayer.HikVideoPlayer;
import com.hikvision.open.hikvideoplayer.HikVideoPlayerCallback;
import com.hikvision.open.hikvideoplayer.HikVideoPlayerFactory;

import java.text.MessageFormat;
import java.util.List;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

public class Test implements HikVideoPlayerCallback, TextureView.SurfaceTextureListener {
    public HikVideoPlayer mPlayer;
    public RcvData.DataBean dataBeans;
    public PlayerStatus mPlayerStatus = PlayerStatus.IDLE;
    public BaseViewHolder viewHolder;
    private Activity activity;

    public Test(RcvData.DataBean dataBeans) {
        this.dataBeans = dataBeans;
        mPlayer = HikVideoPlayerFactory.provideHikVideoPlayer();

    }

    public void play(final BaseViewHolder holder, TextureView textureView) {

        View view = holder.getView(R.id.rcv_sfv);
        final AutoHideView autoHideView=holder.getView(R.id.auto_hide_view);
        this.activity = (Activity) view.getContext();
        holder.setText(R.id.tv_chanel, dataBeans.getCameraName());
        viewHolder = holder;
        if(dataBeans.getUrl_rtsp()==null){
            viewHolder.setVisible(R.id.t_err, true);
            viewHolder.setText(R.id.t_err, "无信号");
            viewHolder.setGone(R.id.item_progress, false);
            return;
        }
        textureView.setSurfaceTextureListener(this);
//        play();
        textureView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mPlayerStatus==PlayerStatus.SUCCESS){

//                    Intent intent = new Intent(activity, StartPlayActivity.class);
//                    intent.putExtra("cameraIndexCode", dataBeans.getCameraIndexCode());
//                    startActivity(intent);
                }

            }
        });
    }

    private void play() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mPlayerStatus = PlayerStatus.LOADING;
                viewHolder.setGone(R.id.item_progress, true);
                if (dataBeans.getUrl_rtsp() != null) {
                    if (!mPlayer.startRealPlay(dataBeans.getUrl_rtsp(), Test.this)) {
                        onPlayerStatus(Status.FAILED, mPlayer.getLastError());
                    }
                }

            }
        }).start();
    }

    @Override
    public void onPlayerStatus(final Status status, final int i) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (status) {
                    case EXCEPTION:
                        stop();
                        viewHolder.setVisible(R.id.t_err, true);
                        viewHolder.setText(R.id.t_err, MessageFormat.format("取流发生异常，错误码：{0}", Integer.toHexString(i)));
                        break;
                    case FAILED:
                        viewHolder.setVisible(R.id.t_err, true);
                        viewHolder.setText(R.id.t_err, MessageFormat.format("播放发生异常，错误码：{0}", Integer.toHexString(i)));
                        stop();
                        break;
                    case FINISH:
                        break;
                    case SUCCESS:
                        viewHolder.setGone(R.id.t_err, false);
                        mPlayerStatus = PlayerStatus.SUCCESS;
                        break;
                }
                viewHolder.setGone(R.id.item_progress, false);

            }
        });


    }


    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        mPlayer.setSurfaceTexture(surface);
        if(mPlayerStatus!=PlayerStatus.FAILED||mPlayerStatus!=PlayerStatus.EXCEPTION){
            play();
        }

    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {

    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        stop();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    public void stop() {

        new Thread() {
            @Override
            public void run() {
                mPlayer.stopPlay();
            }
        }.start();
    }
}
