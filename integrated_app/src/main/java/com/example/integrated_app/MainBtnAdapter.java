package com.example.integrated_app;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.base_lib.baen.MyConstants;
import com.example.rydw_lib.RydwMainActivity;
import com.example.safe_lib.SafeMainActivity;
import com.example.video_lib.ViedoMainActivity;

import java.util.List;

public class MainBtnAdapter extends BaseQuickAdapter<Btnbean, BaseViewHolder> {
    Activity activity;
    public MainBtnAdapter(Activity activity,int layoutResId, @Nullable List<Btnbean> data) {
        super(layoutResId, data);
        this.activity=activity;

    }

    @Override
    protected void convert(BaseViewHolder helper, final Btnbean item) {
        helper.addOnClickListener(R.id.btn_id);
        final int btn_id=item.getBtnname();
        switch (btn_id) {
            case MyConstants.SAFE:
                helper.setText(R.id.btn_txt,"安全监控");
                if (item.isBtntype()){
                    helper.setBackgroundRes(R.id.btn_id, R.drawable.safe);
                    if(item.isUpload()){
                        helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload1);
                    }else  helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload2);
                }else  helper.setBackgroundRes(R.id.btn_id, R.drawable.safe1);

                break;
            case MyConstants.PERSON:
                helper.setText(R.id.btn_txt,"人员定位");
                if (item.isBtntype()){
                    helper.setBackgroundRes(R.id.btn_id, R.drawable.person);
                    if(item.isUpload()){
                        helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload1);
                    }else  helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload2);
                }else  helper.setBackgroundRes(R.id.btn_id, R.drawable.person1);
                break;
            case MyConstants.VIDEO:
                helper.setText(R.id.btn_txt,"工业视频");
                if (item.isBtntype()){
                    helper.setBackgroundRes(R.id.btn_id, R.drawable.video);
                    if(item.isUpload()){
                        helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload1);
                    }else  helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload2);
                }else  helper.setBackgroundRes(R.id.btn_id, R.drawable.video1);
                break;
            case MyConstants.PRODUCT:
                helper.setText(R.id.btn_txt,"产量监控");
                if (item.isBtntype()){
                    helper.setBackgroundRes(R.id.btn_id, R.drawable.product);
                    if(item.isUpload()){
                        helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload1);
                    }else  helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload2);
                }else  helper.setBackgroundRes(R.id.btn_id, R.drawable.product1);

                break;
            case MyConstants.DEVICE:
                helper.setText(R.id.btn_txt,"重型设备");
                if (item.isBtntype()){
                    helper.setBackgroundRes(R.id.btn_id, R.drawable.device);
                    if(item.isUpload()){
                        helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload1);
                    }else  helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload2);
                }else  helper.setBackgroundRes(R.id.btn_id, R.drawable.device1);

                break;
            case MyConstants.RESCUE:
                helper.setText(R.id.btn_txt,"救援管理");
                if (item.isBtntype()){
                    helper.setBackgroundRes(R.id.btn_id, R.drawable.rescue);
                    if(item.isUpload()){
                        helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload1);
                    }else  helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload2);
                }else  helper.setBackgroundRes(R.id.btn_id, R.drawable.rescue1);

                break;
            case MyConstants.WHATE:
                helper.setText(R.id.btn_txt,"水文监测");
                if (item.isBtntype()){
                    helper.setBackgroundRes(R.id.btn_id, R.drawable.whate);
                    if(item.isUpload()){
                        helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload1);
                    }else  helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload2);
                }else  helper.setBackgroundRes(R.id.btn_id, R.drawable.whate1);

                break;
            case MyConstants.KZDYL:
                helper.setText(R.id.btn_txt,"矿震和地应力");
                if (item.isBtntype()){
                    helper.setBackgroundRes(R.id.btn_id, R.drawable.kzdyl);
                    if(item.isUpload()){
                        helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload1);
                    }else  helper.setBackgroundRes(R.id.upload_stats, R.drawable.upload2);
                }else  helper.setBackgroundRes(R.id.btn_id, R.drawable.kzdyl1);

                break;
            default:
                break;

        }

    }

}
