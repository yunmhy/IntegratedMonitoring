package com.example.integrated_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import android.widget.ImageView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.example.base_lib.activity.BaseActivity;
import com.example.login_lib.activity.LoginActivity;



public class LeadActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startActivity(new Intent(LeadActivity.this,LoginActivity.class));
        finish();
    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initData() {
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_lead;
    }
}
