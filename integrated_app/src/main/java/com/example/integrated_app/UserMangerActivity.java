package com.example.integrated_app;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.base_lib.activity.BaseActivity;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.utils.MyViewOutlineProvider;
import com.example.login_lib.activity.LoginActivity;
//import com.sxygsj.android.common_hr.entity.HRFaceInfo;

public class UserMangerActivity extends BaseActivity {
    private TextView textUserName;
    private TextView txtName;
    private TextView txtCompany;
    private TextView txtZw;

    private Button btnExit;
    private TextView txtCompanyId;
    private TextView txtTel;





    @Override
    protected void initView() {
        setBarColor(true);
        getToolbarTitle().setText("个人信息");
//        findViewById(R.id.user_back).setBackground(new WaterMarkBg(baseuserInfo.getUseridname()));
        txtCompany = (TextView) findViewById(R.id.txt_company);
        txtCompanyId = (TextView) findViewById(R.id.txt_company_id);
        textUserName = (TextView) findViewById(R.id.txt_username);
        txtZw = (TextView) findViewById(R.id.txt_zw);
        txtTel = (TextView) findViewById(R.id.txt_tel);
        txtName = (TextView) findViewById(R.id.txt_name);
        btnExit = (Button) findViewById(R.id.btn_exit);
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserMangerActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });
        findViewById(R.id.updatepwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(UserMangerActivity.this, UpdatePwdActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void initData() {
        Intent intent =getIntent();
        txtName.setText(baseuserInfo.getUsername());
        txtCompany.setText(baseuserInfo.getUseridname());
        txtCompanyId.setText(baseuserInfo.getCode());
        textUserName.setText(baseuserInfo.getComename());
        txtZw.setText(baseuserInfo.getPost());
        txtTel.setText(baseuserInfo.getTel());

    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_user_manger;
    }
}
