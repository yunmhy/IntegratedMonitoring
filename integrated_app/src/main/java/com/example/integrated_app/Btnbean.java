package com.example.integrated_app;

public class Btnbean {
    private int btnname;
    private boolean btntype;
    private boolean upload;

    public Btnbean(int btnname, boolean btntype, boolean upload) {
        this.btnname = btnname;
        this.btntype = btntype;
        this.upload = upload;
    }

    public int getBtnname() {
        return btnname;
    }

    public void setBtnname(int btnname) {
        this.btnname = btnname;
    }

    public boolean isBtntype() {
        return btntype;
    }

    public void setBtntype(boolean btntype) {
        this.btntype = btntype;
    }

    public boolean isUpload() {
        return upload;
    }

    public void setUpload(boolean upload) {
        this.upload = upload;
    }
}
