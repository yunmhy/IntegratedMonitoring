package com.example.integrated_app;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.alibaba.android.arouter.launcher.ARouter;
import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.activity.BaseActivity;
import com.example.base_lib.baen.ARConstants;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.login_lib.LoginErrDialog;
import com.example.login_lib.activity.LoginActivity;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

import org.json.JSONException;
import org.json.JSONObject;

public class UpdatePwdActivity extends BaseActivity implements View.OnFocusChangeListener, View.OnClickListener {
    private EditText oldPwd;
    private ImageView oldLine;
    private EditText newPwd;
    private ImageView newLine;
    private EditText sureNewPwd;
    private ImageView sureLine;
    private Button updateSure;
    private ImageView oldHide;
    private ImageView oldDel;
    private ImageView newHide;
    private ImageView newDel;
    private ImageView sureHide;
    private ImageView sureDel;

    @Override
    protected void initView() {
        setBarColor(true);
        getToolbarTitle().setTextColor(Color.BLACK);
        getToolbarTitle().setText("修改密码");
        oldPwd = (EditText) findViewById(R.id.old_pwd);
        oldLine = (ImageView) findViewById(R.id.old_line);
        newPwd = (EditText) findViewById(R.id.new_pwd);
        newLine = (ImageView) findViewById(R.id.new_line);
        sureNewPwd = (EditText) findViewById(R.id.sure_new_pwd);
        sureLine = (ImageView) findViewById(R.id.sure_line);
        updateSure = (Button) findViewById(R.id.update_sure);
        oldHide = (ImageView) findViewById(R.id.old_hide);
        oldDel = (ImageView) findViewById(R.id.old_del);
        newHide = (ImageView) findViewById(R.id.new_hide);
        newDel = (ImageView) findViewById(R.id.new_del);
        sureHide = (ImageView) findViewById(R.id.sure_hide);
        sureDel = (ImageView) findViewById(R.id.sure_del);
        oldPwd.setOnFocusChangeListener(this);
        newPwd.setOnFocusChangeListener(this);
        sureNewPwd.setOnFocusChangeListener(this);
        oldPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isButtonEnable();
            }
        });
        newPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isButtonEnable();
            }
        });
        sureNewPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                isButtonEnable();
            }
        });
        updateSure.setOnClickListener(this);
        oldHide.setOnClickListener(this);
        oldDel.setOnClickListener(this);
        newHide.setOnClickListener(this);
        newDel.setOnClickListener(this);
        sureHide.setOnClickListener(this);
        sureDel.setOnClickListener(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_update_pwd;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        switch (v.getId()) {
            case R.id.old_pwd:
                if (hasFocus) {
                    oldHide.setVisibility(View.VISIBLE);
                    oldDel.setVisibility(View.VISIBLE);
                    oldLine.setBackground(getResources().getDrawable(R.color.update_blue));
                } else {
                    oldHide.setVisibility(View.INVISIBLE);
                    oldDel.setVisibility(View.INVISIBLE);
                    oldLine.setBackground(getResources().getDrawable(R.color.bcolor));
                }
                break;
            case R.id.new_pwd:
                if (hasFocus) {
                    newHide.setVisibility(View.VISIBLE);
                    newDel.setVisibility(View.VISIBLE);
                    newLine.setBackground(getResources().getDrawable(R.color.update_blue));
                } else {
                    newHide.setVisibility(View.INVISIBLE);
                    newDel.setVisibility(View.INVISIBLE);
                    newLine.setBackground(getResources().getDrawable(R.color.bcolor));
                }
                break;
            case R.id.sure_new_pwd:
                if (hasFocus) {
                    sureHide.setVisibility(View.VISIBLE);
                    sureDel.setVisibility(View.VISIBLE);
                    sureLine.setBackground(getResources().getDrawable(R.color.update_blue));
                } else {
                    sureHide.setVisibility(View.INVISIBLE);
                    sureDel.setVisibility(View.INVISIBLE);
                    sureLine.setBackground(getResources().getDrawable(R.color.bcolor));
                }
                break;
        }
    }

    public void isButtonEnable() {
        if (oldPwd.length() > 0 && newPwd.length() > 0 && sureNewPwd.length() > 0) {
            updateSure.setEnabled(true);
        } else {
            updateSure.setEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.old_hide:
                showhide(oldPwd, oldHide);
                break;
            case R.id.old_del:
                oldPwd.setText(null);
                break;
            case R.id.new_hide:
                showhide(newPwd, newHide);
                break;
            case R.id.new_del:
                newPwd.setText(null);
                break;
            case R.id.sure_hide:
                showhide(sureNewPwd, sureHide);
                break;
            case R.id.sure_del:
                sureNewPwd.setText(null);
                break;
            case R.id.update_sure:
                updatePwd();
                break;
            default:
                break;
        }
    }

    private void updatePwd() {
        String oldpwd = oldPwd.getText().toString().trim();
        String newpwd = newPwd.getText().toString().trim();//填好了才有值;
        String surepwd=sureNewPwd.getText().toString().trim();
        // Log.e("user+pwd", "onClick: "+"username:"+username+" pwd:"+pwd);
        if (oldpwd.length() > 0 && newpwd.length() > 0&& surepwd.length() > 0) {
            HttpHeaders headers = new HttpHeaders();
            headers.put("token",baseuserInfo.getToken());
            HttpParams paramsGet = new HttpParams();
            paramsGet.put("oldPwd", oldpwd);
            paramsGet.put("newPwd", newpwd);
            paramsGet.put("confirmPwd", surepwd);
            OkGoBuilder.getInstance()
                    .Builder(this)
                    .baseurl(MyConstants.TestUrl)
                    .inurl("user/pwd")
                    .method(OkGoBuilder.GET)
                    .dialogshow(true)
                    .params(paramsGet)
                        .headers(headers)
                    .cls(String.class)
                    .callback(new MyCallback<String>() {
                        @Override
                        public void onSuccess(String res, String id) {
                            Log.i("TAG",res);
                        }

                        @Override
                        public void onError(String e, String id) {
                            try {
                                JSONObject jsonObject=new JSONObject(e);
                                String msg=jsonObject.getString("msg");
                                new LoginErrDialog(msg).show(getSupportFragmentManager(), "err_dialog");
                            } catch (JSONException ex) {
                                ex.printStackTrace();
                            }


                        }
                    })
                    .build();

        } else {
            Toast.makeText(this, "请填写用户名或密码", Toast.LENGTH_SHORT).show();
        }
    }

private void showhide(EditText oldPwd,ImageView oldHide){
        if(oldPwd.getTransformationMethod()!=HideReturnsTransformationMethod.getInstance()){
        oldHide.setImageResource(R.mipmap.pwd_show);
        oldPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        oldPwd.setSelection(oldPwd.getText().toString().length());
        }else{
        oldHide.setImageResource(R.mipmap.pwd_hide);
        oldPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
        oldPwd.setSelection(oldPwd.getText().toString().length());
        }
        }
        }
