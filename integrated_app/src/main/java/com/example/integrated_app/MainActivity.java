package com.example.integrated_app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.TimePickerView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.activity.BaseActivity;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.ARConstants;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.myserver.MessageService;
import com.example.base_lib.utils.FileUtil;
import com.example.base_lib.utils.MyViewOutlineProvider;
import com.example.rydw_lib.RydwMainActivity;
import com.example.safe_lib.SafeMainActivity;
import com.example.video_lib.ViedoMainActivity;
import com.lzy.okgo.model.HttpParams;
//import com.sxygsj.android.common_hr.entity.HRFaceInfo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

// 在支持路由的页面上添加注解(必选)
// 这里的路径需要注意的是至少需要有两级，/xx/xx
@Route(path = ARConstants.APP_MAIN)
public class MainActivity extends BaseActivity  {


    private ImageView headImg;
    private TextView textUsername;
    private RecyclerView btnRcv;
    private TextView txtMainZw;
    private MainBtnAdapter adapter;
    private static final String TAG = "MainActivity";
//    @Autowired(name = "hrFaceInfo")
//    HRFaceInfo hrFaceInfo;
    private List<Btnbean> list;


    @Override
    protected void initView() {
        startService(new Intent(this,MessageService.class));
        getToolbar().setNavigationIcon(null);
        getToolbar().setBackground(getDrawable(R.drawable.toolbartop));
        getToolbarTitle().setText("个人中心");
        getToolbarTitle().setTextColor(Color.WHITE);
        setTbM(false, R.id.menu_menu);
        headImg = (ImageView) findViewById(R.id.head_img);
        textUsername = (TextView) findViewById(R.id.text_username);
        txtMainZw = (TextView) findViewById(R.id.txt_main_zw);
        btnRcv=(RecyclerView)findViewById(R.id.btn_rcv);

//        btnRcv.setBackground(new WaterMarkBg(baseuserInfo.getUseridname()));
        headImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, UserMangerActivity.class);
//                intent.putExtra("hrFaceInfo", hrFaceInfo);
                startActivity(intent);
            }
        });
    }

    private void getOnlineStates() {
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("", "");
        OkGoBuilder.getInstance()
                .Builder(this)
                .baseurl(MyConstants.BASERURL)
                .inurl("")
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .cls(Object.class)
                .callback(new MyCallback<Object>() {
                    @Override
                    public void onSuccess(Object object, String id) {
                        Log.i(TAG, "onSuccess1: " + object);

                    }

                    @Override
                    public void onError(String e, String id) {
                        Log.i(TAG, "onError: " + e);

                    }
                })
                .build();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void initData() {
        list = new ArrayList<>();
        getBtnData();
        headImg.setOutlineProvider(new MyViewOutlineProvider());
        headImg.setClipToOutline(true);
//        headImg.setImageURI(FileUtil.getFileUri(this, hrFaceInfo.getFacefile()));
//        Log.i(TAG, baseuserInfo.getUsername());
//        getOnlineStates();
        textUsername.setText(baseuserInfo.getUsername());
        txtMainZw.setText(baseuserInfo.getPost());
        adapter=new MainBtnAdapter(this,R.layout.main_btn_itme, list);
        adapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {

            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                Btnbean item = (Btnbean) adapter.getItem(position);
                Intent intent;
                switch (item.getBtnname()) {
                    case MyConstants.SAFE:
                        intent = new Intent(MainActivity.this, SafeMainActivity.class);
//                intent.putExtra("userinfo",baseuserInfo);
                        startActivity(intent);
                        break;
                    case MyConstants.PERSON:
                        intent = new Intent(MainActivity.this, RydwMainActivity.class);
//                intent.putExtra("userinfo",baseuserInfo);
                        startActivity(intent);
                        break;
                    case MyConstants.VIDEO:
                        intent = new Intent(MainActivity.this, ViedoMainActivity.class);
//                intent.putExtra("userinfo",baseuserInfo);
                        startActivity(intent);
                        break;
                    case MyConstants.PRODUCT:

                        dialogShow();
                        break;
                    case MyConstants.DEVICE:
                        dialogShow();
                        break;
                    case MyConstants.RESCUE:
                        dialogShow();
                        break;
                    case MyConstants.WHATE:
                        dialogShow();
                        break;
                    case MyConstants.KZDYL:
                        dialogShow();
                        break;
                    default:
                        break;
                }
            }
        });
        btnRcv.setAdapter(adapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4);
        btnRcv.setLayoutManager(gridLayoutManager);


    }

    private void getBtnData() {
        list.add(new Btnbean(1,true,true));
        list.add(new Btnbean(2,true,false));
        list.add(new Btnbean(3,false,false));
        list.add(new Btnbean(4,true,true));
        list.add(new Btnbean(5,false,false));
        list.add(new Btnbean(6,false,false));
        list.add(new Btnbean(7,false,false));
        list.add(new Btnbean(8,false,false));

    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_main;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        return super.onCreateOptionsMenu(menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        getToolbarTitle().setText(baseuserInfo.getUnitinfo().getNickname());

    }



    public void dialogShow() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("系统正在开发中...").setPositiveButton("确定", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
