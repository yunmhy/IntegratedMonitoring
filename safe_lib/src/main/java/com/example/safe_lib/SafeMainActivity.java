package com.example.safe_lib;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.alibaba.android.arouter.facade.annotation.Autowired;
import com.alibaba.android.arouter.facade.annotation.Route;
import com.example.base_lib.activity.BaseActivity;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.ARConstants;
import com.example.base_lib.baen.UserInfo;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import java.lang.reflect.Field;

import static androidx.constraintlayout.widget.Constraints.TAG;

// 在支持路由的页面上添加注解(必选)
// 这里的路径需要注意的是至少需要有两级，/xx/xx
@Route(path = ARConstants.APP_SAFE)
public class SafeMainActivity extends BaseActivity {
    private BottomNavigationView navView;
    private NavController navController;

//    @Autowired(name = "userinfo")
//    UserInfo userInfo;

    @SuppressLint("ResourceType")
    @Override
    protected void initView() {

        getToolbar().setBackground(getDrawable(R.color.blue));
        setTbM(false,R.id.menu_safe);
//        setBarColor(true);
        navView = findViewById(R.id.nav_view);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navView.setItemTextColor(getResources().getColorStateList(R.drawable.nav_menu_text_color));
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupWithNavController(navView, navController);

        initIcon();
        navView.setItemIconTintList(null);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Navigation.findNavController(view).popBackStack();
                onBackPressed();
            }
        });

    }



    @Override
    protected void initData() {
//        Intent intent=getIntent();
//        userInfo = (UserInfo)intent.getSerializableExtra("userinfo");

    }
    public  UserInfo toValue(){
        return  baseuserInfo;
    }
    @Override
    protected int layoutResId() {
        return R.layout.activity_safe_main;
    }

    @Override
    protected void onResume() {
        getToolbarTitle().setText("安全监控");
        getToolbarTitle().setTextColor(getResources().getColor(R.color.white));
        super.onResume();
    }
    public void  initIcon(){
        resetToDefaultIcon();//重置到默认不选中图片
        NavDestination navDestination = navController.getCurrentDestination(); //获取当前目的地的信息
        int selectedItemId = navDestination.getId();
        if (selectedItemId == R.id.navigation_alarm) {//在这里替换图标
            navView.getMenu().findItem(selectedItemId).setIcon(R.mipmap.safealarm2);
        }
        if (selectedItemId == R.id.navigation_chart) {//在这里替换图标
            navView.getMenu().findItem(selectedItemId).setIcon(R.mipmap.linechart2);


        }
        if (selectedItemId == R.id.navigation_history) {//在这里替换图标
            navView.getMenu().findItem(selectedItemId).setIcon(R.mipmap.history2);


        }
        if (selectedItemId == R.id.navigation_list) {//在这里替换图标
            navView.getMenu().findItem(selectedItemId).setIcon(R.mipmap.list2);


        }
    }
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            resetToDefaultIcon();//重置到默认不选中图片
            NavigationUI.onNavDestinationSelected(item,navController);
            if (item.getItemId() == R.id.navigation_alarm) {//在这里替换图标
                item.setIcon(R.mipmap.safealarm2);
                return true;
            }
            if (item.getItemId() == R.id.navigation_chart) {//在这里替换图标
                item.setIcon(R.mipmap.linechart2);
                return true;
            }
            if (item.getItemId() == R.id.navigation_history) {//在这里替换图标
                item.setIcon(R.mipmap.history2);
                return true;
            }
            if (item.getItemId() == R.id.navigation_list) {//在这里替换图标
                item.setIcon(R.mipmap.list2);
                return true;
            }

            return false;
        }

    };

    private void resetToDefaultIcon() {
        MenuItem alarm =  navView.getMenu().findItem(R.id.navigation_alarm);
        alarm.setIcon(R.mipmap.safealarm1);
        MenuItem chart =  navView.getMenu().findItem(R.id.navigation_chart);
        chart.setIcon(R.mipmap.linechart1);
        MenuItem list =  navView.getMenu().findItem(R.id.navigation_list);
        list.setIcon(R.mipmap.list1);
        MenuItem history =  navView.getMenu().findItem(R.id.navigation_history);
        history.setIcon(R.mipmap.history1);
    }
    @Override
    public void onBackPressed() {
        if(navView.getSelectedItemId()==R.id.navigation_alarm){
            super.onBackPressed();
        }
        else {
            navView.setSelectedItemId(R.id.navigation_alarm);

        }
    }
}
