package com.example.safe_lib.bean;

import java.util.List;

public class SafeHistoryBean {


    /**
     * msg : 查询成功
     * code : 0
     * data : [{"sensorname":"环境瓦斯","alarmnum":0},{"sensorname":"一氧化碳","alarmnum":0},{"sensorname":"二氧化碳","alarmnum":0},{"sensorname":"粉尘","alarmnum":0},{"sensorname":"管道瓦斯","alarmnum":0},{"sensorname":"全量程甲烷","alarmnum":0},{"sensorname":"烟雾","alarmnum":0},{"sensorname":"管道一氧化碳","alarmnum":0},{"sensorname":"激光甲烷","alarmnum":0},{"sensorname":"风门","alarmnum":1},{"sensorname":"主通风机","alarmnum":0}]
     */

    private String msg;
    private int code;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * sensorname : 环境瓦斯
         * alarmnum : 0
         */

        private String sensorname;
        private int alarmnum;

        public String getSensorname() {
            return sensorname;
        }

        public void setSensorname(String sensorname) {
            this.sensorname = sensorname;
        }

        public int getAlarmnum() {
            return alarmnum;
        }

        public void setAlarmnum(int alarmnum) {
            this.alarmnum = alarmnum;
        }
    }
}
