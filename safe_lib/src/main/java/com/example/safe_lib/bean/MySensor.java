package com.example.safe_lib.bean;

import java.util.List;

public class MySensor {

    private List<String> sensorname;
    private List<List<String>> sensorcode;

    public MySensor(List<String> sensorname, List<List<String>> sensorcode) {
        this.sensorname = sensorname;
        this.sensorcode = sensorcode;
    }

    public List<String> getSensorname() {
        return sensorname;
    }

    public void setSensorname(List<String> sensorname) {
        this.sensorname = sensorname;
    }

    public List<List<String>> getSensorcode() {
        return sensorcode;
    }

    public void setSensorcode(List<List<String>> sensorcode) {
        this.sensorcode = sensorcode;
    }
}
