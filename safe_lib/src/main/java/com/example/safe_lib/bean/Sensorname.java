package com.example.safe_lib.bean;

import java.util.List;

public class Sensorname {

    /**
     * msg : 查询成功
     * code : 0
     * data : [{"id":1,"sensortype":1,"sensorcode":"0001","sensorname":"环境瓦斯","importantlevel":1,"note":null,"isuse":null,"sort":1},{"id":4,"sensortype":1,"sensorcode":"0004","sensorname":"一氧化碳","importantlevel":1,"note":null,"isuse":null,"sort":2},{"id":22,"sensortype":1,"sensorcode":"0022","sensorname":"管道瓦斯","importantlevel":1,"note":null,"isuse":null,"sort":3},{"id":38,"sensortype":1,"sensorcode":"0038","sensorname":"全量程甲烷","importantlevel":1,"note":null,"isuse":null,"sort":3},{"id":14,"sensortype":1,"sensorcode":"0014","sensorname":"粉尘","importantlevel":1,"note":null,"isuse":null,"sort":4},{"id":72,"sensortype":2,"sensorcode":"1001","sensorname":"局部通风机 ","importantlevel":0,"note":null,"isuse":null,"sort":5},{"id":81,"sensortype":2,"sensorcode":"1010","sensorname":"主通风机","importantlevel":1,"note":null,"isuse":null,"sort":6},{"id":73,"sensortype":2,"sensorcode":"1002","sensorname":"风门","importantlevel":1,"note":null,"isuse":null,"sort":7},{"id":74,"sensortype":2,"sensorcode":"1003","sensorname":"风筒状态","importantlevel":0,"note":null,"isuse":null,"sort":8},{"id":39,"sensortype":1,"sensorcode":"0039","sensorname":"烟雾","importantlevel":1,"note":null,"isuse":null,"sort":9},{"id":2,"sensortype":1,"sensorcode":"0002","sensorname":"风速","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":3,"sensortype":1,"sensorcode":"0003","sensorname":"环境温度","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":5,"sensortype":1,"sensorcode":"0005","sensorname":"风压","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":6,"sensortype":1,"sensorcode":"0006","sensorname":"负压","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":7,"sensortype":1,"sensorcode":"0007","sensorname":"水池水位","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":8,"sensortype":1,"sensorcode":"0008","sensorname":"煤位 ","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":9,"sensortype":1,"sensorcode":"0009","sensorname":"硫化氢","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":10,"sensortype":1,"sensorcode":"0010","sensorname":"水温度","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":11,"sensortype":1,"sensorcode":"0011","sensorname":"高低浓瓦斯","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":12,"sensortype":1,"sensorcode":"0012","sensorname":"氧气","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":13,"sensortype":1,"sensorcode":"0013","sensorname":"二氧化碳","importantlevel":1,"note":null,"isuse":null,"sort":null},{"id":15,"sensortype":1,"sensorcode":"0015","sensorname":"电压 ","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":16,"sensortype":1,"sensorcode":"0016","sensorname":"频率","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":17,"sensortype":1,"sensorcode":"0017","sensorname":"电流","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":18,"sensortype":1,"sensorcode":"0018","sensorname":"湿度","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":19,"sensortype":1,"sensorcode":"0019","sensorname":"风量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":20,"sensortype":1,"sensorcode":"0020","sensorname":"顶板离层位移","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":21,"sensortype":1,"sensorcode":"0021","sensorname":"坝体位移","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":23,"sensortype":1,"sensorcode":"0023","sensorname":"管道温度","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":24,"sensortype":1,"sensorcode":"0024","sensorname":"水质","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":25,"sensortype":1,"sensorcode":"0025","sensorname":"管道压力","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":26,"sensortype":1,"sensorcode":"0026","sensorname":"轴承温度 ","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":27,"sensortype":1,"sensorcode":"0027","sensorname":"噪音","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":28,"sensortype":1,"sensorcode":"0028","sensorname":"电机温度","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":29,"sensortype":1,"sensorcode":"0029","sensorname":"水库水位","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":30,"sensortype":1,"sensorcode":"0030","sensorname":"浸润线","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":31,"sensortype":1,"sensorcode":"0031","sensorname":"降雨量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":32,"sensortype":1,"sensorcode":"0032","sensorname":"液压压力","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":33,"sensortype":1,"sensorcode":"0033","sensorname":"围岩应力","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":34,"sensortype":1,"sensorcode":"0034","sensorname":"钻孔应力","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":35,"sensortype":1,"sensorcode":"0035","sensorname":"锚杆应力","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":36,"sensortype":1,"sensorcode":"0036","sensorname":"混合瓦斯流量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":37,"sensortype":1,"sensorcode":"0037","sensorname":"纯瓦斯流量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":40,"sensortype":1,"sensorcode":"0040","sensorname":"氧化氮","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":41,"sensortype":1,"sensorcode":"0041","sensorname":"二氧化硫","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":42,"sensortype":1,"sensorcode":"0042","sensorname":"氢气","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":43,"sensortype":1,"sensorcode":"0043","sensorname":"流量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":44,"sensortype":1,"sensorcode":"0044","sensorname":"压差","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":45,"sensortype":1,"sensorcode":"0045","sensorname":"扭矩","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":46,"sensortype":1,"sensorcode":"0046","sensorname":"重量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":47,"sensortype":1,"sensorcode":"0047","sensorname":"转速","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":48,"sensortype":1,"sensorcode":"0048","sensorname":"速度","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":49,"sensortype":1,"sensorcode":"0049","sensorname":"水压","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":50,"sensortype":1,"sensorcode":"0050","sensorname":"振动","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":51,"sensortype":1,"sensorcode":"0051","sensorname":"风筒风量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":52,"sensortype":1,"sensorcode":"0052","sensorname":"风速风向","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":53,"sensortype":1,"sensorcode":"0053","sensorname":"电池电量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":54,"sensortype":1,"sensorcode":"0054","sensorname":"标混","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":55,"sensortype":1,"sensorcode":"0055","sensorname":"标纯","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":56,"sensortype":1,"sensorcode":"0056","sensorname":"管道一氧化碳","importantlevel":1,"note":null,"isuse":null,"sort":null},{"id":57,"sensortype":1,"sensorcode":"0057","sensorname":"氢气","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":58,"sensortype":1,"sensorcode":"0058","sensorname":"管道流量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":59,"sensortype":1,"sensorcode":"0059","sensorname":"二氧化氮","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":60,"sensortype":1,"sensorcode":"0060","sensorname":"激光甲烷","importantlevel":1,"note":null,"isuse":null,"sort":null},{"id":61,"sensortype":1,"sensorcode":"0061","sensorname":"氨气","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":62,"sensortype":1,"sensorcode":"0062","sensorname":"氮气","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":63,"sensortype":1,"sensorcode":"0063","sensorname":"乙烯","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":64,"sensortype":1,"sensorcode":"0064","sensorname":"乙烷","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":65,"sensortype":1,"sensorcode":"0065","sensorname":"压强","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":66,"sensortype":1,"sensorcode":"0066","sensorname":"液位","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":67,"sensortype":1,"sensorcode":"0067","sensorname":"物位","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":68,"sensortype":1,"sensorcode":"0068","sensorname":"开度","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":69,"sensortype":1,"sensorcode":"0069","sensorname":"高度","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":70,"sensortype":1,"sensorcode":"0070","sensorname":"流量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":71,"sensortype":1,"sensorcode":"0071","sensorname":"硫化氢浓度","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":75,"sensortype":2,"sensorcode":"1004","sensorname":"设备开停","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":76,"sensortype":2,"sensorcode":"1005","sensorname":"开关","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":77,"sensortype":2,"sensorcode":"1006","sensorname":"风向","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":78,"sensortype":2,"sensorcode":"1007","sensorname":"煤仓空满","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":79,"sensortype":2,"sensorcode":"1008","sensorname":"烟雾","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":80,"sensortype":2,"sensorcode":"1009","sensorname":"断电器","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":82,"sensortype":2,"sensorcode":"1011","sensorname":"馈电器","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":83,"sensortype":2,"sensorcode":"1012","sensorname":"皮带开停","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":84,"sensortype":2,"sensorcode":"1013","sensorname":"水泵开停","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":85,"sensortype":2,"sensorcode":"1014","sensorname":"电源状态","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":86,"sensortype":2,"sensorcode":"1015","sensorname":"绞车","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":87,"sensortype":2,"sensorcode":"1016","sensorname":"阀门","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":88,"sensortype":2,"sensorcode":"1017","sensorname":"报警器","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":89,"sensortype":2,"sensorcode":"1018","sensorname":"供水","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":90,"sensortype":2,"sensorcode":"1019","sensorname":"喷雾","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":91,"sensortype":2,"sensorcode":"1020","sensorname":"交直流","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":92,"sensortype":2,"sensorcode":"1021","sensorname":"开关柜","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":93,"sensortype":2,"sensorcode":"1022","sensorname":"交换机","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":94,"sensortype":2,"sensorcode":"1023","sensorname":"计量开停控制器","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":95,"sensortype":2,"sensorcode":"1024","sensorname":"控制量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":96,"sensortype":2,"sensorcode":"1025","sensorname":"管道缺水","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":97,"sensortype":2,"sensorcode":"1026","sensorname":"抽放泵开停","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":98,"sensortype":7,"sensorcode":"3001","sensorname":"产量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":99,"sensortype":7,"sensorcode":"3002","sensorname":"瓦斯抽放量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":100,"sensortype":7,"sensorcode":"3003","sensorname":"排水量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":101,"sensortype":7,"sensorcode":"3004","sensorname":"钩数","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":102,"sensortype":7,"sensorcode":"3005","sensorname":"水流量","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":103,"sensortype":7,"sensorcode":"3006","sensorname":"瓦斯量日累计(抽放)","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":104,"sensortype":7,"sensorcode":"3007","sensorname":"混和量日累计(抽放)","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":105,"sensortype":7,"sensorcode":"3008","sensorname":"产量班累计","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":106,"sensortype":7,"sensorcode":"3009","sensorname":"产量日累计","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":107,"sensortype":7,"sensorcode":"4001","sensorname":"分站","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":108,"sensortype":7,"sensorcode":"4002","sensorname":"断馈电器","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":109,"sensortype":7,"sensorcode":"4003","sensorname":"供电状态","importantlevel":0,"note":null,"isuse":null,"sort":null},{"id":110,"sensortype":7,"sensorcode":"4004","sensorname":"多路电源","importantlevel":0,"note":null,"isuse":null,"sort":null}]
     */

    private String msg;
    private int code;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * sensortype : 1
         * sensorcode : 0001
         * sensorname : 环境瓦斯
         * importantlevel : 1
         * note : null
         * isuse : null
         * sort : 1
         */

        private int id;
        private int sensortype;
        private String sensorcode;
        private String sensorname;
        private int importantlevel;
        private Object note;
        private Object isuse;
        private int sort;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getSensortype() {
            return sensortype;
        }

        public void setSensortype(int sensortype) {
            this.sensortype = sensortype;
        }

        public String getSensorcode() {
            return sensorcode;
        }

        public void setSensorcode(String sensorcode) {
            this.sensorcode = sensorcode;
        }

        public String getSensorname() {
            return sensorname;
        }

        public void setSensorname(String sensorname) {
            this.sensorname = sensorname;
        }

        public int getImportantlevel() {
            return importantlevel;
        }

        public void setImportantlevel(int importantlevel) {
            this.importantlevel = importantlevel;
        }

        public Object getNote() {
            return note;
        }

        public void setNote(Object note) {
            this.note = note;
        }

        public Object getIsuse() {
            return isuse;
        }

        public void setIsuse(Object isuse) {
            this.isuse = isuse;
        }

        public int getSort() {
            return sort;
        }

        public void setSort(int sort) {
            this.sort = sort;
        }
    }
}
