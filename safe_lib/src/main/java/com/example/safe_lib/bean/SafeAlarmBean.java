package com.example.safe_lib.bean;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SafeAlarmBean {


    /**
     * msg : 查询成功
     * code : 0
     * data : [{"stationnum":"014","sensornum":"014A09","starttime":"2020-03-07T16:29:36.000+0000","persisttime":682,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"12工作面回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"014","sensornum":"014A09","starttime":"2020-03-07T16:40:14.000+0000","persisttime":636,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"12工作面回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"014","sensornum":"014A09","starttime":"2020-03-07T16:49:02.000+0000","persisttime":631,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"12工作面回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"014","sensornum":"014A13","starttime":"2020-03-07T16:40:14.000+0000","persisttime":667,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"12回风水泵（240m）","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"014","sensornum":"014A13","starttime":"2020-03-07T16:49:02.000+0000","persisttime":600,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"12回风水泵（240m）","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"014","sensornum":"014A15","starttime":"2020-03-07T16:40:14.000+0000","persisttime":975,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"12回风临时避难硐室(外）","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"014","sensornum":"014A15","starttime":"2020-03-07T16:48:20.000+0000","persisttime":612,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"12回风临时避难硐室(外）","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"014","sensornum":"014A16","starttime":"2020-03-07T16:40:12.000+0000","persisttime":1007,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"12回风临时避难硐室(里）","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"014","sensornum":"014A16","starttime":"2020-03-07T16:48:20.000+0000","persisttime":642,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"12回风临时避难硐室(里）","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"015","sensornum":"015A01","starttime":"2020-03-10T16:01:16.000+0000","persisttime":906,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06回风工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"015","sensornum":"015A01","starttime":"2020-03-10T16:10:41.000+0000","persisttime":618,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06回风工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"015","sensornum":"015A01","starttime":"2020-03-10T16:22:59.000+0000","persisttime":620,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06回风工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"015","sensornum":"015A01","starttime":"2020-03-10T16:24:27.000+0000","persisttime":625,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06回风工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"015","sensornum":"015A01","starttime":"2020-03-10T16:25:35.000+0000","persisttime":1267,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06回风工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"015","sensornum":"015A02","starttime":"2020-03-10T16:01:16.000+0000","persisttime":937,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06回风回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"015","sensornum":"015A02","starttime":"2020-03-10T16:10:41.000+0000","persisttime":649,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06回风回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"015","sensornum":"015A02","starttime":"2020-03-10T16:23:02.000+0000","persisttime":679,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06回风回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"015","sensornum":"015A02","starttime":"2020-03-10T16:25:35.000+0000","persisttime":1236,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06回风回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A01","starttime":"2020-03-07T17:03:50.000+0000","persisttime":1424,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺隅角","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A01","starttime":"2020-03-07T17:09:13.000+0000","persisttime":1101,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺隅角","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A01","starttime":"2020-03-07T18:12:28.000+0000","persisttime":742,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺隅角","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A01","starttime":"2020-03-07T18:21:33.000+0000","persisttime":599,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺隅角","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A02","starttime":"2020-03-07T17:01:55.000+0000","persisttime":1539,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A02","starttime":"2020-03-07T17:09:13.000+0000","persisttime":1132,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A02","starttime":"2020-03-07T18:12:30.000+0000","persisttime":771,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A02","starttime":"2020-03-07T18:21:33.000+0000","persisttime":630,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A03","starttime":"2020-03-07T17:03:03.000+0000","persisttime":1533,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺履带钻机","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A03","starttime":"2020-03-07T17:09:13.000+0000","persisttime":1101,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺履带钻机","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A03","starttime":"2020-03-07T18:12:28.000+0000","persisttime":742,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺履带钻机","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A03","starttime":"2020-03-07T18:21:33.000+0000","persisttime":599,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺履带钻机","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A04","starttime":"2020-03-07T17:04:09.000+0000","persisttime":1436,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07中部","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A04","starttime":"2020-03-07T17:09:13.000+0000","persisttime":1163,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07中部","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A04","starttime":"2020-03-07T18:12:28.000+0000","persisttime":804,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07中部","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A04","starttime":"2020-03-07T18:21:33.000+0000","persisttime":691,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07中部","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A07","starttime":"2020-03-07T17:00:44.000+0000","persisttime":1641,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07钻机","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A07","starttime":"2020-03-07T17:09:13.000+0000","persisttime":1132,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07钻机","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A07","starttime":"2020-03-07T18:12:28.000+0000","persisttime":742,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07钻机","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A07","starttime":"2020-03-07T18:21:33.000+0000","persisttime":599,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07钻机","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A09","starttime":"2020-03-07T17:13:13.000+0000","persisttime":5142,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A10","starttime":"2020-03-07T17:13:22.000+0000","persisttime":5103,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回风临时避难硐室（里）","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A11","starttime":"2020-03-07T17:13:44.000+0000","persisttime":5081,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回风临时避难硐室（外）","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A13","starttime":"2020-03-07T17:13:30.000+0000","persisttime":4972,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺电气设备","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"017","sensornum":"017A14","starttime":"2020-03-07T17:13:53.000+0000","persisttime":4949,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"07回顺绞车","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"023","sensornum":"023A01","starttime":"2020-03-09T16:08:59.000+0000","persisttime":766,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"13运输工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"023","sensornum":"023A02","starttime":"2020-03-09T16:13:32.000+0000","persisttime":616,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"13运输中部","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"023","sensornum":"023A02","starttime":"2020-03-09T16:17:55.000+0000","persisttime":1583,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"13运输中部","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"023","sensornum":"023A03","starttime":"2020-03-09T16:13:35.000+0000","persisttime":644,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"13运输回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"023","sensornum":"023A03","starttime":"2020-03-09T16:17:55.000+0000","persisttime":1614,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"13运输回风流","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"024","sensornum":"024A01","starttime":"2020-03-08T15:11:12.000+0000","persisttime":5063,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"13回风工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"025","sensornum":"025A01","starttime":"2020-03-07T16:07:58.000+0000","persisttime":625,"alarmvalue":0,"statusdec":"断电","typename":"模拟量","sensorplace":"06运输工作面","sensorunit":"CH4%","sensorname":"环境瓦斯"},{"stationnum":"027","sensornum":"027D16","starttime":"2020-03-08T14:26:21.000+0000","persisttime":619,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"胶带四联巷","sensorunit":null,"sensorname":"风门"},{"stationnum":"027","sensornum":"027D16","starttime":"2020-03-08T14:27:24.000+0000","persisttime":617,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"胶带四联巷","sensorunit":null,"sensorname":"风门"},{"stationnum":"027","sensornum":"027D16","starttime":"2020-03-08T14:37:10.000+0000","persisttime":2068,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"胶带四联巷","sensorunit":null,"sensorname":"风门"},{"stationnum":"015","sensornum":"015D04","starttime":"2020-03-10T16:34:20.000+0000","persisttime":711,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"015","sensornum":"015D04","starttime":"2020-03-10T15:54:05.000+0000","persisttime":3126,"alarmvalue":2,"statusdec":"断电","typename":"开关量","sensorplace":"06回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"015","sensornum":"015D04","starttime":"2020-03-10T16:32:02.000+0000","persisttime":849,"alarmvalue":2,"statusdec":"断电","typename":"开关量","sensorplace":"06回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"015","sensornum":"015D04","starttime":"2020-03-10T16:33:10.000+0000","persisttime":812,"alarmvalue":2,"statusdec":"断电","typename":"开关量","sensorplace":"06回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"015","sensornum":"015D04","starttime":"2020-03-10T16:34:20.000+0000","persisttime":803,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"023","sensornum":"023D04","starttime":"2020-03-07T17:05:47.000+0000","persisttime":3165,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13运输工作面","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"023","sensornum":"023D04","starttime":"2020-03-07T17:05:47.000+0000","persisttime":3196,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13运输工作面","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"023","sensornum":"023D04","starttime":"2020-03-09T16:09:19.000+0000","persisttime":622,"alarmvalue":2,"statusdec":"断电","typename":"开关量","sensorplace":"13运输工作面","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"024","sensornum":"024D04","starttime":"2020-03-07T14:07:29.000+0000","persisttime":2133,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"024","sensornum":"024D04","starttime":"2020-03-08T17:24:50.000+0000","persisttime":601,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"024","sensornum":"024D04","starttime":"2020-03-09T19:40:56.000+0000","persisttime":3201,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"024","sensornum":"024D04","starttime":"2020-03-07T14:07:29.000+0000","persisttime":2164,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"024","sensornum":"024D04","starttime":"2020-03-08T15:11:09.000+0000","persisttime":5097,"alarmvalue":2,"statusdec":"断电","typename":"开关量","sensorplace":"13回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"024","sensornum":"024D04","starttime":"2020-03-08T17:24:50.000+0000","persisttime":632,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"024","sensornum":"024D04","starttime":"2020-03-09T19:40:56.000+0000","persisttime":5110,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风","sensorunit":null,"sensorname":"风筒状态"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-07T21:12:51.000+0000","persisttime":8983,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-07T21:15:28.000+0000","persisttime":8826,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-08T16:52:19.000+0000","persisttime":1961,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-09T19:26:32.000+0000","persisttime":864,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-10T18:41:45.000+0000","persisttime":795,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-10T19:07:03.000+0000","persisttime":881,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-07T21:12:51.000+0000","persisttime":9014,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-07T21:15:28.000+0000","persisttime":8899,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-08T16:52:19.000+0000","persisttime":2398,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-09T19:26:32.000+0000","persisttime":1110,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-10T16:01:25.000+0000","persisttime":619,"alarmvalue":2,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-10T16:01:48.000+0000","persisttime":626,"alarmvalue":2,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-10T16:13:07.000+0000","persisttime":626,"alarmvalue":2,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-10T16:25:35.000+0000","persisttime":1297,"alarmvalue":2,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-10T18:41:45.000+0000","persisttime":826,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"015","sensornum":"015D05","starttime":"2020-03-10T19:07:03.000+0000","persisttime":1804,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-07T18:31:56.000+0000","persisttime":666,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-08T17:31:54.000+0000","persisttime":9279,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-09T16:06:41.000+0000","persisttime":626,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-09T16:47:41.000+0000","persisttime":945,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-10T16:45:41.000+0000","persisttime":799,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-07T18:31:56.000+0000","persisttime":728,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-08T17:31:54.000+0000","persisttime":9310,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-09T16:06:41.000+0000","persisttime":626,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-09T16:44:17.000+0000","persisttime":1149,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D05","starttime":"2020-03-10T16:45:41.000+0000","persisttime":1117,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D06","starttime":"2020-03-09T16:47:47.000+0000","persisttime":1343,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D06","starttime":"2020-03-09T17:03:02.000+0000","persisttime":613,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D06","starttime":"2020-03-09T16:44:40.000+0000","persisttime":1499,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"023","sensornum":"023D06","starttime":"2020-03-09T17:03:02.000+0000","persisttime":644,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-07T14:06:33.000+0000","persisttime":2189,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-07T18:01:28.000+0000","persisttime":627,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-08T16:35:31.000+0000","persisttime":1025,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-09T16:06:27.000+0000","persisttime":610,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-10T16:47:27.000+0000","persisttime":693,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-07T14:06:33.000+0000","persisttime":2189,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-07T18:01:28.000+0000","persisttime":627,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-08T16:35:31.000+0000","persisttime":1025,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-09T16:06:27.000+0000","persisttime":610,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D07","starttime":"2020-03-10T16:47:27.000+0000","persisttime":693,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-07T14:06:33.000+0000","persisttime":2220,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-07T18:01:28.000+0000","persisttime":658,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-08T16:35:31.000+0000","persisttime":1056,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-09T16:06:27.000+0000","persisttime":671,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-10T16:47:27.000+0000","persisttime":1626,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-07T14:06:33.000+0000","persisttime":2251,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-07T18:01:28.000+0000","persisttime":689,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-08T16:35:31.000+0000","persisttime":1086,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-09T16:06:27.000+0000","persisttime":702,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"024","sensornum":"024D08","starttime":"2020-03-10T16:47:27.000+0000","persisttime":1011,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"13回风主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-07T18:21:48.000+0000","persisttime":707,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-08T20:09:05.000+0000","persisttime":4675,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-08T20:09:13.000+0000","persisttime":4667,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-09T17:08:40.000+0000","persisttime":2557,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T16:08:06.000+0000","persisttime":1482,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T16:48:23.000+0000","persisttime":3019,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T16:58:59.000+0000","persisttime":2383,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T17:04:13.000+0000","persisttime":2069,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T17:14:25.000+0000","persisttime":4732,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T17:35:32.000+0000","persisttime":5506,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T18:55:00.000+0000","persisttime":772,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T18:55:08.000+0000","persisttime":764,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T18:57:43.000+0000","persisttime":670,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T18:59:32.000+0000","persisttime":623,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-07T18:21:48.000+0000","persisttime":646,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-08T20:09:05.000+0000","persisttime":4675,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-08T20:09:13.000+0000","persisttime":16346,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-09T17:08:40.000+0000","persisttime":3203,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T16:08:06.000+0000","persisttime":1482,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T16:48:23.000+0000","persisttime":3019,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T16:58:59.000+0000","persisttime":2383,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T17:14:25.000+0000","persisttime":4640,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T17:35:32.000+0000","persisttime":3465,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T18:55:00.000+0000","persisttime":803,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T18:55:08.000+0000","persisttime":730,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T18:57:43.000+0000","persisttime":640,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D05","starttime":"2020-03-10T18:59:32.000+0000","persisttime":623,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机一级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-07T18:21:40.000+0000","persisttime":654,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-08T20:09:05.000+0000","persisttime":16354,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-09T17:08:31.000+0000","persisttime":2566,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T16:07:51.000+0000","persisttime":1528,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T16:48:15.000+0000","persisttime":6210,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T16:58:59.000+0000","persisttime":5566,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T17:04:13.000+0000","persisttime":5252,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T17:14:25.000+0000","persisttime":1457,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T17:35:23.000+0000","persisttime":3382,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T18:55:00.000+0000","persisttime":738,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T18:57:35.000+0000","persisttime":617,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T18:59:32.000+0000","persisttime":653,"alarmvalue":0,"statusdec":"报警","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-07T18:21:40.000+0000","persisttime":654,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-08T20:09:05.000+0000","persisttime":16395,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-09T17:08:31.000+0000","persisttime":2689,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T16:07:51.000+0000","persisttime":1559,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T16:48:15.000+0000","persisttime":6302,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T16:58:59.000+0000","persisttime":6181,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T17:04:13.000+0000","persisttime":2069,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T17:14:25.000+0000","persisttime":1457,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T17:35:23.000+0000","persisttime":3382,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T18:55:00.000+0000","persisttime":738,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T18:57:35.000+0000","persisttime":617,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"},{"stationnum":"025","sensornum":"025D06","starttime":"2020-03-10T18:59:32.000+0000","persisttime":685,"alarmvalue":0,"statusdec":"断电","typename":"开关量","sensorplace":"06运输主风机二级","sensorunit":null,"sensorname":"设备开停"}]
     */

    private String msg;
    private int code;
    private ArrayList<DataBean> data;


    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public ArrayList<DataBean> getData() {
        return data;
    }

    public void setData(ArrayList<DataBean> data) {
        this.data = data;
    }

    public static class DataBean implements Parcelable{
        /**
         * stationnum : 014
         * sensornum : 014A09
         * starttime : 2020-03-07T16:29:36.000+0000
         * persisttime : 682
         * alarmvalue : 0
         * statusdec : 断电
         * typename : 模拟量
         * sensorplace : 12工作面回风流
         * sensorunit : CH4%
         * sensorname : 环境瓦斯
         */

        private String stationnum;
        private String sensornum;
        private String starttime;
        private int persisttime;
        private String alarmvalue;
        private String statusdec;
        private String typename;
        private String sensorplace;
        private String sensorunit;
        private String sensorname;
        private boolean isshow=false;
        /**
         * endtime : 2020-03-18T16:02:38.000+0800
         * alarmvalue : 1.48
         * minedispose :
         * minedispose_person :
         * minedispose_time :
         * alarmreason :
         */

        private String endtime;
        private String minedispose;
        private String minedispose_person;
        private String minedispose_time;
        private String alarmreason;
        /**
         * ondescribe : null
         * offdescribe : null
         */

        private String ondescribe;
        private String offdescribe;

        public boolean isIsshow() {
            return isshow;
        }

        public void setIsshow(boolean isshow) {
            this.isshow = isshow;
        }
        public String getStationnum() {
            return stationnum;
        }

        public void setStationnum(String stationnum) {
            this.stationnum = stationnum;
        }

        public String getSensornum() {
            return sensornum;
        }

        public void setSensornum(String sensornum) {
            this.sensornum = sensornum;
        }

        public String getStarttime() {
            return starttime;
        }

        public void setStarttime(String starttime) {
            this.starttime = starttime;
        }

        public int getPersisttime() {
            return persisttime;
        }

        public void setPersisttime(int persisttime) {
            this.persisttime = persisttime;
        }

        public String getAlarmvalue() {
            return alarmvalue;
        }

        public void setAlarmvalue(String alarmvalue) {
            this.alarmvalue = alarmvalue;
        }

        public String getStatusdec() {
            return statusdec;
        }

        public void setStatusdec(String statusdec) {
            this.statusdec = statusdec;
        }

        public String getTypename() {
            return typename;
        }

        public void setTypename(String typename) {
            this.typename = typename;
        }

        public String getSensorplace() {
            return sensorplace;
        }

        public void setSensorplace(String sensorplace) {
            this.sensorplace = sensorplace;
        }

        public String getSensorunit() {
            return sensorunit;
        }

        public void setSensorunit(String sensorunit) {
            this.sensorunit = sensorunit;
        }

        public String getSensorname() {
            return sensorname;
        }

        public void setSensorname(String sensorname) {
            this.sensorname = sensorname;
        }


        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(stationnum);
            dest.writeString(sensornum);
            dest.writeString(starttime);
            dest.writeInt(persisttime);
            dest.writeString(alarmvalue);
            dest.writeString(statusdec);
            dest.writeString(typename);
            dest.writeString(sensorplace);
            dest.writeString(sensorunit);
            dest.writeString(sensorname);
            dest.writeString(endtime);
            dest.writeString(minedispose);
            dest.writeString(minedispose_person);
            dest.writeString(minedispose_time);
            dest.writeString(alarmreason);
            dest.writeString(ondescribe);
            dest.writeString(offdescribe);
        }
        public static final Creator<DataBean> CREATOR = new Creator(){

            @Override
            public DataBean createFromParcel(Parcel source) {
                // TODO Auto-generated method stub
                // 必须按成员变量声明的顺序读取数据，不然会出现获取数据出错
                DataBean p = new DataBean();
                p.setStationnum(source.readString());
                p.setSensornum(source.readString());
                p.setStarttime(source.readString());
                p.setPersisttime(source.readInt());
                p.setAlarmvalue(source.readString());
                p.setStatusdec(source.readString());
                p.setTypename(source.readString());
                p.setSensorplace(source.readString());
                p.setSensorunit(source.readString());
                p.setSensorname(source.readString());
                p.setEndtime(source.readString());
                p.setMinedispose(source.readString());
                p.setMinedispose_person(source.readString());
                p.setMinedispose_time(source.readString());
                p.setAlarmreason(source.readString());
                p.setOndescribe(source.readString());
                p.setOffdescribe(source.readString());
                return p;
            }

            @Override
            public DataBean[] newArray(int size) {
                // TODO Auto-generated method stub
                return new DataBean[size];
            }
        };

        public String getEndtime() {
            return endtime;
        }

        public void setEndtime(String endtime) {
            this.endtime = endtime;
        }

        public String getMinedispose() {
            return minedispose;
        }

        public void setMinedispose(String minedispose) {
            this.minedispose = minedispose;
        }

        public String getMinedispose_person() {
            return minedispose_person;
        }

        public void setMinedispose_person(String minedispose_person) {
            this.minedispose_person = minedispose_person;
        }

        public String getMinedispose_time() {
            return minedispose_time;
        }

        public void setMinedispose_time(String minedispose_time) {
            this.minedispose_time = minedispose_time;
        }

        public String getAlarmreason() {
            return alarmreason;
        }

        public void setAlarmreason(String alarmreason) {
            this.alarmreason = alarmreason;
        }


        public String getOndescribe() {
            return ondescribe;
        }

        public void setOndescribe(String ondescribe) {
            this.ondescribe = ondescribe;
        }

        public String getOffdescribe() {
            return offdescribe;
        }

        public void setOffdescribe(String offdescribe) {
            this.offdescribe = offdescribe;
        }

    }
}
