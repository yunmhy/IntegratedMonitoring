package com.example.safe_lib.bean;

import java.util.List;

public class MyLineChartData {

    private String sersnorname;
    private List<MyLineChartBean>beanList;

    public String getSersnorname() {
        return sersnorname;
    }

    public void setSersnorname(String sersnorname) {
        this.sersnorname = sersnorname;
    }

    public MyLineChartData(String sersnorname, List<MyLineChartBean> beanList) {
        this.sersnorname=sersnorname;
        this.beanList = beanList;

    }

    public List<MyLineChartBean> getBeanList() {
        return beanList;
    }

    public void setBeanList(List<MyLineChartBean> beanList) {
        this.beanList = beanList;
    }

    public static class MyLineChartBean {
        private String sensornum;
        private LineChartBean data;

        public MyLineChartBean(String sensornum, LineChartBean data) {
            this.sensornum = sensornum;
            this.data = data;
        }

        public LineChartBean getData() {
            return data;
        }

        public void setData(LineChartBean data) {
            this.data = data;
        }

        public String getSensornum() {
            return sensornum;
        }

        public void setSensornum(String sensornum) {
            this.sensornum = sensornum;
        }
    }
}
