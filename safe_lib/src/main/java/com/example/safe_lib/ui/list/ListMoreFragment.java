package com.example.safe_lib.ui.list;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.fragment.BaseFragment;
import com.example.base_lib.utils.TimeUtils;
import com.example.safe_lib.R;
import com.example.safe_lib.SafeMainActivity;
import com.example.safe_lib.bean.SensorsInfo;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListMoreFragment extends BaseFragment {
    private TextView tStationnum;
    private TextView tSensornum;
    private TextView tSensortype;
    private TextView tTypename;
    private TextView tSensorcode;
    private TextView tSensorname;
    private TextView tSensorplace;
    private TextView tSensorunit;
    private TextView tCreatetime;
    private TextView tArrangeup;
    private TextView tArrangedown;
    private TextView tWarningvalueup;
    private TextView tWarningvaluedown;
    private TextView tLwarningvalueup;
    private TextView tLwarningvaluedown;
    private TextView tPoweroffup;
    private TextView tPoweroffdown;
    private TextView tPoweronup;
    private TextView tPowerondown;
    private ListViewModel listViewModel;
    private String sensornum;
    private String unitcode;

    public ListMoreFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        listViewModel =
                ViewModelProviders.of(getActivity()).get(ListViewModel.class);
        super.initBeforeCreateView(savedInstanceState);

    }

    @Override
    protected void initData() {
        Bundle bundle=getArguments();
        unitcode=bundle.getString("unitcode");
        sensornum=bundle.getString("sensornum");
        listViewModel.getSensorsInfo(unitcode,sensornum);
        listViewModel.getSensors().observe(this, new Observer<SensorsInfo>() {
            @Override
            public void onChanged(SensorsInfo sensorsInfo) {
                if (sensorsInfo.getCode() == 0) {
                    initSersorsData(sensorsInfo.getData().get(0));
                } else {
                    Toast.makeText(getContext(), "无数据", Toast.LENGTH_LONG).show();
                }
            }
        });
        listViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)) {

                    Toast.makeText(getContext(), "加载数据失败", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void initSersorsData(SensorsInfo.DataBean data) {
        tStationnum.setText(data.getStationnum());
        tSensornum.setText(data.getSensornum());
        tSensortype.setText(data.getSensortype()+"");
        tTypename.setText(data.getTypename());
        tSensorcode.setText(data.getSensorcode());
        tSensorname.setText(data.getSensorname());
        tSensorplace.setText(data.getSensorplace());
        tSensorunit.setText(data.getSensorunit());
        tCreatetime.setText(TimeUtils.CurveTimeFarmat(data.getCreatetime()));
        tArrangeup.setText(data.getArrangeup()+"");
        tArrangedown.setText(data.getArrangedown()+"");
        tWarningvalueup.setText(data.getWarningvalueup()+"");
        tWarningvaluedown.setText(data.getWarningvaluedown()+"");
        tLwarningvalueup.setText(data.getLwarningvalueup()+"");
        tLwarningvaluedown.setText(data.getLwarningvaluedown()+"");
        tPoweroffup.setText(data.getPoweroffvalueup()+"");
        tPoweroffdown.setText(data.getPoweroffvaluedown()+"");
        tPoweronup.setText(data.getPoweronvalueup()+"");
        tPowerondown.setText(data.getPoweronvaluedown()+"");
    }


    @Override
    protected void initView(View view) {
        tStationnum = (TextView) view.findViewById(R.id.t_stationnum);
        tSensornum = (TextView) view.findViewById(R.id.t_sensornum);
        tSensortype = (TextView) view.findViewById(R.id.t_sensortype);
        tTypename = (TextView) view.findViewById(R.id.t_typename);
        tSensorcode = (TextView) view.findViewById(R.id.t_sensorcode);
        tSensorname = (TextView) view.findViewById(R.id.t_sensorname);
        tSensorplace = (TextView) view.findViewById(R.id.t_sensorplace);
        tSensorunit = (TextView) view.findViewById(R.id.t_sensorunit);
        tCreatetime = (TextView) view.findViewById(R.id.t_createtime);
        tArrangeup = (TextView) view.findViewById(R.id.t_arrangeup);
        tArrangedown = (TextView) view.findViewById(R.id.t_arrangedown);
        tWarningvalueup = (TextView) view.findViewById(R.id.t_warningvalueup);
        tWarningvaluedown = (TextView) view.findViewById(R.id.t_warningvaluedown);
        tLwarningvalueup = (TextView) view.findViewById(R.id.t_lwarningvalueup);
        tLwarningvaluedown = (TextView) view.findViewById(R.id.t_lwarningvaluedown);
        tPoweroffup = (TextView) view.findViewById(R.id.t_poweroffup);
        tPoweroffdown = (TextView) view.findViewById(R.id.t_poweroffdown);
        tPoweronup = (TextView) view.findViewById(R.id.t_poweronup);
        tPowerondown = (TextView) view.findViewById(R.id.t_powerondown);

    }


    @Override
    protected int contentLayout() {
        return R.layout.fragment_list_more;
    }

}
