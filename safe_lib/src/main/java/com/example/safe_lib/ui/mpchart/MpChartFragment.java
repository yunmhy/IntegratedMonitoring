package com.example.safe_lib.ui.mpchart;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.bigkoo.pickerview.view.TimePickerView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.fragment.BaseFragment;
import com.example.safe_lib.R;
import com.example.safe_lib.SafeMainActivity;
import com.example.safe_lib.adapter.AddSerAdapter;
import com.example.safe_lib.adapter.LineChartAdapter;
import com.example.safe_lib.bean.LineChartBean;
import com.example.safe_lib.bean.MyLineChartData;
import com.example.safe_lib.bean.MySensor;
import com.example.safe_lib.bean.SafelistBean;
import com.example.safe_lib.ui.list.ListViewModel;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MpChartFragment extends BaseFragment {

    private RecyclerView rcySersnoNum;
    private TimePickerView pvTime;
    private TimePickerView pvendTime;
    private OptionsPickerView pvSensor;
    private String token;
    private String unitcode;
    List<SafelistBean.DataBean> sensornums;
    //    private ArrayList<SafeAlarmBean.DataBean> sensornums;
    private String showsensor;
    private String startTime;
    private String endTime;
    private UserInfo userInfo;
    private LinearLayout tNickid;

    private RecyclerView recyclerView;
    private LineChartAdapter lineadapter;
    private AddSerAdapter Addadapter;
    String sensorname;//传感器名称
    String sensorunit;//传感器单位
    List<MyLineChartData> mylist = new ArrayList();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private Button addLine;

    public MpChartFragment() {
        // Required empty public constructor
    }

    private ListViewModel listViewModel;

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        listViewModel =
                ViewModelProviders.of(getActivity()).get(ListViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userInfo = ((SafeMainActivity) context).toValue();
    }

    @Override
    protected void initData() {
//        SafeAlarmBean safeAlarmBean = alarmViewModel.getData().getValue();
//        if (safeAlarmBean != null) {
//            sensornums = safeAlarmBean.getData();
//        }
        listViewModel.set(userInfo, this.getActivity());
        unitcode = userInfo.getUnitinfo().get(0).getUnitcode();

        token = userInfo.getToken();
        listViewModel.getsername().observe(this, new Observer<MySensor>() {

            @Override
            public void onChanged(MySensor mySensor) {
                initNickPicker(getContext(), mySensor.getSensorname(), mySensor.getSensorcode(), "选择传感器");
            }
        });

//        Bundle bundle = getArguments();
//        if (bundle != null) {
//                    unitcode = bundle.getString("unitcode");
////        sensornums = bundle.getParcelableArrayList("sensornum");
//            pos = bundle.getInt("showsensor");
//            startTime = TimeUtils.CurveTimeFarmat(sensornums.get(pos).getStarttime());
//            choseStartTime.setText(startTime);
//            endTime = TimeUtils.TimeAdd(Calendar.SECOND, sensornums.get(pos).getStarttime(), sensornums.get(pos).getPersisttime());
//            choseEndTime.setText(endTime);
//            showsensor = sensornums.get(pos).getSensornum();
//            tNickid.setText(showsensor);
//            getChartData();
//        } else {
//
//        }

//        if (pos >= 0) {
//            int new_pos = 0;
//            for (int i = 0; i < sensorlist.size(); i++) {
//                if (sensorlist.get(i) != null && sensorlist.get(i).equals(showsensor)) {
//                    new_pos = i;
//                }
//            }
//            pvSensor.setSelectOptions(new_pos);
//        }
    }

    @Override
    public void onStart() {
        super.onStart();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(listViewModel.getsername().getValue()==null){
                    listViewModel.getSensorname(true);
                }

            }
        },10);
    }

    @Override
    protected void initView(View view) {
        rcySersnoNum = (RecyclerView) view.findViewById(R.id.rcy_sersno_num);
        recyclerView = view.findViewById(R.id.line_rcy);
        view.findViewById(R.id.line_by).setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));
        tNickid = (LinearLayout) view.findViewById(R.id.l_chose_cgq);
        initTimePicker();
        initTimeEndPicker();
        pvTime.setDate(Calendar.getInstance());
        tNickid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvSensor.show();
            }
        });
        Addadapter = new AddSerAdapter(R.layout.sensor_item);
        rcySersnoNum.setAdapter(Addadapter);
        LinearLayoutManager addlayoutManager = new LinearLayoutManager(getContext());
        addlayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rcySersnoNum.setLayoutManager(addlayoutManager);
        View empty = LayoutInflater.from(getContext()).inflate(R.layout.line_empty_item, null, false);
        lineadapter = new LineChartAdapter(getContext(), R.layout.linechart_item);
        lineadapter.setEmptyView(empty);
        recyclerView.setAdapter(lineadapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
            Addadapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                    Addadapter.remove(position);
                    lineadapter.remove(position);
                    mylist.remove(position);
                }

        });
    }

    private void initNickPicker(final Context context, final List nicklist, final List numlist, final String title) {
        pvSensor = new OptionsPickerBuilder(context, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                sensorname = (String) nicklist.get(options1);
                List list = (List) numlist.get(options1);
                if (list.size()<=0){
                    Toast.makeText(getContext(),"请选择传感器编号",Toast.LENGTH_SHORT);
                    return;
                }
                showsensor = (String) list.get(options2);
                pvTime.show();
//                TextView tv = (TextView) v.findViewById(v.getId());
//                tv.setText(sensorname + ":" + showsensor);
//                int new_pos = 0;
//                for (int i = 0; i < sensorlist.size(); i++) {
//                    if (sensorlist.get(i) != null && sensorlist.get(i).equals(showsensor)) {
//                        new_pos = i;
//                    }
//                }

            }

        })
                .isDialog(true)
                .setContentTextSize(18)
                .setLayoutRes(R.layout.pickerview_custom_nick, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        final TextView tvTitle = (TextView) v.findViewById(R.id.nick_title);
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
                        final TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
                        tvTitle.setText(title);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvSensor.returnData();
                                pvSensor.dismiss();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvSensor.dismiss();
                            }
                        });
                    }
                })
                .setTextColorCenter(getResources().getColor(R.color.blue))
                .setItemVisibleCount(9)
                .setLineSpacingMultiplier((float) 2.6)
                .setBackgroundId(R.color.black)
                .build();

        pvSensor.setPicker(nicklist, numlist);//添加数据源

    }


    private void initTimePicker() {
        final Calendar startDate = Calendar.getInstance();
        startDate.set(2019, 10, 1);
        final Calendar endDate = Calendar.getInstance();

        pvTime = new TimePickerBuilder(getContext(), new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                startTime = sdf.format(date);
                pvendTime.setDate(Calendar.getInstance());
                pvendTime.show();
//                TextView tv = (TextView) v.findViewById(v.getId());
//                tv.setText(sdf.format(date));
//                if (v.getId() == R.id.chose_end_time) {
//                    endTime = sdf.format(date);
//                }else startTime = sdf.format(date);
//                Toast.makeText(getContext(), date.toString(), Toast.LENGTH_LONG).show();
            }
        })
                .setType(new boolean[]{true, true, true, true, false, false})//分别对应年月日时分秒，默认全部显示
                .isDialog(true)
                .setContentTextSize(14)
                .setLayoutRes(R.layout.pickerview_custom_time, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
                        final TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
                        TextView textView = (TextView) v.findViewById(R.id.time_title);
                        textView.setText("选择开始时间");
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvTime.returnData();
                                pvTime.dismiss();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvTime.dismiss();
                            }
                        });
                    }
                })
                .setTextColorCenter(getResources().getColor(R.color.blue))
                .setItemVisibleCount(5)
                .setDate(Calendar.getInstance())
                .setRangDate(startDate, endDate)
                .setItemVisibleCount(9)
                .setLineSpacingMultiplier((float) 2.6)
                .setBackgroundId(R.color.black)
                .setLabel("", "", "", "", "", "")
                .isCenterLabel(false).build();

    }

    private void initTimeEndPicker() {
        final Calendar startDate = Calendar.getInstance();
        startDate.set(2019, 10, 1);
        final Calendar endDate = Calendar.getInstance();
        pvendTime = new TimePickerBuilder(getContext(), new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                endTime = sdf.format(date);
                if (sensorname == null) {
                    Toast.makeText(getContext(), "请选择传感器", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (startTime != null && endTime != null) {
                    try {
                        if (sdf.parse(startTime).before(sdf.parse(endTime))) {
                            getChartData();
                        } else {
                            Toast.makeText(getContext(), "时间选择错误", Toast.LENGTH_SHORT).show();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else Toast.makeText(getContext(), "请选择日期", Toast.LENGTH_SHORT).show();
//                TextView tv = (TextView) v.findViewById(v.getId());
//                tv.setText(sdf.format(date));
//                if (v.getId() == R.id.chose_end_time) {
//                    endTime = sdf.format(date);
//                }else startTime = sdf.format(date);
//                Toast.makeText(getContext(), date.toString(), Toast.LENGTH_LONG).show();
            }
        })
                .setType(new boolean[]{true, true, true, true, false, false})//分别对应年月日时分秒，默认全部显示
                .isDialog(true)
                .setContentTextSize(14)
                .setLayoutRes(R.layout.pickerview_custom_time, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
                        final TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
                        TextView textView = (TextView) v.findViewById(R.id.time_title);
                        textView.setText("选择结束时间");
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvendTime.returnData();
                                pvendTime.dismiss();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvendTime.dismiss();
                            }
                        });
                    }
                })
                .setTextColorCenter(getResources().getColor(R.color.blue))
                .setItemVisibleCount(5)
                .setDate(Calendar.getInstance())
                .setRangDate(startDate, endDate)
                .setItemVisibleCount(9)
                .setLineSpacingMultiplier((float) 2.6)
                .setBackgroundId(R.color.black)
                .setLabel("", "", "", "", "", "")
                .isCenterLabel(false).build();

    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_chart;
    }

    public void getChartData() {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", token);
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", unitcode);
        paramsGet.put("sensornum", showsensor);
        paramsGet.put("starttime", startTime);
        paramsGet.put("endtime", endTime);
        OkGoBuilder.getInstance()
                .Builder(getActivity())
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/safety/curve")
                .cls(LineChartBean.class)
                .method(OkGoBuilder.GET)
                .headers(headers)
                .params(paramsGet)
                .dialogshow(true)
                .callback(new MyCallback<LineChartBean>() {
                    @Override
                    public void onSuccess(LineChartBean data, String code) {
                        if (data.getData() != null && data.getData().size() > 0) {
                            drawchart(data);
                        } else {
                            Toast.makeText(getContext(), "无数据", Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(String e, String code) {
                        Log.e("linechart", e);
                    }
                }).build();
    }

    private void drawchart(LineChartBean data) {
        Addadapter.addData(sensorname+"-"+showsensor);
        List<MyLineChartData.MyLineChartBean> myLineChartBeans = new ArrayList<>();
        myLineChartBeans.add(new MyLineChartData.MyLineChartBean(showsensor, data));
        MyLineChartData myLineChartData = new MyLineChartData(sensorname, myLineChartBeans);
        for (int i = 0; i < mylist.size(); i++) {
            MyLineChartData lineChartData = mylist.get(i);
            for (int j = 0; j < lineChartData.getBeanList().size(); j++) {
                if (lineChartData.getBeanList().get(j).getSensornum().equals(showsensor)) {
                    lineChartData.getBeanList().get(j).setData(data);
                    lineadapter.setData(i, lineChartData);
                    mylist.set(i, lineChartData);
                    return;
                }
            }
        }

        lineadapter.addData(myLineChartData);
        mylist.add(myLineChartData);


    }


}


