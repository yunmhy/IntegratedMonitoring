package com.example.safe_lib.ui.history;

import android.content.Context;
import android.os.Bundle;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.builder.TimePickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.listener.OnTimeSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.bigkoo.pickerview.view.TimePickerView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.fragment.BaseFragment;
import com.example.safe_lib.R;
import com.example.safe_lib.SafeMainActivity;
import com.example.safe_lib.adapter.ChoseAdapter;
import com.example.safe_lib.adapter.HistoryAdapter;
import com.example.safe_lib.bean.SafeHistoryBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class HistoryFragment extends BaseFragment {
    private RecyclerView ryHistoryRcy;
    private HistoryViewModel historyViewModel;
    private TextView txtSafeHistTime;
    private TimePickerView pvTime;
    private SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd");
    private UserInfo userInfo;
    private String unitcode;
    private HistoryAdapter historyAdapter;
    private List<SafeHistoryBean.DataBean> historyBeanList;
    private String historydate;
    private OptionsPickerView pvNick;
    private List<UserInfo.UnitinfoBean> unitinfo;
    private TextView choseHistoryNickname;
    private ConstraintLayout ecly;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userInfo = ((SafeMainActivity) context).toValue();
    }
    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        historyViewModel =
                ViewModelProviders.of(getActivity()).get(HistoryViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }


    @Override
    protected void initData() {
        historyViewModel.set(userInfo, this.getActivity());
        View empty = LayoutInflater.from(getContext()).inflate(R.layout.empty_item, null, false);
        historyAdapter.setEmptyView(empty);
        final TextView textView = empty.findViewById(R.id.txt_errmsg);
        final ConstraintLayout errlay = empty.findViewById(R.id.err_lay);
        errlay.setVisibility(View.INVISIBLE);
        final Button btn_rest = empty.findViewById(R.id.btn_rest);
        btn_rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historyViewModel.getReport(unitcode,historydate);
            }
        });

        historyViewModel.set(userInfo, this.getActivity());
        historyViewModel.getData().observe(this, new Observer<SafeHistoryBean>() {
            @Override
            public void onChanged(SafeHistoryBean historyBean) {
                if(historyBean.getCode()==0&&historyBean.getData()!=null&&historyBean.getData().size()>0){
//                    safelist=safeAlarmBeans.getData();
                    ecly.setVisibility(View.GONE);
                    historyAdapter.setNewData(historyBean.getData());
                }else {
                    ecly.setVisibility(View.VISIBLE);
                }

            }
        });

        historyViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)) {
                    errlay.setVisibility(View.VISIBLE);
                    textView.setText(s);
                    Toast.makeText(getContext(), "加载数据失败", Toast.LENGTH_LONG).show();
                }
//                ryHistorySrl.setRefreshing(false);
            }
        });
        unitinfo = userInfo.getUnitinfo();
        List<String> nickname=new ArrayList<>();
        for(int i = 0; i< unitinfo.size(); i++){
            nickname.add(unitinfo.get(i).getNickname());
        }
        unitcode=unitinfo.get(0).getUnitcode();
        initNickPicker(getContext(),nickname,"选择煤矿");
        choseHistoryNickname.setText(nickname.get(0));
        pvNick.setSelectOptions(0);
        if (historydate!=null&&unitcode!=null){
            historyViewModel.getReport(unitcode,historydate);
        }
    }

    @Override
    protected void initView( View view) {
        view.setBackground(new WaterMarkBg(userInfo.getUseridname()));
        ecly = view.findViewById(R.id.emty_cly);
        choseHistoryNickname = (TextView)view.findViewById(R.id.chose_history_nickname);
        choseHistoryNickname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvNick.show(v);
            }
        });
        ryHistoryRcy = (RecyclerView) view.findViewById(R.id.safe_history_rcy);
        view.findViewById(R.id.history_b).setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));
        txtSafeHistTime = (TextView) view.findViewById(R.id.txt_safe_hist_time);
        historydate=sdf.format(Calendar.getInstance().getTime());
        txtSafeHistTime.setText(historydate);
        historyAdapter=new HistoryAdapter(R.layout.safe_history_item);
        initTimePicker();
        txtSafeHistTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvTime.show(v);
            }
        });
        ryHistoryRcy.setAdapter(historyAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ryHistoryRcy.setLayoutManager(layoutManager);
        historyAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                Bundle bundle=new Bundle();
//                bundle.putString("unitcode",unitcode);
////                bundle.putParcelableArrayList("sensornum",safelist);
//                SafeHistoryBean.DataBean safeHistoryBean = (SafeHistoryBean.DataBean) adapter.getData().get(position);
//                bundle.putString("showsensor",safeHistoryBean.getSensorname());
//                Navigation.findNavController(view).navigate(R.id.action_navigation_history_to_reportMoreFragment, bundle);
            }
        });
    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_history;
    }
    private void initTimePicker() {

        pvTime = new TimePickerBuilder(getContext(), new OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date, View v) {//选中事件回调
                TextView tv = (TextView) v.findViewById(v.getId());
                tv.setText(sdf.format(date));
                historydate =sdf.format(date);
                if (historydate!=null&&unitcode!=null){
                    historyViewModel.getReport(unitcode,historydate);
                }
//                Toast.makeText(getContext(), date.toString(), Toast.LENGTH_LONG).show();
            }
        }).setType(new boolean[]{true, true, true, false, false, false})//分别对应年月日时分秒，默认全部显示
                .isDialog(true)
                .setContentTextSize(18)
                .setLayoutRes(R.layout.pickerview_custom_time, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        v.findViewById(R.id.tl_h).setVisibility(View.GONE);
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
                        final TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvTime.returnData();
                                pvTime.dismiss();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvTime.dismiss();
                            }
                        });
                    }
                })  .setItemVisibleCount(5)
                .setDate(Calendar.getInstance())
                .setTextColorCenter(getResources().getColor(R.color.blue))
                .setLineSpacingMultiplier((float) 2.6)
                .setBackgroundId(R.color.black)
                .setLabel("","","","","","")
                .isCenterLabel(false).build();

    }
    private void initNickPicker(final Context context, final List nicklist, final String title) {

        pvNick= new OptionsPickerBuilder(context, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                TextView tv = (TextView) v.findViewById(v.getId());
                tv.setText((String)nicklist.get(options1));
                unitcode=unitinfo.get(options1).getUnitcode();
            }

        })
                .isDialog(true)
                .setContentTextSize(18)
                .setLayoutRes(R.layout.pickerview_custom_nick, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        final TextView tvTitle=(TextView) v.findViewById(R.id.nick_title);
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
                        final TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
                        tvTitle.setText(title);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvNick.returnData();
                                pvNick.dismiss();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvNick.dismiss();
                            }
                        });
                    }
                })
                .setTextColorCenter(getResources().getColor(R.color.blue))
                .setItemVisibleCount(9)
                .setLineSpacingMultiplier((float) 2.6)
                .setBackgroundId(R.color.black)
                .build();

        pvNick.setPicker(nicklist);//添加数据源

    }
}