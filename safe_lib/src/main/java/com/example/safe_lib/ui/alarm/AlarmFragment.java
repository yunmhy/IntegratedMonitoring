package com.example.safe_lib.ui.alarm;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.fragment.BaseFragment;

import com.example.base_lib.utils.PvChose;
import com.example.safe_lib.R;
import com.example.safe_lib.SafeMainActivity;
import com.example.safe_lib.adapter.AlarmAdapter;
import com.example.safe_lib.adapter.ChoseAdapter;
import com.example.safe_lib.bean.SafeAlarmBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class AlarmFragment extends BaseFragment {
    private SwipeRefreshLayout refreshLayout;
    private AlarmViewModel alarmViewModel;
    private AlarmAdapter alarmAdapter;
    private RecyclerView alarmRcy;
    private UserInfo userInfo;
    private TextView chose_nick;
    private String unitcode;
    private String nickName;
    private Timer timer;
    private ArrayList<SafeAlarmBean.DataBean> safelist = new ArrayList<>();

    private OptionsPickerView pvNick;
    private List<UserInfo.UnitinfoBean> unitinfo;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userInfo = ((SafeMainActivity) context).toValue();
    }

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        alarmViewModel =
                ViewModelProviders.of(getActivity()).get(AlarmViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }

    @Override
    protected void initView(View view) {
        SafeMainActivity activity = (SafeMainActivity) getActivity();
        activity.initIcon();
        chose_nick = view.findViewById(R.id.chose_nickname);
        alarmRcy = (RecyclerView) view.findViewById(R.id.alarm_rcy);
        alarmRcy.setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.mswipeRefreshLayout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh(unitcode);
            }
        });
        alarmAdapter = new AlarmAdapter(R.layout.alarm_itm);
        alarmAdapter.bindToRecyclerView(alarmRcy);
        alarmAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                if (view.getId() == R.id.ly_click) {
                    Bundle bundle = new Bundle();
                    bundle.putString("nickname", nickName);
                    bundle.putString("unitcode", unitcode);
//                bundle.putParcelableArrayList("sensornum",safelist);
                    bundle.putInt("showsensor", position);
                    Navigation.findNavController(view).navigate(R.id.action_navigation_alarm_to_onlnyLCFragment, bundle);
                }
                if (view.getId() == R.id.show_clxx) {

                    SafeAlarmBean.DataBean item = (SafeAlarmBean.DataBean) adapter.getItem(position);
                    View viewByPosition = adapter.getViewByPosition(position, R.id.more_ly);
                    View imgshow = adapter.getViewByPosition(position, R.id.img_chuliren);
                    if (!item.isIsshow()) {
                        viewByPosition.setVisibility(View.VISIBLE);
                        item.setIsshow(true);
                        imgshow.setRotation(0);
                    } else {
                        imgshow.setRotation(180);
                        item.setIsshow(false);
                        viewByPosition.setVisibility(View.GONE);
                    }
                }


            }
        });
        alarmRcy.setAdapter(alarmAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        alarmRcy.setLayoutManager(layoutManager);
        chose_nick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pvNick.isShowing()) {
                    pvNick.show(v);
                } else {
                    pvNick.dismiss();
                }


            }
        });
    }

    @Override
    protected void initData() {
        View empty = LayoutInflater.from(getContext()).inflate(R.layout.empty_item, null, false);
        alarmAdapter.setEmptyView(empty);
        final TextView textView = empty.findViewById(R.id.txt_errmsg);
        final ConstraintLayout errlay = empty.findViewById(R.id.err_lay);
        final TextView msg = empty.findViewById(R.id.text_empty);
        final ImageView imageView = empty.findViewById(R.id.empty_img);
        errlay.setVisibility(View.INVISIBLE);
        final Button btn_rest = empty.findViewById(R.id.btn_rest);
        btn_rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh(unitcode);
            }
        });
        alarmViewModel.set(userInfo, this.getActivity());
        alarmViewModel.getData().observe(this, new Observer<SafeAlarmBean>() {
            @Override
            public void onChanged(SafeAlarmBean safeAlarmBeans) {
                if (safeAlarmBeans.getCode()==0&&safeAlarmBeans.getData()!= null&&safeAlarmBeans.getData().size()>0) {
//                    safelist=safeAlarmBeans.getData();
                    alarmAdapter.setNewData(safeAlarmBeans.getData());
                } else {
                    errlay.setVisibility(View.VISIBLE);
                    btn_rest.setVisibility(View.INVISIBLE);
                    textView.setVisibility(View.INVISIBLE);
                    imageView.setVisibility(View.INVISIBLE);
                    msg.setText("暂无数据");
                }

            }
        });

        alarmViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)) {
                    errlay.setVisibility(View.VISIBLE);
                    textView.setText(s);
                    Toast.makeText(getContext(), "加载数据失败", Toast.LENGTH_LONG).show();
                }
                refreshLayout.setRefreshing(false);
            }
        });

        unitinfo = userInfo.getUnitinfo();
        List<String> nickname = new ArrayList<>();
        for (int i = 0; i < unitinfo.size(); i++) {
            nickname.add(unitinfo.get(i).getNickname());
        }
        unitcode = unitinfo.get(0).getUnitcode();
        nickName = nickname.get(0);
        chose_nick.setText(nickName);

        initNickPicker(getContext(), nickname, "选择煤矿");

    }

    @Override
    public void onStart() {
        super.onStart();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                refresh(unitcode);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(true);
                    }
                });
            }
        }, 500, 30000);
    }

    private void initNickPicker(final Context context, final List nicklist, final String title) {

        pvNick = new OptionsPickerBuilder(context, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                TextView tv = (TextView) v.findViewById(v.getId());
                unitcode = unitinfo.get(options1).getUnitcode();
                nickName = (String) nicklist.get(options1);
                tv.setText(nickName);
                refresh(unitcode);
            }

        })
                .isDialog(true)
                .setContentTextSize(18)
                .setLayoutRes(R.layout.pickerview_custom_nick, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        final TextView tvTitle = (TextView) v.findViewById(R.id.nick_title);
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
                        final TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
                        tvTitle.setText(title);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvNick.returnData();
                                pvNick.dismiss();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvNick.dismiss();
                            }
                        });
                    }
                })
                .setTextColorCenter(getResources().getColor(R.color.blue))
                .setItemVisibleCount(9)
                .setLineSpacingMultiplier((float) 2.6)
                .setBackgroundId(R.color.black)
                .build();

        pvNick.setPicker(nicklist);//添加数据源

    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_alarm;
    }

    private void refresh(String unitcode) {

        alarmViewModel.getAlarmData(unitcode);
    }

    @Override
    public void onStop() {
        super.onStop();
        timer.cancel();
    }
}