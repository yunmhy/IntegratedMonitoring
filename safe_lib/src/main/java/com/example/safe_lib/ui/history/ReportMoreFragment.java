package com.example.safe_lib.ui.history;


import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.fragment.BaseFragment;
import com.example.safe_lib.R;
import com.example.safe_lib.adapter.AlarmAdapter;
import com.example.safe_lib.bean.SafeAlarmBean;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportMoreFragment extends BaseFragment {
    private LinearLayout reportNickname;
    private TextView choseNickname;
    private SwipeRefreshLayout reportAlarmRfl;
    private RecyclerView reportAlarmRcy;
    private AlarmAdapter alarmAdapter;
    private HistoryViewModel historyViewModel;
    private String unitcode;
    private String sersnor;
    public ReportMoreFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        historyViewModel =
                ViewModelProviders.of(getActivity()).get(HistoryViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }

    @Override
    protected void initData() {
        Bundle bundle =getArguments();
        unitcode=bundle.getString("unitcode");
        sersnor=bundle.getString("showsensor");
        View empty = LayoutInflater.from(getContext()).inflate(R.layout.empty_item, null, false);
        alarmAdapter.setEmptyView(empty);
        final TextView textView = empty.findViewById(R.id.txt_errmsg);
        final ConstraintLayout errlay = empty.findViewById(R.id.err_lay);
        errlay.setVisibility(View.INVISIBLE);
        final Button btn_rest = empty.findViewById(R.id.btn_rest);
        btn_rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh(unitcode,sersnor);
            }
        });
        historyViewModel.getAlarmaData().observe(this, new Observer<SafeAlarmBean>() {
            @Override
            public void onChanged(SafeAlarmBean safeAlarmBeans) {
                if(safeAlarmBeans.getCode()==0){
//                    safelist=safeAlarmBeans.getData();
                    alarmAdapter.setNewData(safeAlarmBeans.getData());
                }else {
                    errlay.setVisibility(View.VISIBLE);
                    textView.setText(safeAlarmBeans.getMsg());
                }

            }
        });

        historyViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)) {
                    errlay.setVisibility(View.VISIBLE);
                    textView.setText(s);
                    Toast.makeText(getContext(), "加载数据失败", Toast.LENGTH_LONG).show();
                }
                reportAlarmRfl.setRefreshing(false);
            }
        });

        refresh(unitcode,sersnor);
    }

    @Override
    protected void initView(View view) {
        reportNickname = (LinearLayout) view.findViewById(R.id.report_nickname);
        choseNickname = (TextView) view.findViewById(R.id.chose_nickname);
        reportAlarmRfl = (SwipeRefreshLayout) view.findViewById(R.id.report_alarm_rfl);
        reportAlarmRcy = (RecyclerView) view.findViewById(R.id.report_alarm_rcy);

        reportAlarmRfl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh(unitcode,sersnor);
            }
        });
        alarmAdapter = new AlarmAdapter(R.layout.alarm_itm);

        alarmAdapter.bindToRecyclerView(reportAlarmRcy);
        alarmAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
//                if(view.getId()==R.id.ly_click){
//                    Bundle bundle = new Bundle();
////                bundle.putString("token",userInfo.getToken());
//
//                    bundle.putString("unitcode",unitcode);
////                bundle.putParcelableArrayList("sensornum",safelist);
//                    bundle.putInt("showsensor",position);
//                    Navigation.findNavController(view).navigate(R.id.action_navigation_alarm_to_mpChartFragment2, bundle);
//                }
                if(view.getId()==R.id.show_clxx){

                    SafeAlarmBean.DataBean item = (SafeAlarmBean.DataBean) adapter.getItem(position);
                    View viewByPosition = adapter.getViewByPosition(position, R.id.more_ly);
                    View imgshow=adapter.getViewByPosition(position, R.id.img_chuliren);
                    if(!item.isIsshow()){
                        viewByPosition.setVisibility(View.VISIBLE);
                        item.setIsshow(true);
                        imgshow.setRotation(0);
                    }
                    else {
                        imgshow.setRotation(180);
                        item.setIsshow(false);
                        viewByPosition.setVisibility(View.GONE);
                    }
                }


            }
        });
        reportAlarmRcy.setAdapter(alarmAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        reportAlarmRcy.setLayoutManager(layoutManager);
    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_report_more;
    }
    private void refresh(String unitcode,String sensors) {
        reportAlarmRfl.setRefreshing(true);
        historyViewModel.getAlarmData(unitcode,sensors);
    }
}
