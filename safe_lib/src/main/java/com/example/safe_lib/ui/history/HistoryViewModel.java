package com.example.safe_lib.ui.history;

import android.app.Activity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.safe_lib.bean.SafeAlarmBean;
import com.example.safe_lib.bean.SafeHistoryBean;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

public class HistoryViewModel extends ViewModel {
    private MutableLiveData<String> isErr = new MutableLiveData<>();
    private MutableLiveData<SafeHistoryBean> mutableLiveData = new MutableLiveData<>();
    private MutableLiveData<SafeAlarmBean> alarmLiveData = new MutableLiveData<>();
    UserInfo userInfo;
    Activity activity;
    public HistoryViewModel() {

    }
    public MutableLiveData<SafeHistoryBean> getData() {
        return mutableLiveData;
    }
    public MutableLiveData<SafeAlarmBean>getAlarmaData(){
        return alarmLiveData;
    }
    public LiveData<String> getErr() {
        return isErr;
    }
    public void getReport(String unitcode,String date){
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", unitcode);
        paramsGet.put("dateStr", date);
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/safety/alarmreport")
                .cls(SafeHistoryBean.class)
                .method(OkGoBuilder.PSOT)
                .params(paramsGet)
                .dialogshow(true)
                .callback(new MyCallback<SafeHistoryBean>() {
                    @Override
                    public void onSuccess(SafeHistoryBean safeHistoryBeanBean, String code) {
                        isErr.postValue(code);
                        mutableLiveData.postValue(safeHistoryBeanBean);
                    }

                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }
                }).build();
    }
    public void set(UserInfo userInfo, Activity activity) {
        this.userInfo = userInfo;
        this.activity =activity;
    }
    public void getAlarmData(String unitcode,String sensors) {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams params = new HttpParams();
        params.put("unitcode", unitcode);
        params.put("sensornum",sensors);
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/safety/alarms")
                .cls(SafeAlarmBean.class)
                .method(OkGoBuilder.GET)
                .headers(headers)
                .params(params)
                .dialogshow(false)
                .callback(new MyCallback<SafeAlarmBean>() {
                    @Override
                    public void onSuccess(SafeAlarmBean safeAlarmBean, String code) {
                        isErr.postValue(code);
                        alarmLiveData.postValue(safeAlarmBean);
                    }
                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }
                }).build();

    }
}