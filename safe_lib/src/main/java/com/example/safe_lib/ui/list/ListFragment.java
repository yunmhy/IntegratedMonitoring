package com.example.safe_lib.ui.list;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.fragment.BaseFragment;
import com.example.safe_lib.R;
import com.example.safe_lib.SafeMainActivity;
import com.example.safe_lib.adapter.ChoseAdapter;
import com.example.safe_lib.adapter.SafeListAdapter;
import com.example.safe_lib.bean.MySensor;
import com.example.safe_lib.bean.SafelistBean;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class ListFragment extends BaseFragment {
    private SwipeRefreshLayout safeListRefer;
    private RecyclerView safeListRcy;
    private ListViewModel listViewModel;
    private UserInfo userInfo;
    private SafeListAdapter listAdapter;
    private String unitcode;
    private OptionsPickerView pvNick;
    private List<UserInfo.UnitinfoBean> unitinfo;
    private TextView listTextNickname;
    private List<SafelistBean.DataBean> safelistBeansData;
    private String sensorname;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userInfo = ((SafeMainActivity) context).toValue();
    }

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        listViewModel =
                ViewModelProviders.of(getActivity()).get(ListViewModel.class);
        super.initBeforeCreateView(savedInstanceState);

    }

    @Override
    protected void initData() {
        View empty = LayoutInflater.from(getContext()).inflate(R.layout.empty_item, null, false);
        listAdapter.setEmptyView(empty);
        final TextView textView = empty.findViewById(R.id.txt_errmsg);
        final ConstraintLayout errlay = empty.findViewById(R.id.err_lay);
        final TextView msg = empty.findViewById(R.id.text_empty);
        final ImageView imageView = empty.findViewById(R.id.empty_img);
        errlay.setVisibility(View.INVISIBLE);
        final Button btn_rest = empty.findViewById(R.id.btn_rest);
        btn_rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh(unitcode);
            }
        });
        listViewModel.set(userInfo, this.getActivity());
        listViewModel.getData().observe(this, new Observer<SafelistBean>() {
            @Override
            public void onChanged(SafelistBean safelistBeans) {
                if(safelistBeans.getCode()==0&&safelistBeans.getData()!=null&&safelistBeans.getData().size()>0){
                    safelistBeansData = safelistBeans.getData();
                    showSensor();
//                    listAdapter.setNewData(safelistBeansData);

                }else {
                    errlay.setVisibility(View.VISIBLE);
                    btn_rest.setVisibility(View.INVISIBLE);
                    textView.setVisibility(View.INVISIBLE);
                    imageView.setVisibility(View.INVISIBLE);
                    msg.setText("暂无数据");
                }

            }
        });

        listViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)) {
                    errlay.setVisibility(View.VISIBLE);
                    textView.setText(s);
                    Toast.makeText(getContext(), "加载数据失败", Toast.LENGTH_LONG).show();
                }
                safeListRefer.setRefreshing(false);
            }
        });

        unitinfo = userInfo.getUnitinfo();
//        listTextNickname.setText(unitinfo.get(0).getNickname());
        unitcode=unitinfo.get(0).getUnitcode();
//        refresh(unitcode);
//        if (listViewModel.getsername().getValue()!=null){
//            initNickPicker(getContext(), listViewModel.getsername().getValue().getSensorname(),"选择传感器类型");
//        }else {
//
//        };
        listViewModel.getsername().observe(this, new Observer<MySensor>() {
            @Override
            public void onChanged(MySensor mySensor) {
                initNickPicker(getContext(), mySensor.getSensorname(),"选择传感器类型");
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        safeListRcy.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (listViewModel.getsername().getValue()==null){
                    refresh(unitcode);
                }
            }
        },500);
    }

    @Override
    protected void initView( View view) {
        view.setBackground(new WaterMarkBg(userInfo.getUseridname()));
        safeListRefer = (SwipeRefreshLayout) view.findViewById(R.id.safe_list_refer);
        safeListRcy = (RecyclerView) view.findViewById(R.id.safe_list_rcy);
        safeListRcy.setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));
        listTextNickname = (TextView) view.findViewById(R.id.list_text_nickname);
        safeListRefer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh(unitcode);
            }
        });
        listAdapter = new SafeListAdapter(R.layout.safe_list_item);
        listTextNickname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvNick.show(v);
            }
        });
        listAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                Bundle bundle = new Bundle();
                SafelistBean.DataBean dataBean= (SafelistBean.DataBean)adapter.getData().get(position);
//                bundle.putString("token",userInfo.getToken());
                bundle.putString("unitcode",unitcode);
                bundle.putString("sensornum",dataBean.getSensornum());
                Navigation.findNavController(view).navigate(R.id.action_navigation_list_to_listMoreFragment,bundle);
            }
        });
        safeListRcy.setAdapter(listAdapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        safeListRcy.setLayoutManager(layoutManager);
    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_list;
    }
    private void initNickPicker(final Context context, final List<String> nicklist, final String title) {
//        final List<String>sensorlist=new ArrayList<>();
//        for (int i = 0; i < nicklist.size(); i++) {
//            sensorlist.add(nicklist.get(i).getSensorname());
//        }
//        HashSet h = new HashSet(sensorlist);
//        sensorlist.clear();
//        sensorlist.addAll(h);
        pvNick= new OptionsPickerBuilder(context, new OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int options2, int options3, View v) {
                TextView tv = (TextView) v.findViewById(v.getId());
                sensorname = nicklist.get(options1);
                tv.setText(sensorname);
                if(safelistBeansData!=null&&safelistBeansData.size()>0){
                    showSensor();
                }

            }

        })
                .isDialog(true)
                .setContentTextSize(18)
                .setLayoutRes(R.layout.pickerview_custom_nick, new CustomListener() {
                    @Override
                    public void customLayout(View v) {
                        final TextView tvTitle=(TextView) v.findViewById(R.id.nick_title);
                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
                        final TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
                        tvTitle.setText(title);
                        tvSubmit.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvNick.returnData();
                                pvNick.dismiss();
                            }
                        });
                        ivCancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                pvNick.dismiss();
                            }
                        });
                    }
                })
                .setTextColorCenter(getResources().getColor(R.color.blue))
                .setItemVisibleCount(9)
                .setLineSpacingMultiplier((float) 2.6)
                .setBackgroundId(R.color.black)
                .build();

        pvNick.setPicker(nicklist);//添加数据源

    }

    private void showSensor() {
        if (sensorname==null){
            listAdapter.setNewData(safelistBeansData);
        }else {
            List<SafelistBean.DataBean>temlist=new ArrayList<>();
            Iterator<SafelistBean.DataBean> iterator = safelistBeansData.iterator();
            while (iterator.hasNext()){
                SafelistBean.DataBean bean = iterator.next();
                if (bean.getSensorname().equals(sensorname)){
                    temlist.add(bean);
                }
            }
            listAdapter.setNewData(temlist);
        }


    }

    private void refresh(String unitcode) {
        safeListRefer.setRefreshing(true);
        listViewModel.getSensorname(false);
//        listViewModel.getSfaeListData(unitcode);
    }
}