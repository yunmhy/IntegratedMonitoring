package com.example.safe_lib.ui.alarm;

import android.app.Activity;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.safe_lib.bean.LineChartBean;
import com.example.safe_lib.bean.SafeAlarmBean;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

public class AlarmViewModel extends AndroidViewModel {

    private MutableLiveData<String> isErr = new MutableLiveData<>();
    private MutableLiveData<SafeAlarmBean> mutableLiveData = new MutableLiveData<>();
    private Application application;
    UserInfo userInfo;
    Activity activity;
    public String unitcode;

    public AlarmViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

    }

    public MutableLiveData<SafeAlarmBean> getData() {
        return mutableLiveData;
    }
    public LiveData<String> getErr() {
        return isErr;
    }
    public void getAlarmData(String unitcode) {
        this.unitcode=unitcode;
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams params = new HttpParams();
        params.put("unitcode", unitcode);
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/safety/alarms")
                .cls(SafeAlarmBean.class)
                .method(OkGoBuilder.GET)
                .headers(headers)
                .params(params)
                .dialogshow(false)
                .callback(new MyCallback<SafeAlarmBean>() {
                    @Override
                    public void onSuccess(SafeAlarmBean safeAlarmBean, String code) {
                        isErr.postValue(code);
                        mutableLiveData.postValue(safeAlarmBean);
                    }
                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }
                }).build();

    }

    public void set(UserInfo userInfo, Activity activity) {
        this.userInfo = userInfo;
       this.activity =activity;
        }

}