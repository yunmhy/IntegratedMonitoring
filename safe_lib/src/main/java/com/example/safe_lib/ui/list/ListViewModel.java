package com.example.safe_lib.ui.list;

import android.app.Activity;
import android.app.Application;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.safe_lib.bean.MySensor;
import com.example.safe_lib.bean.SafelistBean;
import com.example.safe_lib.bean.Sensorname;
import com.example.safe_lib.bean.SensorsInfo;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class ListViewModel extends ViewModel {

    private MutableLiveData<String> isErr = new MutableLiveData<>();
    private MutableLiveData<SafelistBean> mutableLiveData = new MutableLiveData<>();
    private MutableLiveData<SensorsInfo> sensorsLiveData = new MutableLiveData<>();
    private MutableLiveData<MySensor>sensorlist= new MutableLiveData<>();
    UserInfo userInfo;
    Activity activity;
    private List<String> sernamelist;

    public ListViewModel() {

    }
    public void getSensorname(final boolean f) {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams params = new HttpParams();
        params.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/safety/sensorname")
                .cls(Sensorname.class)
                .method(OkGoBuilder.GET)
                .headers(headers)
                .params(params)
                .dialogshow(f)
                .callback(new MyCallback<Sensorname>() {
                    @Override
                    public void onSuccess(final Sensorname safelistBean, String code) {
                        if (safelistBean.getData() != null) {
                            sernamelist = new ArrayList<>();
                            Iterator<Sensorname.DataBean> iterator = safelistBean.getData().iterator();
                            while (iterator.hasNext()){
                                sernamelist.add(iterator.next().getSensorname());
                            }

                            getSfaeListData(userInfo.getUnitinfo().get(0).getUnitcode(),f);
                        }

                    }

                    @Override
                    public void onError(String e, String code) {
                        Toast.makeText(activity.getApplication(), "获取传感器信息失败", Toast.LENGTH_SHORT).show();
                    }
                }).build();
    }
    public void getSfaeListData(String unitcode,boolean f) {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams params = new HttpParams();
        params.put("unitcode", unitcode);
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/safety/sensors")
                .cls(SafelistBean.class)
                .method(OkGoBuilder.GET)
                .headers(headers)
                .params(params)
                .dialogshow(f)
                .callback(new MyCallback<SafelistBean>() {
                    @Override
                    public void onSuccess(SafelistBean safelistBean, String code) {
                        if (safelistBean.getData()!=null){
                            isErr.postValue(code);
                            mutableLiveData.postValue(safelistBean);
                            initSensors(safelistBean.getData());
                        }

                    }
                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }
                }).build();

    }

    public void set(UserInfo userInfo, FragmentActivity activity) {
        this.userInfo = userInfo;
        this.activity =activity;
    }

    public void getSensorsInfo(String unitcode,String sensornum){
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams params = new HttpParams();
        params.put("unitcode", unitcode);
        params.put("sensornum", sensornum);
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/safety/sensorsdefine")
                .cls(SensorsInfo.class)
                .method(OkGoBuilder.GET)
                .headers(headers)
                .params(params)
                .dialogshow(true)
                .callback(new MyCallback<SensorsInfo>() {
                    @Override
                    public void onSuccess(SensorsInfo sensorsInfo, String code) {
                            isErr.postValue(code);
                            sensorsLiveData.postValue(sensorsInfo);
                    }
                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }
                }).build();

    }

    public LiveData<SafelistBean> getData() {
        return mutableLiveData;
    }
    public LiveData<SensorsInfo>getSensors(){
        return sensorsLiveData;
    }
    public LiveData<MySensor>getsername(){return sensorlist;}
    public LiveData<String> getErr() {
        return isErr;
    }

    private void initSensors(List<SafelistBean.DataBean> data) {

        List<List<String>> sensornumlist = new ArrayList<>();
        HashSet h = new HashSet();
        for (int i = 0; i < sernamelist.size(); i++) {
            String name = sernamelist.get(i);
            ArrayList<String> numList = new ArrayList<>();
            for (int j = 0; j < data.size(); j++) {
                if (data.get(j).getSensorname()!=null&&data.get(j).getSensorname().equals(name)) {
                    numList.add(data.get(j).getSensornum());
                }
            }
            h.clear();
            h.addAll(numList);
            numList.clear();
            numList.addAll(h);
            sensornumlist.add(numList);
        }
        sensorlist.postValue(new MySensor(sernamelist,sensornumlist));
    }

}