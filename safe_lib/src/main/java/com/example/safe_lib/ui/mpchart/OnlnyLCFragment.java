package com.example.safe_lib.ui.mpchart;


import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.fragment.BaseFragment;
import com.example.base_lib.utils.TimeUtils;
import com.example.safe_lib.R;
import com.example.safe_lib.SafeMainActivity;
import com.example.safe_lib.bean.LineChartBean;
import com.example.safe_lib.bean.SafeAlarmBean;
import com.example.safe_lib.ui.alarm.AlarmViewModel;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.MPPointD;
import com.github.mikephil.charting.utils.Transformer;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class OnlnyLCFragment extends BaseFragment {
    private RelativeLayout onlyLy;
    private TextView olcNickname;
    private TextView t_ser_name;
    private TextView t_ser_code;
    private TextView t_ser_place;
    private LineChart onlyLineC;
    private String unitcode;
    private int pos;
    private String token;
    private String startTime;
    private String endTime;
    private ArrayList<String> mXData=new ArrayList<>();
    private UserInfo userInfo;
    private AlarmViewModel alarmViewModel;
    private ArrayList<SafeAlarmBean.DataBean> sensornums;
    private String sensornum;
    private String nickname;
    private YAxis yAxisLeft;

    public OnlnyLCFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        alarmViewModel =
                ViewModelProviders.of(getActivity()).get(AlarmViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userInfo = ((SafeMainActivity) context).toValue();
    }
    @Override
    protected void initData() {
        SafeAlarmBean safeAlarmBean = alarmViewModel.getData().getValue();
        if(safeAlarmBean!=null){
            sensornums = safeAlarmBean.getData();
        }
        Bundle bundle = getArguments();
        nickname = bundle.getString("nickname");
        unitcode = bundle.getString("unitcode");
        pos = bundle.getInt("showsensor");
        token = userInfo.getToken();
        startTime = TimeUtils.CurveTimeFarmat(sensornums.get(pos).getStarttime());
        endTime = TimeUtils.CurveTimeFarmat(sensornums.get(pos).getEndtime()==null?"":sensornums.get(pos).getEndtime());
        sensornum = sensornums.get(pos).getSensornum();
        olcNickname.setText(nickname);
        t_ser_name.setText(sensornums.get(pos).getSensorname());
        t_ser_code.setText(sensornums.get(pos).getSensornum());
        t_ser_place.setText(sensornums.get(pos).getSensorplace());
        getChartData();
    }

    @Override
    protected void initView(View view) {
        onlyLy = (RelativeLayout) view.findViewById(R.id.only_ly);
        olcNickname = (TextView) view.findViewById(R.id.olc_nickname);
        t_ser_name = (TextView) view.findViewById(R.id.t_ser_name);
        t_ser_code= (TextView) view.findViewById(R.id.t_ser_code);
        t_ser_place = (TextView) view.findViewById(R.id.t_ser_place);
        onlyLineC = (LineChart) view.findViewById(R.id.only_line_c);
        view.findViewById(R.id.only_by).setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));
        initLineChart();

    }

    private void initLineChart() {

//        onlyLineC.getLegend().setEnabled(false);// 不显示图例
        onlyLineC.getLegend().setTextSize(14);
        onlyLineC.getDescription().setEnabled(true);
        onlyLineC.getDescription().setText("时间");// 不显示描述
//        onlyLineC.setScaleEnabled(false);   // 取消缩放
        onlyLineC.setNoDataText("请稍等...");// 没有数据的时候默认显示的文字
        onlyLineC.setNoDataTextColor(Color.BLUE);
        onlyLineC.setBorderColor(Color.BLUE);
        onlyLineC.setTouchEnabled(true);
        onlyLineC.setDragEnabled(true);
        // 如果x轴label文字比较大，可以设置边距
        onlyLineC.setExtraRightOffset(25f);
        onlyLineC.setExtraBottomOffset(10f);
        onlyLineC.setExtraTopOffset(10f);
        onlyLineC.setDrawBorders(false);//禁止绘制图表边框的线
        //设置是否可以触摸
        onlyLineC.setTouchEnabled(true);
        //设置是否可以拖拽
        onlyLineC.setDragEnabled(true);
        onlyLineC.setExtraRightOffset(25f);
        onlyLineC.setExtraLeftOffset(10f);

        setYAxis();
        setXAxis();

    }

    private void setYAxis() {
// 不显示右侧Y轴
        YAxis yAxisRight = onlyLineC.getAxisRight();
        yAxisRight.setEnabled(false);
        yAxisLeft = onlyLineC.getAxisLeft();
        // 强制显示Y轴6个坐标
//        yAxisLeft.setLabelCount(4, true);
        // 将y轴0点轴的颜色设置为透明
        yAxisLeft.setZeroLineColor(Color.WHITE);
        yAxisLeft.setTextColor(Color.parseColor("#8F8E94"));
        yAxisLeft.setTextSize(10);
        //不显示最顶部的label
        yAxisRight.setDrawTopYLabelEntry(false);
        // 设置y轴网格的颜色
        yAxisLeft.setGridColor(Color.parseColor("#8F8E94"));
//         yAxisLeft.setGranularity(0.5f);
        yAxisLeft.enableGridDashedLine(10f, 10f, 0f);
//        yAxisLeft.setAxisMaximum(0.0f);
        //Y方向文字的位置，在线外侧.(默认在外侧)
        yAxisLeft.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        //设置label偏移量，正值向下，负值向上
//         yAxisLeft.setYOffset(-10f);
        yAxisLeft.setSpaceTop(15f);


    }

    private void setLowLimitLine(float low,YAxis yAxis) {
        LimitLine hightLimit = new LimitLine(low, "报警下限");
        hightLimit.setLineWidth(1f);
        hightLimit.setTextSize(10f);
        hightLimit.enableDashedLine(10f, 10f, 0f);
        hightLimit.setLineColor(Color.RED);
        hightLimit.setTextColor(Color.RED);
        yAxis.addLimitLine(hightLimit);
        onlyLineC.invalidate();
    }

    private void setHightLimitLine(float hight,YAxis yAxis) {
        LimitLine hightLimit = new LimitLine(hight, "报警上限");
        hightLimit.setLineWidth(1f);
        hightLimit.enableDashedLine(10f, 10f, 0f);
        hightLimit.setTextSize(10f);
        hightLimit.setLineColor(Color.RED);
        hightLimit.setTextColor(Color.RED);
        yAxis.addLimitLine(hightLimit);
        onlyLineC.invalidate();
    }

    private void setXAxis() {
        // 设置x轴的数据
        XAxis xAxis = onlyLineC.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(Color.parseColor("#8F8E94"));
        xAxis.setTextSize(10);
        // 设置x轴网格的颜色
        xAxis.setGridColor(Color.parseColor("#8F8E94"));
        xAxis.setGranularity(1.0f);
        //如果设置为true，则在绘制时会避免“剪掉”在x轴上的图表或屏幕边缘的第一个和最后一个坐标轴标签项。
        // xAxis.setAvoidFirstLastClipping(true);
        // x轴最左多出空n个坐标
         xAxis.setSpaceMax(2.0f);
        // 让左侧x轴不从0点开始
         xAxis.setSpaceMin(2.0f);

// 获取到数据后，格式化x轴的数据
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float v, AxisBase axisBase) {
                int index = (int) v;
                // 只显示首尾日期

                if (index < 0 || index >= mXData.size()) {//防止出现数组越界;
                    return "";
                } else {
                    //  Log.e("index&&value", "value: "+ value +" index"+index+" time"+format_realtimes.get(index));
                    return mXData.get(index);
                }
//                if (i == 0 || i == mXData.size() - 1) {
//
//                } else {
//                    return "";
//                }
            }
        });
    }
    public void getChartData() {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", token);
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", unitcode);
        paramsGet.put("sensornum", sensornum);
        paramsGet.put("starttime", startTime);
        paramsGet.put("endtime", endTime);
        OkGoBuilder.getInstance()
                .Builder(getActivity())
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/safety/curve")
                .cls(LineChartBean.class)
                .method(OkGoBuilder.GET)
                .headers(headers)
                .params(paramsGet)
                .dialogshow(true)
                .callback(new MyCallback<LineChartBean>() {
                    @Override
                    public void onSuccess(LineChartBean data, String code) {
                        if (data.getData() != null && data.getData().size() > 0) {
                            initChartData(data);
                        }else {

                            Toast.makeText(getContext(),"无数据",Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onError(String e, String code) {
                        Log.e("linechart", e);
                    }
                }).build();
    }

    private void initChartData(LineChartBean data) {
        LineDataSet.Mode mode= LineDataSet.Mode.CUBIC_BEZIER;
        if (data.getSensorInfo().get(0).getTypename().equals("开关量")){
            formatY(data.getSensorInfo().get(0));
            mode= LineDataSet.Mode.STEPPED;
        }
        if (data.getSensorInfo().get(0).getTypename().equals("控制量")||data.getSensorInfo().get(0).getTypename().equals("多态量"))mode= LineDataSet.Mode.STEPPED;
        double warningvalueup = data.getSensorInfo().get(0).getWarningvalueup();
        double warningvaluedown = data.getSensorInfo().get(0).getWarningvaluedown();
        //设置高限制线
        if(warningvalueup!=0.0&&mode.name().equals(LineDataSet.Mode.CUBIC_BEZIER.name())){
            setHightLimitLine((float) warningvalueup,yAxisLeft);
        }
       if (warningvaluedown!=0.0&&mode.name().equals(LineDataSet.Mode.CUBIC_BEZIER.name())){
           if (warningvalueup==0.0||warningvaluedown!=warningvaluedown){
               //设置底限制线
               setLowLimitLine((float)warningvaluedown,yAxisLeft);
           }
       }

        int position=0;
        // 获取的坐标数据集合
        List<Entry> entries = new ArrayList<>();
        Iterator<LineChartBean.DataBean> iterator = data.getData().iterator();
        while (iterator.hasNext()) {
            final LineChartBean.DataBean bean = iterator.next();
            entries.add(new Entry(position, (float) bean.getRealvalue()));

            mXData.add(TimeUtils.chartTimeFarmat(bean.getDatatime()));
            position++;
        }


        String unit;
        if(sensornums.get(pos).getSensorunit()==null){
            unit=sensornums.get(pos).getTypename();
        }else unit=" 单位:"+sensornums.get(pos).getSensorunit();
// 创建数据的包装类
        LineDataSet lineDataSet = new LineDataSet(entries, unit);
        float Ymax=entries.get(0).getY();
        float Xmax=entries.get(0).getX();
        float Ymin=entries.get(0).getY();
        float Xmin=entries.get(0).getX();
        for(Entry e:entries){
            if (Ymax<e.getY()){Ymax=e.getY();Xmax=e.getX();}
            if (Ymin>e.getY()){Ymax=e.getY();Xmin=e.getX();}
        }
        Transformer trans = onlyLineC.getTransformer(lineDataSet.getAxisDependency());
        MPPointD pointD = trans.getPixelForValues(Xmax, Ymax);
        Paint paintDrawPointFill = new Paint(Paint.ANTI_ALIAS_FLAG);
        paintDrawPointFill.setStyle(Paint.Style.FILL);
        paintDrawPointFill.setColor(Color.WHITE);
//        Canvas c =new Canvas();
//        c.drawCircle((float) pointD.x, (float) pointD.y, ScreenUnit.dp2px(getContext(), 6),paintDrawPointFill);

// 点击圆点不显示高亮
        lineDataSet.setDrawHighlightIndicators(false);
        lineDataSet.setMode(mode);

// 设置折线的颜色
        lineDataSet.setColor(Color.BLUE);
// 填充颜色(渐变色)
//        lineDataSet.setDrawFilled(true);
//        lineDataSet.setFillDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
//                new int[]{Color.parseColor("#ff4472fa"), Color.parseColor("#004472fa")}));
        lineDataSet.setLineWidth(2f);
// 坐标不显示值
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawCircles(false);//在点上画圆 默认true
        LineData lineData = new LineData(lineDataSet);
        LineChartMarkView mv = new LineChartMarkView(getContext(), mXData,data.getSensorInfo().get(0));
        onlyLineC.setData(lineData);
        mv.setChartView(onlyLineC);
        onlyLineC.setMarker(mv);
//        float ratio = (float) mXData.size() / (float) 12;
        //显示的时候是按照多大的比率缩放显示,1f表示不放大缩小
        onlyLineC.zoom(1f, 1f, 0, 0);
    }


    private void formatY(final LineChartBean.SensorInfoBean sensorInfoBean) {
        yAxisLeft.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if (value==0.0f){return sensorInfoBean.getOffdescribe();}
                if (value==1.0f){return sensorInfoBean.getOndescribe();}
                return "";
            }
        });

    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_onlny_lc;
    }

}
