package com.example.safe_lib.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.safe_lib.R;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

public class AddSerAdapter extends BaseQuickAdapter<String,BaseViewHolder> {
    public AddSerAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, String item) {
        helper.addOnClickListener(R.id.img_del);
        helper.setText(R.id.ser_type,item.split("-")[0]).setText(R.id.ser_code,item.split("-")[1]);
    }



}
