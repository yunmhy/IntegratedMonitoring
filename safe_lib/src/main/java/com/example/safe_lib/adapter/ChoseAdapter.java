package com.example.safe_lib.adapter;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.example.base_lib.baen.UserInfo;
import com.example.safe_lib.R;

import java.util.List;

public class ChoseAdapter extends BaseAdapter {
    private Context context ;
    private List<String> list;

    public ChoseAdapter(Context context, List<String> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }




    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView== null){
            holder= new ViewHolder();
            convertView= LayoutInflater.from(context).inflate(R.layout.spiner_item,null);
            holder.textView= convertView.findViewById(R.id.item_nickname);

            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }

        holder.textView.setText(list.get(position));
        return convertView;
    }


    @Override
    public boolean isEmpty() {
        return false;
    }

    private class ViewHolder{
        TextView textView;

    }
}
