package com.example.safe_lib.adapter;

import android.graphics.Color;
import android.text.TextPaint;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import com.example.base_lib.utils.TimeUtils;
import com.example.safe_lib.R;
import com.example.safe_lib.bean.SafeAlarmBean;


public class AlarmAdapter extends BaseQuickAdapter<SafeAlarmBean.DataBean, BaseViewHolder> {


    public AlarmAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(final BaseViewHolder helper, SafeAlarmBean.DataBean item) {
        helper.addOnClickListener(R.id.show_clxx).addOnClickListener(R.id.ly_click);
        TextView view = helper.getView(R.id.text_end_time);
        TextPaint tp = view.getPaint();
        if(item.getEndtime()==null){
            tp.setFakeBoldText(true);
            view.setTextColor(Color.parseColor("#FF0000"));
        }
        String value=item.getAlarmvalue();
        if (item.getTypename()!=null&&item.getTypename().equals("开关量")){
            if (value.equals("0.00")){
                value=item.getOffdescribe();
            }else value=item.getOndescribe();
        }
        helper.setText(R.id.text_cgq_name, (helper.getPosition() + 1) + " " + "-" + " " + item.getSensorname()).setText(R.id.text_cgq_id, item.getSensornum())
                .setText(R.id.text_start_time, TimeUtils.CurveTimeFarmat(item.getStarttime())).setText(R.id.text_time_long, "" + item.getPersisttime() + "秒")
                .setText(R.id.txt_max, value + (item.getSensorunit()==null?"":item.getSensorunit())).setText(R.id.txt_stats, item.getStatusdec())
                .setText(R.id.text_cgq_local, item.getSensorplace()).setText(R.id.txt_stationid, item.getStationnum())
                .setText(R.id.text_end_time, item.getEndtime()==null ? "正在报警" : TimeUtils.CurveTimeFarmat(item.getEndtime()))
                .setText(R.id.txt_cl_timer, item.getMinedispose_time())
                .setText(R.id.txt_clcs, item.getMinedispose()).setText(R.id.txt_manager, item.getMinedispose_person())
                .setText(R.id.txt_reason, item.getAlarmreason());

    }


}
