package com.example.safe_lib.adapter;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.safe_lib.R;
import com.example.safe_lib.bean.SafeHistoryBean;

import java.util.List;

public class HistoryAdapter extends BaseQuickAdapter<SafeHistoryBean.DataBean, BaseViewHolder> {

    public HistoryAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, SafeHistoryBean.DataBean item) {
            helper.setText(R.id.txt_history_name,item.getSensorname()).setText(R.id.txt_history_value,item.getAlarmnum()+"");
    }
}
