package com.example.safe_lib.adapter;



import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;

import com.example.safe_lib.R;
import com.example.safe_lib.bean.SafelistBean;

public class SafeListAdapter extends BaseQuickAdapter <SafelistBean.DataBean,BaseViewHolder>{

    public SafeListAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, SafelistBean.DataBean item) {
        helper.setText(R.id.list_cgq_name,(helper.getPosition()+1)+" "+"-"+" "+item.getSensorname())
                .setText(R.id.list_cgq_id,item.getSensornum())
                .setText(R.id.list_cgq_place,item.getSensorplace())
                .setText(R.id.list_value,item.getRealvalue()+item.getSensorunit()==null?"":item.getSensorunit())
                .setText(R.id.list_stats,item.getStatusdec());

    }




}
