package com.example.safe_lib.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.base_lib.utils.TimeUtils;
import com.example.safe_lib.R;
import com.example.safe_lib.bean.LineChartBean;
import com.example.safe_lib.bean.MyLineChartData;
import com.example.safe_lib.ui.mpchart.LineChartMarkView;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class LineChartAdapter extends BaseQuickAdapter<MyLineChartData, BaseViewHolder> {
    private ArrayList<String> mXData;
    private LineData lineData;
    private Context context;
    private YAxis yAxisLeft;

    public LineChartAdapter(Context context,int layoutResId) {
        super(layoutResId);
        this.context=context;
    }

    @Override
    protected void convert(BaseViewHolder helper, MyLineChartData item) {
        String unit;
        LineChartBean.SensorInfoBean infoBean = item.getBeanList().get(0).getData().getSensorInfo().get(0);
        if(infoBean.getSensorunit()==null){
            unit="";
        }else unit="单位："+ infoBean.getSensorunit();
        helper.setText(R.id.lc_ser_name,item.getSersnorname()).setText(R.id.lc_ser_unit,unit);
        LineChart lineChart = (LineChart) helper.getView(R.id.line_c);
        initLineChart(lineChart);

        List<MyLineChartData.MyLineChartBean> beanList = item.getBeanList();
        Iterator<MyLineChartData.MyLineChartBean> iterator=beanList.iterator();
        double hight = infoBean.getWarningvalueup();
        double low = infoBean.getWarningvaluedown();

        LineDataSet.Mode mode= LineDataSet.Mode.CUBIC_BEZIER;
        if (infoBean.getTypename().equals("开关量")){
            formatY(infoBean);
            mode= LineDataSet.Mode.STEPPED;
        }
        if (infoBean.getTypename().equals("控制量")||infoBean.getTypename().equals("多态量"))mode= LineDataSet.Mode.STEPPED;

        //设置高限制线
        if(hight!=0.0&&mode.name().equals(LineDataSet.Mode.CUBIC_BEZIER.name())){
            setHightLimitLine((float) hight,yAxisLeft,lineChart);
        }
        if (low!=0.0&&mode.name().equals(LineDataSet.Mode.CUBIC_BEZIER.name())){
            if (hight==0.0||low!=low){
                //设置底限制线
                setLowLimitLine((float)low,yAxisLeft,lineChart);
            }
        }
        lineData = new LineData();
        String color="4472FA";
        while (iterator.hasNext()){
            MyLineChartData.MyLineChartBean chartBean = iterator.next();
            mXData=new ArrayList<>();
            initChartData(chartBean,lineChart,color);
            color="F04134";
        }


    }

    private void formatY(final LineChartBean.SensorInfoBean infoBean) {
        yAxisLeft.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if (value==0.0f){return infoBean.getOffdescribe();}
                if (value==1.0f){return infoBean.getOndescribe();}
                return "";
            }
        });
    }

    private void initLineChart(LineChart onlyLineC) {
//        onlyLineC.getLegend().setEnabled(false);// 不显示图例
        onlyLineC.getLegend().setTextSize(14);
        onlyLineC.getDescription().setEnabled(true);
        onlyLineC.getDescription().setText("时间");// 不显示描述
//        onlyLineC.setScaleEnabled(false);   // 取消缩放
        onlyLineC.setNoDataText("请稍等...");// 没有数据的时候默认显示的文字
        onlyLineC.setNoDataTextColor(Color.BLUE);
        onlyLineC.setBorderColor(Color.BLUE);
        onlyLineC.setTouchEnabled(true);
        onlyLineC.setDragEnabled(true);
        // 如果x轴label文字比较大，可以设置边距
        onlyLineC.setExtraRightOffset(25f);
        onlyLineC.setExtraBottomOffset(10f);
        onlyLineC.setExtraTopOffset(10f);
        onlyLineC.setDrawBorders(false);//禁止绘制图表边框的线
        //设置是否可以触摸
        onlyLineC.setTouchEnabled(true);
        //设置是否可以拖拽
        onlyLineC.setDragEnabled(true);
        onlyLineC.setExtraRightOffset(25f);
        onlyLineC.setExtraLeftOffset(10f);
        setYAxis(onlyLineC);
        setXAxis(onlyLineC);

    }

    private void setYAxis(LineChart onlyLineC) {
// 不显示右侧Y轴
        YAxis yAxisRight = onlyLineC.getAxisRight();
        yAxisRight.setEnabled(false);
        yAxisLeft = onlyLineC.getAxisLeft();
        // 强制显示Y轴6个坐标
//        yAxisLeft.setLabelCount(5, true);
        // 将y轴0点轴的颜色设置为透明
        yAxisLeft.setZeroLineColor(Color.WHITE);
        yAxisLeft.setTextColor(Color.parseColor("#8F8E94"));
        yAxisLeft.setTextSize(10);
        //不显示最顶部的label
        yAxisRight.setDrawTopYLabelEntry(false);
        // 设置y轴网格的颜色
        yAxisLeft.setGridColor(Color.parseColor("#8F8E94"));
//         yAxisLeft.setGranularity(0.5f);
        yAxisLeft.enableGridDashedLine(10f, 10f, 0f);
//        yAxisLeft.setAxisMaximum(0.0f);
        //Y方向文字的位置，在线外侧.(默认在外侧)
        yAxisLeft.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        //设置label偏移量，正值向下，负值向上
        // yAxisLeft.setYOffset(-10f);
        yAxisRight.setSpaceTop(15f);

    }

    private void setXAxis(LineChart onlyLineC) {
        // 设置x轴的数据
        XAxis xAxis = onlyLineC.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextColor(Color.parseColor("#8F8E94"));
        xAxis.setTextSize(10);
        // 设置x轴网格的颜色
        xAxis.setGridColor(Color.parseColor("#8F8E94"));
        xAxis.setGranularity(1.0f);
        //如果设置为true，则在绘制时会避免“剪掉”在x轴上的图表或屏幕边缘的第一个和最后一个坐标轴标签项。
        // xAxis.setAvoidFirstLastClipping(true);
        // x轴最左多出空n个坐标
        xAxis.setSpaceMax(10.0f);
        // 让左侧x轴不从0点开始
        xAxis.setSpaceMin(10.0f);

// 获取到数据后，格式化x轴的数据
        xAxis.setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float v, AxisBase axisBase) {
                int index = (int) v;
                // 只显示首尾日期

                if (index < 0 || index >= mXData.size()) {//防止出现数组越界;
                    return "";
                } else {
                    //  Log.e("index&&value", "value: "+ value +" index"+index+" time"+format_realtimes.get(index));
                    return mXData.get(index);
                }
//                if (i == 0 || i == mXData.size() - 1) {
//
//                } else {
//                    return "";
//                }
            }
        });
    }
    private void setLowLimitLine(float low,YAxis yAxis,LineChart onlyLineC) {
        LimitLine hightLimit = new LimitLine(low, "报警下限");
        hightLimit.setLineWidth(1f);
        hightLimit.setTextSize(10f);
        hightLimit.enableDashedLine(10f, 10f, 0f);
        hightLimit.setLineColor(Color.RED);
        hightLimit.setTextColor(Color.RED);
        yAxis.addLimitLine(hightLimit);
        onlyLineC.invalidate();
    }

    private void setHightLimitLine(float hight,YAxis yAxis,LineChart onlyLineC) {
        LimitLine hightLimit = new LimitLine(hight, "报警上限");
        hightLimit.setLineWidth(1f);
        hightLimit.setTextSize(10f);
        hightLimit.enableDashedLine(10f, 10f, 0f);
        hightLimit.setLineColor(Color.RED);
        hightLimit.setTextColor(Color.RED);
        yAxis.addLimitLine(hightLimit);
        onlyLineC.invalidate();
    }

    private void initChartData(MyLineChartData.MyLineChartBean data, LineChart lineChart,String color) {

        int position=0;
        // 获取的坐标数据集合
        List<Entry> entries = new ArrayList<>();
        Iterator<LineChartBean.DataBean> iterator = data.getData().getData().iterator();
        while (iterator.hasNext()) {
            final LineChartBean.DataBean bean = iterator.next();
            entries.add(new Entry(position, (float) bean.getRealvalue()));
            mXData.add(TimeUtils.chartTimeFarmat(bean.getDatatime()));
            position++;
        }

// 创建数据的包装类
        LineDataSet lineDataSet = new LineDataSet(entries, data.getSensornum()+" "+data.getData().getSensorInfo().get(0).getSensorplace());
// 点击圆点不显示高亮
        lineDataSet.setDrawHighlightIndicators(false);
// 设置折线的颜色
        lineDataSet.setColor(Color.parseColor("#ff"+color));
// 填充颜色(渐变色)
//        lineDataSet.setDrawFilled(true);
//        lineDataSet.setFillDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM,
//                new int[]{Color.parseColor("#ff"+color), Color.parseColor("#00"+color)}));
        lineDataSet.setLineWidth(2f);
// 坐标不显示值
        lineDataSet.setDrawValues(false);
        lineDataSet.setDrawCircles(false);//在点上画圆 默认true
        lineData.addDataSet(lineDataSet);
        lineChart.setData(lineData);
        LineChartMarkView mv = new LineChartMarkView(context, mXData,data.getData().getSensorInfo().get(0));
        mv.setChartView(lineChart);
        lineChart.setMarker(mv);
//        float ratio = (float) mXData.size() / (float) 12;
        //显示的时候是按照多大的比率缩放显示,1f表示不放大缩小
        lineChart.zoom(1f, 1f, 0, 0);
    }
//    public void setMarkerView() {
//        LineChartMarkView mv = new LineChartMarkView(, time);
//        mv.setChartView(lineC);
//        lineC.setMarker(mv);
//
//    }
}
