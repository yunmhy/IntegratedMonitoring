//package com.example.login_lib.common;
//
//import android.content.Context;
//import android.graphics.Bitmap;
//import android.os.CountDownTimer;
//import android.os.Handler;
//
//import android.view.TextureView;
//import android.widget.Toast;
//
//import com.arcsoft.face.ErrorInfo;
//import com.arcsoft.face.FaceFeature;
//import com.arcsoft.face.FaceInfo;
//
//import com.example.base_lib.MyCallback;
//import com.example.base_lib.baen.MyConstants;
//import com.example.base_lib.utils.FileUtil;
//import com.example.login_lib.fragment.SetFaceFragment;
//import com.sxygsj.android.common_hr.cameraUtil.CameraUtil;
//import com.sxygsj.android.common_hr.entity.HRFaceInfo;
//import com.sxygsj.android.common_hr.util.ByteUtil;
//import com.sxygsj.android.common_hr.util.face.HRFaceHelper;
//
//import java.io.File;
//import java.util.List;
//import java.util.concurrent.ExecutorService;
//import java.util.concurrent.Executors;
//
//public class FaceVerification {
//    float score = 0.8f;
//    private boolean isFaceRecognize = false;
//    private HRFaceHelper mHRFaceHelper;
//    private FaceFeature basefaceFeature;
//    private TextureView textureView;
//    private MyCallback myCallback;
//    private Context context;
//    private CameraUtil cameraUtil;
//    private ExecutorService service;
//    public FaceRecognitionThread mFaceRecognitionThread = null;
//    private CountDownTimer mCountDownTimer = null;
//
//    public FaceVerification(Context context) {
//        this.context = context;
//
//        mCountDownTimer = new CountDownTimer(10 * 1000, 1 * 1000) {
//            @Override
//            public void onTick(long millisUntilFinished) {
//
//            }
//
//            @Override
//            public void onFinish() {
//                isFaceRecognize = false;
//                myCallback.onError("验证超时", MyConstants.FAILED);
//
//            }
//        };
//        mFaceRecognitionThread = new FaceRecognitionThread();
//    }
//
//    public void setMyCallback(MyCallback myCallback) {
//        this.myCallback = myCallback;
//    }
//
//    public void initHr() {
//        mHRFaceHelper = new HRFaceHelper.Builder()
//                .appId(com.sxygsj.android.common_hr.util.Constants.APP_ID)
//                .key(com.sxygsj.android.common_hr.util.Constants.SDK_KEY)
//                .context(context)
//                .build();
//        int init = mHRFaceHelper.init(HRFaceHelper.ASF_DETECT_MODE_VIDEO);
//        if (init != ErrorInfo.MOK) {
//            Toast.makeText(context, "初始化失败，请检查网络", Toast.LENGTH_LONG).show();
//            return;
//        }
//    }
//
//    public void setFaceFeature(FaceFeature basefaceFeature) {
//        this.basefaceFeature = basefaceFeature;
//    }
//
//    public void setTextureView(TextureView textureView) {
//        this.textureView = textureView;
////        initCamera();
//
//    }
//
//    public void check() {
//        mCountDownTimer.start();
//        isFaceRecognize = true;
//
//
//    }
//    public void closeservice(){
//
//        if(service!=null){
//            service.shutdownNow();
//        }
//        close();
//    }
//    public void check(float score) {
//        this.score = score;
//        this.check();
//
//    }
//    public void  cameraRelese(){
//        CameraUtil.releaseinit();
//    }
//    public void initCamera() {
//        service = Executors.newSingleThreadExecutor();
//        cameraUtil = CameraUtil.getInstance(context, textureView, textureView);
//        cameraUtil.setDefauleCamera(false);
//        cameraUtil.setPreviewFrameCallback(new CameraUtil.OnPreviewFrameCallback() {
//            @Override
//            public void onCameraFront(byte[][] bytes, int orientation) {
//                service.execute(mFaceRecognitionThread);
//            }
//
//            @Override
//            public void onCameraBack(byte[][] bytes, int orientation) {
//                service.execute(mFaceRecognitionThread);
//            }
//
//            @Override
//            public void onCameraFront(byte[] bytes, int orientation) {
//
//            }
//
//            @Override
//            public void onCameraBack(byte[] bytes, int orientation) {
//
//            }
//        });
//        cameraUtil.startPreview(context);
//    }
//
//    public void exit(){
//        isFaceRecognize = false;
//        mCountDownTimer.cancel();
//
//    }
//
//    public void close() {
//        if (mHRFaceHelper != null) {
//            mHRFaceHelper.finish();
//        }
//        if (cameraUtil != null) {
//            cameraUtil.release();
//        }
//
//    }
//
//
//    private class FaceRecognitionThread implements Runnable {
//
//        public FaceRecognitionThread() {
//        }
//
//        @Override
//        public void run() {
//            if (!isFaceRecognize) {
//                return;
//            }
//            Bitmap bitmap = textureView.getBitmap();
//            List<FaceInfo> faceInfos = mHRFaceHelper.findFace(bitmap);
//            if (faceInfos.size() != 0) {
//                final boolean b = mHRFaceHelper.livingThing(bitmap, faceInfos);
//                if (b) {
//                    FaceFeature faceFeature = mHRFaceHelper.getFaceInfo(bitmap, faceInfos);
//                    if (basefaceFeature != null) {
//                        startface(bitmap, faceFeature);
//                    } else {
//                        byte[] featureData = faceFeature.getFeatureData();
//                        String datas = ByteUtil.BytesToString(featureData);
//                        HRFaceInfo hrFaceInfo = new HRFaceInfo();
//                        hrFaceInfo.setData(datas);
//                        File file = FileUtil.facefile(context, bitmap, hrFaceInfo.getName());
//                        hrFaceInfo.setFacefile(file);
//                        exit();
//                        myCallback.onSuccess(hrFaceInfo, MyConstants.SUCCESS);
//                    }
//                }
//            }
//        }
//
//        public void startface(Bitmap bitmap, FaceFeature faceFeature) {
//            float f = mHRFaceHelper.face(faceFeature, basefaceFeature);
//            if (f >= score) {
//                exit();
//                myCallback.onSuccess("验证成功", MyConstants.SUCCESS);
//
//            }
//
//        }
//    }
//}
