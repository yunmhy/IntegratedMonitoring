//package com.example.login_lib.fragment;
//
//
//import android.os.Handler;
//import android.util.Log;
//import android.view.TextureView;
//import android.view.View;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.alibaba.android.arouter.launcher.ARouter;
//import com.arcsoft.face.FaceFeature;
//import com.example.base_lib.MyCallback;
//import com.example.base_lib.MyOkGo.OkGoBuilder;
//import com.example.base_lib.baen.ARConstants;
//import com.example.base_lib.baen.MyConstants;
//import com.example.base_lib.baen.UserInfo;
//import com.example.base_lib.utils.FileUtil;
//import com.example.base_lib.utils.MyViewOutlineProvider;
//import com.example.login_lib.R;
//
//import com.lzy.okgo.model.HttpParams;
//import com.sxygsj.android.common_hr.entity.HRFaceInfo;
//import com.sxygsj.android.common_hr.util.ByteUtil;
//import com.yanzhenjie.permission.Action;
//import com.yanzhenjie.permission.AndPermission;
//import com.yanzhenjie.permission.runtime.Permission;
//
//
//import java.util.List;
//
//import androidx.fragment.app.Fragment;
//
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class VerificationFragment extends LoginBaseFragment {
//    private static final String TAG = "VerificationFragment";
//    private ImageView mImageView2;
//    private TextureView mCheckTextureView;
//    private TextView mMsg;
//    private TextView mMsg2;
//    private Button mBtnFace2;
//    private int type = 0; //0 验证 1上传 2 进入系统
//    private UserInfo userInfo;
//    private FaceFeature faceFeature;
//    private String[] permission = new String[]{Permission.CAMERA, Permission.READ_EXTERNAL_STORAGE,
//            Permission.WRITE_EXTERNAL_STORAGE, Permission.READ_PHONE_STATE};
//    private HRFaceInfo hrFaceInfo;
//
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        check.setMyCallback(new MyCallback<String>() {
//            @Override
//            public void onSuccess(String o, String code) {
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        type = 1;
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                mImageView2.setVisibility(View.VISIBLE);
//                                mCheckTextureView.setVisibility(View.INVISIBLE);
//                                check.close();
//                                mBtnFace2.setEnabled(true);
//                                mMsg.setText("验证成功");
//                                mMsg2.setText("请上传");
//                                mBtnFace2.setText("上传");
//                            }
//                        }, 500);
//
//                    }
//                });
//            }
//
//            @Override
//            public void onError(String e, String code) {
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getActivity().getApplicationContext(), "验证超时,请重新录入", Toast.LENGTH_LONG).show();
//                    }
//                });
//
//                check.close();
//                navController.popBackStack();
//            }
//        });
//
//
//    }
//
//    public void checkPermission() {
//        AndPermission.with(getContext())
//                .runtime()
//                .permission(permission)
//                .onDenied(new Action<List<String>>() {
//                    @Override
//                    public void onAction(List<String> data) {
//                        Toast.makeText(VerificationFragment.this.getContext(), "没有照相机的权限", Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .onGranted(new Action<List<String>>() {
//                    @Override
//                    public void onAction(List<String> data) {
//                        check.initHr();
//                        check.initCamera();
//                    }
//                })
//                .start();
//    }
//
//    public VerificationFragment() {
//
//    }
//
//    private void uploaddata() {
//
//        mMsg.setText("上传中。。。");
//        mMsg2.setText("请稍等");
//        mBtnFace2.setEnabled(false);
//        HttpParams paramsPost = new HttpParams();
////        paramsPost.put("file", userInfo.getHrFaceInfo().getFacefile());
//        paramsPost.put("comename", userInfo.getUsername());
////        paramsPost.put("data", userInfo.getHrFaceInfo().getData());
////        Log.i("人脸数据", userInfo.getHrFaceInfo().getData());
//        OkGoBuilder.getInstance()
//                .Builder(this.mActivity)
//                .baseurl(MyConstants.TestUrl)
//                .inurl("upload")
//                .method(OkGoBuilder.PSOT)
//                .params(paramsPost)
//                .cls(String.class)
//                .callback(new MyCallback<String>() {
//                    @Override
//                    public void onSuccess(String s, String code) {
//                        if (s.equals("ok")) {
////                            FileUtil.deletfile(userInfo.getHrFaceInfo().getFacefile());
//                            type = 2;
//                            mBtnFace2.setEnabled(true);
//
//                            mMsg.setText("上传成功");
//                            mMsg2.setText("进入系统");
//                            mBtnFace2.setText("确定");
//
//                        }
//                    }
//
//                    @Override
//                    public void onError(String e, String code) {
//                        Log.e(TAG, e);
//                        mBtnFace2.setEnabled(true);
//                        mMsg.setBackground(getResources().getDrawable(R.color.textRed));
//                        mMsg2.setBackground(getResources().getDrawable(R.color.textRed));
//                        mMsg.setText("上传失败");
//                        mMsg2.setText("重新上传");
//                    }
//                }).build();
//
//    }
//
//
//
//
//
//    @Override
//    protected void initData() {
//        userInfo = viewModel.getUserinfo();
//        hrFaceInfo = viewModel.getHrFaceInfo();
//        faceFeature = new FaceFeature();
//        faceFeature.setFeatureData(ByteUtil.StringToBytes(hrFaceInfo.getData()));
//        mImageView2.setImageURI(FileUtil.getFileUri(getContext(), hrFaceInfo.getFacefile()));
////        mImageView2.setImageURI(FileUtil.getFileUri(getActivity().getApplicationContext(), userInfo.getHrFaceInfo().getFacefile()));
//        check.setFaceFeature(faceFeature);
//    }
//
//    @Override
//    protected void initView(View view) {
//        viewModel.setLiveData("人脸验证");
//        mImageView2 = (ImageView) view.findViewById(R.id.imageView2);
//        mImageView2.setOutlineProvider(new MyViewOutlineProvider());
//        mImageView2.setClipToOutline(true);
//        mCheckTextureView = (TextureView) view.findViewById(R.id.check_textureView);
//        mCheckTextureView.setOutlineProvider(new MyViewOutlineProvider());
//        mCheckTextureView.setClipToOutline(true);
//        check.setTextureView(mCheckTextureView);
//        mMsg = (TextView) view.findViewById(R.id.msg);
//        mMsg2 = (TextView) view.findViewById(R.id.msg2);
//        mBtnFace2 = (Button) view.findViewById(R.id.btn_face2);
//        mBtnFace2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (type) {
//                    case 0:
//                        checkPermission();
//                        mImageView2.setVisibility(View.INVISIBLE);
////                        mCheckTextureView.setVisibility(View.VISIBLE);
//                        mMsg.setText("验证中。。。");
//                        mMsg2.setText("请稍后");
//                        mBtnFace2.setEnabled(false);
//                        check.check(0.8f);
//                        break;
//                    case 1:
//                        uploaddata();
//                        break;
//                    case 2:
//                        ARouter.getInstance().build(ARConstants.APP_MAIN)
//                                .withSerializable("hrFaceInfo", hrFaceInfo)
//                                .navigation();
//                        getActivity().finish();
//                        break;
//                    default:
//                        break;
//                }
//            }
//        });
//    }
//
//    @Override
//    protected int contentLayout() {
//        return R.layout.fragment_check;
//    }
//
//    @Override
//    public void onPause() {
//        check.exit();
//        super.onPause();
//    }
//}
