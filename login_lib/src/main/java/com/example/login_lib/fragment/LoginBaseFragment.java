//package com.example.login_lib.fragment;
//
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Toast;
//
//import com.example.base_lib.MyCallback;
//import com.example.base_lib.fragment.BaseFragment;
////import com.example.login_lib.common.FaceVerification;
////import com.example.login_lib.common.FaceViewModel;
//import com.tencent.mmkv.MMKV;
//
//import androidx.lifecycle.ViewModelProviders;
//import androidx.navigation.NavController;
//import androidx.navigation.fragment.NavHostFragment;
//
//public abstract class LoginBaseFragment extends BaseFragment {
//    protected MMKV mmkv;
//    protected FaceViewModel viewModel;
//    protected NavController navController ;
//    protected FaceVerification check;
//    @Override
//    protected void initBeforeCreateView(Bundle savedInstanceState) {
//        mmkv= MMKV.defaultMMKV();
//        viewModel= ViewModelProviders.of(getActivity()).get(FaceViewModel.class);
//        navController= NavHostFragment.findNavController(this);
//        check =  new FaceVerification(this.getContext());
//        check.cameraRelese();
//        super.initBeforeCreateView(savedInstanceState);
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        check.closeservice();
//    }
//}
