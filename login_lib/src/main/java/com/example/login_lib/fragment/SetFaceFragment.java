//package com.example.login_lib.fragment;
//
//
//import android.os.Bundle;
//import android.os.Handler;
//import android.view.LayoutInflater;
//import android.view.TextureView;
//import android.view.View;
//import android.view.ViewGroup;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.view.animation.LinearInterpolator;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.example.base_lib.MyCallback;
//import com.example.base_lib.baen.UserInfo;
//import com.example.base_lib.utils.MyViewOutlineProvider;
//import com.example.login_lib.R;
//
//
//import com.sxygsj.android.common_hr.entity.HRFaceInfo;
//import com.yanzhenjie.permission.Action;
//import com.yanzhenjie.permission.AndPermission;
//import com.yanzhenjie.permission.runtime.Permission;
//
//import java.util.List;
//
//import androidx.annotation.NonNull;
//import androidx.annotation.Nullable;
//import androidx.fragment.app.Fragment;
//import androidx.navigation.Navigation;
//
///**
// * A simple {@link Fragment} subclass.
// */
//public class SetFaceFragment extends LoginBaseFragment{
//    private boolean isRun = false;
//    private TextureView mTextureView;
//    private TextView cr_txt, msg_txt;
//    private ImageView cream_btn;
//    private String[] permission = new String[]{Permission.CAMERA, Permission.READ_EXTERNAL_STORAGE,
//            Permission.WRITE_EXTERNAL_STORAGE, Permission.READ_PHONE_STATE};
//    private UserInfo userInfo;
//    private String TAG = "SetFaceFragment";
//    private ImageView faceLoginImg1;
//    private ImageView faceLoginImg2;
//    public SetFaceFragment() {
//        // Required empty public constructor
//    }
//
//
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view =super.onCreateView(inflater, container, savedInstanceState);
//        checkPermission();
//        check.setMyCallback(new MyCallback<HRFaceInfo>() {
//            @Override
//            public void onSuccess(HRFaceInfo hrFaceInfo, String code) {
//                viewModel.setHrFaceInfo(hrFaceInfo);
////                viewModel.setUserinfo(userInfo);
//                check.close();
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Navigation.findNavController(SetFaceFragment.this.getView()).navigate(R.id.action_setFaceFragment_to_checkFragment);
//                        cream_btn.setEnabled(true);
//                    }
//                });
//
//            }
//
//            @Override
//            public void onError(String e, String code) {
//                getActivity().runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(getActivity().getApplicationContext(), "未找到人脸，请重新拍摄", Toast.LENGTH_LONG).show();
//                        msg_txt.setText("请将人脸置入采集框内");
//                        cream_btn.setEnabled(true);
//                    }
//                });
//
//            }
//        });
//        return view;
//    }
//
//    @Override
//    protected void initData() {
//        userInfo=viewModel.getUserinfo();
//    }
//
//    @Override
//    protected void initView(View view) {
//        viewModel.setLiveData("人脸采集" );
//        cr_txt = view.findViewById(R.id.cream_txt);
//        msg_txt = view.findViewById(R.id.txt_msg);
//        cream_btn = view.findViewById(R.id.cream_btn);
//        mTextureView = (TextureView) view.findViewById(R.id.mTextureView);
//        mTextureView.setOutlineProvider(new MyViewOutlineProvider());
//        mTextureView.setClipToOutline(true);
//        check.setTextureView(mTextureView);
//        cream_btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                cream_btn.setEnabled(false);
//                msg_txt.setText("采集中。。。请保持人脸不移动");
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        check.check();
//
//                    }
//                },1000);
//
//            }
//        });
//        faceLoginImg1 = (ImageView) view.findViewById(R.id.face_login_img_1);
//        faceLoginImg2 = (ImageView)  view.findViewById(R.id.face_login_img_2);
//
//        Animation operatingAnim = AnimationUtils.loadAnimation(getContext(), R.anim.da);
//        LinearInterpolator lir = new LinearInterpolator();
//        operatingAnim.setInterpolator(lir);
//        faceLoginImg1.setAnimation(operatingAnim);
//
//        Animation operatingAnim1 = AnimationUtils.loadAnimation(getContext(), R.anim.xiao);
//        LinearInterpolator lir1 = new LinearInterpolator();
//        operatingAnim1.setInterpolator(lir1);
//        faceLoginImg2.setAnimation(operatingAnim1);
//
//    }
//
//    public void checkPermission() {
//        AndPermission.with(this)
//                .runtime()
//                .permission(permission)
//                .onDenied(new Action<List<String>>() {
//                    @Override
//                    public void onAction(List<String> data) {
//                        Toast.makeText(SetFaceFragment.this.getContext(), "没有照相机的权限", Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .onGranted(new Action<List<String>>() {
//                    @Override
//                    public void onAction(List<String> data) {
//                        check.initHr();
//                        check.initCamera();
//
//                    }
//                })
//                .start();
//    }
//
//    @Override
//    protected int contentLayout() {
//        return R.layout.fragment_set_face;
//    }
//
//    @Override
//    public void onPause() {
//        check.exit();
//        super.onPause();
//    }
//
//
//}
