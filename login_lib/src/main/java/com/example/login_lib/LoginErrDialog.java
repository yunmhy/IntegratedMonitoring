package com.example.login_lib;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.base_lib.fragment.BaseDialogFragment;

public class LoginErrDialog extends BaseDialogFragment {

    private TextView errMsg;
    private TextView errSure;
    private String msg;
    public LoginErrDialog(String msg) {
        this.msg=msg;
    }

    @Override
    protected void initData() {
        errMsg.setText(msg);
    }

    @Override
    protected void initView(View view) {
        errMsg = (TextView) view.findViewById(R.id.err_msg);
        errSure = (TextView) view.findViewById(R.id.err_sure);
        errSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }

    @Override
    protected int getContentLayout() {
        return R.layout.dialog_loginerr;
    }
}
