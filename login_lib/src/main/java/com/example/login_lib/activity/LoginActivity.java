package com.example.login_lib.activity;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.ImageView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.MutableLiveData;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.alibaba.android.arouter.launcher.ARouter;
import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.activity.BaseActivity;

import com.example.base_lib.baen.ARConstants;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.utils.DeviceInfoUtils;
import com.example.login_lib.LoginErrDialog;
import com.example.login_lib.R;


import com.google.gson.Gson;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;
//import com.sxygsj.android.common_hr.entity.HRFaceInfo;

import java.io.UnsupportedEncodingException;

// 在支持路由的页面上添加注解(必选)
// 这里的路径需要注意的是至少需要有两级，/xx/xx
@Route(path = ARConstants.APP_Login)
public class LoginActivity extends BaseActivity implements View.OnFocusChangeListener {
    private static final String TAG = "LoginActivity";
    private EditText mEditUsername;
    private EditText mEditPwd;
    private Button mBtnLogin;
    private String username;
    private String pwd;
    private ImageView txtUserImg;
    private ImageView userLine;
    private ImageView txtPassImg;
    private ImageView passLine;
    private ImageView clearUsername;
    private ImageView showPassword;
    public MutableLiveData<String> testid = new MutableLiveData<>();
    private Boolean showP = true;

    @Override
    protected void initView() {
        getToolbar().setVisibility(View.GONE);
        mEditUsername = (EditText) findViewById(R.id.edit_username);
        mEditPwd = (EditText) findViewById(R.id.edit_pwd);
        mEditUsername.setOnFocusChangeListener(this);
        mEditPwd.setOnFocusChangeListener(this);
        mBtnLogin = (Button) findViewById(R.id.btn_login);
        txtUserImg = (ImageView) findViewById(R.id.txt_user_img);
        userLine = (ImageView) findViewById(R.id.user_line);
        txtPassImg = (ImageView) findViewById(R.id.txt_pass_img);
        passLine = (ImageView) findViewById(R.id.pass_line);
        mEditUsername.setText(mmkv.decodeString("username"));
        mEditPwd.setText(mmkv.decodeString("password"));
        clearUsername = (ImageView) findViewById(R.id.clear_username);
        clearUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditUsername.setText(null);
            }
        });
        showPassword = (ImageView) findViewById(R.id.show_password);
        showPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (showP) {// 显示密码
                    showPassword.setImageDrawable(getResources().getDrawable(R.drawable.show_password));
                    mEditPwd.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    mEditPwd.setSelection(mEditPwd.getText().toString().length());
                    showP = !showP;
                } else {// 隐藏密码
                    showPassword.setImageDrawable(getResources().getDrawable(R.drawable.hide_password));
                    mEditPwd.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    mEditPwd.setSelection(mEditPwd.getText().toString().length());
                    showP = !showP;
                }

            }
        });
        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                username = mEditUsername.getText().toString().trim();//填好了才有值;
                pwd = mEditPwd.getText().toString().trim();
                // Log.e("user+pwd", "onClick: "+"username:"+username+" pwd:"+pwd);
                if (username.length() > 0 && pwd.length() > 0) {
//                    UserInfo userInfo=new UserInfo();
//                    userInfo.setUsername(username);
//                                            ARouter.getInstance().build(ARConstants.APP_MAIN)
//                                .withSerializable("userinfo",userInfo)
//                                .navigation();
                    if(username.equals("sxygsj123")&&pwd.equals("999999")){
                        Intent intent =new Intent(LoginActivity.this,SetActivity.class);
                        startActivity(intent);
                    }else {
                        getLogin(username, pwd);//登录
                    }

                } else {
                    Toast.makeText(LoginActivity.this, "请填写用户名或密码", Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    public void isButtonEnable() {
        if (mEditUsername.length() > 0 && mEditPwd.length() > 0) {
            mBtnLogin.setEnabled(true);
        } else {
            mBtnLogin.setEnabled(false);
        }
    }

    @Override
    protected void initData() {
        isButtonEnable();
        if (mEditUsername.length() > 0) {
            uichange(txtUserImg, R.drawable.edit_user_1, userLine, R.color.white, clearUsername, View.VISIBLE);
        } else {
            uichange(txtUserImg, R.drawable.edit_user, userLine, R.color.edit_line, clearUsername, View.INVISIBLE);
        }

        if (mEditPwd.length() > 0) {
            uichange(txtPassImg, R.drawable.edit_password_1, passLine, R.color.white, showPassword, View.VISIBLE);
        } else {

            uichange(txtPassImg, R.drawable.edit_password, passLine, R.color.edit_line, showPassword, View.INVISIBLE);
        }
        mEditUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                txtUserImg.setImageResource(R.drawable.edit_user_1);
                userLine.setBackgroundColor(getResources().getColor(R.color.white));
                if (count > 0) {
                    clearUsername.setVisibility(View.VISIBLE);
                } else {
                    clearUsername.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    uichange(txtUserImg, R.drawable.edit_user_1, userLine, R.color.white, clearUsername, View.VISIBLE);
                } else {
                    uichange(txtUserImg, R.drawable.edit_user, userLine, R.color.edit_line, clearUsername, View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                isButtonEnable();
                if (s.length() > 0) {
                    clearUsername.setVisibility(View.VISIBLE);
                    txtUserImg.setImageResource(R.drawable.edit_user_1);
                    userLine.setBackgroundColor(getResources().getColor(R.color.white));
                } else {
                    uichange(txtUserImg, R.drawable.edit_user, userLine, R.color.edit_line, clearUsername, View.INVISIBLE);
                }
            }
        });
        mEditPwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                txtPassImg.setImageResource(R.drawable.edit_password_1);
                passLine.setBackgroundColor(getResources().getColor(R.color.white));
                if (count > 0) {
                    showPassword.setVisibility(View.VISIBLE);
                } else {
                    showPassword.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count > 0) {
                    uichange(txtPassImg, R.drawable.edit_password_1, passLine, R.color.white, showPassword, View.VISIBLE);
                } else {
                    uichange(txtPassImg, R.drawable.edit_password, passLine, R.color.edit_line, showPassword, View.INVISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                isButtonEnable();
                if (s.length() > 0) {
                    uichange(txtPassImg, R.drawable.edit_password_1, passLine, R.color.white, showPassword, View.VISIBLE);
                } else {
                    uichange(txtPassImg, R.drawable.edit_password, passLine, R.color.edit_line, showPassword, View.INVISIBLE);
                }
            }
        });
    }

    private void uichange(ImageView txtPassImg, int p, ImageView passLine, int p2, ImageView showPassword, int invisible) {
        txtPassImg.setImageResource(p);
        passLine.setBackgroundColor(getResources().getColor(p2));
        showPassword.setVisibility(invisible);
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_login;
    }


    private void getLogin(final String username, final String pwd) {
        if (DeviceInfoUtils.getSIM(this) == null) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
            return;
        } else {
            try {
                byte[] textByte = new Gson().toJson(DeviceInfoUtils.getSIM(this)).getBytes("UTF-8");
                String unique = Base64.encodeToString(textByte, Base64.DEFAULT);
                Log.i("unique",unique);
                HttpHeaders headers = new HttpHeaders();
                HttpParams paramsGet = new HttpParams();
                paramsGet.put("uniqueid", unique);
                paramsGet.put("comename", username);
                paramsGet.put("password", pwd);
                OkGoBuilder.getInstance()
                        .Builder(this)
                        .baseurl(MyConstants.TestUrl)
                        .inurl("login")
                        .method(OkGoBuilder.GET)
                        .dialogshow(true)
                        .params(paramsGet)
//                        .headers(headers)
                        .cls(UserInfo.class)
                        .callback(new MyCallback<UserInfo>() {
                            @Override
                            public void onSuccess(UserInfo userInfo, String id) {
                                baseuserInfo = userInfo;
                                Log.i(TAG, "onSuccess1: " + userInfo.getUsername());
                                mmkv.encode("username", username);
                                mmkv.encode("password", pwd);
                                ARouter.getInstance().build(ARConstants.APP_MAIN)
//                                .withSerializable("userinfo",userInfo)
                                        .navigation();
//                        getFaceData(userInfo);
                            }

                            @Override
                            public void onError(String e, String id) {
                                new LoginErrDialog(e).show(getSupportFragmentManager(), "err_dialog");
                                Log.i(TAG, "onError: " + e);
                                Toast.makeText(LoginActivity.this, e, Toast.LENGTH_LONG).show();
                            }
                        })
                        .build();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

        }


    }

//    private void getFaceData(final UserInfo user) {
//        HttpParams paramsGet = new HttpParams();
//        paramsGet.put("name", user.getUsername());
//        OkGoBuilder.getInstance()
//                .Builder(this)
//                .baseurl(MyConstants.BASERURL)
//                .inurl("facedata")
//                .method(OkGoBuilder.GET)
//                .params(paramsGet)
//                .cls(HRFaceInfo.class)
//                .callback(new MyCallback<HRFaceInfo>() {
//                    @Override
//                    public void onSuccess(HRFaceInfo faceInfo, String id) {
//                        Log.i(TAG, "onSuccess1: " + faceInfo.getName());
//                        if (faceInfo != null) {
//                            Intent intent;
//                            if (TextUtils.isEmpty(faceInfo.getData()) || TextUtils.isEmpty(faceInfo.getUrl())) {
//                                intent = new Intent(LoginActivity.this, UploadFaceActivity.class);
//                            } else {
//                                intent = new Intent(LoginActivity.this, FaceLoginActivity.class);
////                                finish();
//                            }
////                            user.setHrFaceInfo(faceInfo);
//                            intent.putExtra("HRFaceInfo", faceInfo);
//                            startActivity(intent);
//
//                        }
//                    }
//
//                    @Override
//                    public void onError(String e, String id) {
//                        Log.i(TAG, "onSuccess1: " + e);
//                    }
//                })
//                .build();
//    }

    @Override
    public void onBackPressed() {
        onDestroy();
    }

    public boolean isEnable(String username, String password) {
        if (username.equals("") || password.equals("")) {
            return true;
        } else return false;

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            if (v.getId() == R.id.edit_username) {
                txtUserImg.setImageResource(R.drawable.edit_user_1);
                userLine.setBackgroundColor(getResources().getColor(R.color.white));
            }
            if (v.getId() == R.id.edit_pwd) {
                txtPassImg.setImageResource(R.drawable.edit_password_1);
                passLine.setBackgroundColor(getResources().getColor(R.color.white));
            }
        } else {
            if (v.getId() == R.id.edit_username) {
                if (mEditUsername.length() == 0) {
                    txtUserImg.setImageResource(R.drawable.edit_user);
                    userLine.setBackgroundColor(getResources().getColor(R.color.edit_line));
                }

            }
            if (v.getId() == R.id.edit_pwd) {
                if (mEditPwd.length() == 0) {
                    txtPassImg.setImageResource(R.drawable.edit_password);
                    passLine.setBackgroundColor(getResources().getColor(R.color.edit_line));
                }

            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 0:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    getLogin(username, pwd);
                } else {
                    new LoginErrDialog("需要开启相应权限").show(getSupportFragmentManager(), "err_dialog");
                }
                break;
            default:
                break;
        }
    }
}
