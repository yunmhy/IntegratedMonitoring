//package com.example.login_lib.activity;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.os.Handler;
//import android.view.TextureView;
//import android.view.View;
//import android.view.animation.Animation;
//import android.view.animation.AnimationUtils;
//import android.view.animation.LinearInterpolator;
//import android.widget.ImageView;
//import android.widget.TextView;
//import android.widget.Toast;
//
//import com.alibaba.android.arouter.launcher.ARouter;
//import com.arcsoft.face.FaceFeature;
//import com.example.base_lib.MyCallback;
//import com.example.base_lib.activity.BaseActivity;
//import com.example.base_lib.baen.ARConstants;
//import com.example.base_lib.utils.MyViewOutlineProvider;
//import com.example.login_lib.R;
//
//import com.example.login_lib.common.FaceVerification;
//import com.sxygsj.android.common_hr.entity.HRFaceInfo;
//import com.sxygsj.android.common_hr.util.ByteUtil;
//import com.yanzhenjie.permission.Action;
//import com.yanzhenjie.permission.AndPermission;
//import com.yanzhenjie.permission.runtime.Permission;
//
//import java.util.List;
//
//public class FaceLoginActivity extends BaseActivity {
//    private ImageView faceLoginimg;
//    private TextureView faceLoginTrv;
//    private TextView faceLoginMsg;
//    private HRFaceInfo hrFaceInfo;
//    private FaceFeature basefaceFeature;
////    private UserInfo userInfo;
//    private String[] permission = new String[]{Permission.CAMERA, Permission.READ_EXTERNAL_STORAGE,
//            Permission.WRITE_EXTERNAL_STORAGE, Permission.READ_PHONE_STATE};
//    private FaceVerification check;
//    private ImageView faceLoginImg1;
//    private ImageView faceLoginImg2;
//
//
//
//    @Override
//    protected void initBeforeCreateView(Bundle savedInstanceState) {
//        super.initBeforeCreateView(savedInstanceState);
//        check = new FaceVerification(this);
//        check.setMyCallback( new MyCallback<String>() {
//            @Override
//            public void onSuccess(final String s, String code) {
//                //Todo 将来改成RXJAVA
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        faceLoginMsg.setText(s);
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                // 路由跳转并携带参数
//                                ARouter.getInstance().build(ARConstants.APP_MAIN)
//                                        .withSerializable("hrFaceInfo",hrFaceInfo)
//                                        .navigation();
//                                finish();
//                            }
//                        }, 2000);
//                    }
//                });
//            }
//            @Override
//            public void onError(final String e, String code) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        faceLoginMsg.setText(e);
//                        faceLoginMsg.setTextColor(getResources().getColor(R.color.textRed));
//                        Toast.makeText(FaceLoginActivity.this.getApplicationContext(), "验证超时,请重新登录", Toast.LENGTH_SHORT).show();
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                finish();
//                            }
//                        }, 2000);
//                    }
//                });
//
//            }
//        });
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        checkPermission();
//
//    }
//
//    private void checkPermission() {
//        AndPermission.with(this)
//                .runtime()
//                .permission(permission)
//                .onDenied(new Action<List<String>>() {
//                    @Override
//                    public void onAction(List<String> data) {
//                        Toast.makeText(FaceLoginActivity.this, "没有照相机的权限", Toast.LENGTH_SHORT).show();
//                    }
//                })
//                .onGranted(new Action<List<String>>() {
//                    @Override
//                    public void onAction(List<String> data) {
//                        check.initCamera();
//                        check.initHr();
//                        check.check(0.8f);
//
//                    }
//                })
//                .start();
//    }
//
//    @Override
//    protected void initView() {
//        getToolbar().setVisibility(View.GONE);
//        faceLoginimg = (ImageView) findViewById(R.id.face_login_img);
////        faceLoginimg.setOutlineProvider(new MyViewOutlineProvider());
////        faceLoginimg.setClipToOutline(true);
//        faceLoginTrv = (TextureView) findViewById(R.id.face_login_trv);
//        faceLoginTrv.setOutlineProvider(new MyViewOutlineProvider());
//        faceLoginTrv.setClipToOutline(true);
//        faceLoginMsg = (TextView) findViewById(R.id.face_login_msg);
//        check.setTextureView(faceLoginTrv);
//        faceLoginImg1 = (ImageView) findViewById(R.id.face_login_img_1);
//        faceLoginImg2 = (ImageView) findViewById(R.id.face_login_img_2);
//
//        Animation operatingAnim = AnimationUtils.loadAnimation(this, R.anim.da);
//        LinearInterpolator lir = new LinearInterpolator();
//        operatingAnim.setInterpolator(lir);
//        faceLoginImg1.setAnimation(operatingAnim);
//
//        Animation operatingAnim1 = AnimationUtils.loadAnimation(this, R.anim.xiao);
//        LinearInterpolator lir1 = new LinearInterpolator();
//        operatingAnim1.setInterpolator(lir1);
//        faceLoginImg2.setAnimation(operatingAnim1);
//    }
//
//    @Override
//    protected void initData() {
//        Intent intent =getIntent();
//        hrFaceInfo = (HRFaceInfo) intent.getSerializableExtra("HRFaceInfo");
////        hrFaceInfo = userInfo.getHrFaceInfo();
//        basefaceFeature = new FaceFeature();
////        basefaceFeature.setFeatureData(ByteUtil.StringToBytes(hrFaceInfo.getData()));
////        check.setFaceFeature(basefaceFeature);
//    }
//
//    @Override
//    protected int layoutResId() {
//        return R.layout.activity_face_login;
//    }
//
//
//    @Override
//    protected void onStop() {
//        check.close();
//        super.onStop();
//    }
//
//}
