package com.example.login_lib.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.base_lib.activity.BaseActivity;
import com.example.base_lib.baen.MyConstants;
import com.example.login_lib.R;

public class SetActivity extends BaseActivity {
    private EditText setIp;
    private Button setSure;

    @Override
    protected void initView() {
        setBarColor(true);
        getToolbarTitle().setText("系统配置");
        setTbM(false,R.id.menu_menu);
        setIp = (EditText) findViewById(R.id.set_ip);
        setSure = (Button) findViewById(R.id.set_sure);
        setIp.setText(MyConstants.TestUrl);
        setSure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyConstants.TestUrl=setIp.getText().toString();
                Toast.makeText(getApplicationContext(),"ip为："+MyConstants.TestUrl,Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_set;
    }
}
