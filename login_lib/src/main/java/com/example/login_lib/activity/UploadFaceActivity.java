//package com.example.login_lib.activity;
//
//import android.content.Intent;
//import android.graphics.Color;
//import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.widget.TextView;
//
//import com.example.base_lib.activity.BaseActivity;
//import com.example.base_lib.baen.UserInfo;
//import com.example.login_lib.R;
//
//import com.example.login_lib.common.FaceViewModel;
//import com.sxygsj.android.common_hr.entity.HRFaceInfo;
//
//import androidx.appcompat.widget.Toolbar;
//import androidx.lifecycle.Observer;
//import androidx.lifecycle.ViewModelProviders;
//
//
//public class UploadFaceActivity extends BaseActivity {
//    protected FaceViewModel viewModel;
//
//
//    @Override
//    protected void initBeforeCreateView(Bundle savedInstanceState) {
//        viewModel= ViewModelProviders.of(this).get(FaceViewModel.class);
//        super.initBeforeCreateView(savedInstanceState);
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        super.onCreateOptionsMenu(menu);
//        MenuItem menuItem = menu.findItem(R.id.menu_menu);
//        menuItem.setVisible(false);
//        return true;
//    }
//
//    @Override
//    protected void initView() {
//        setTbM(false,R.id.menu_menu);
//        getToolbar().setBackground(getResources().getDrawable(R.color.blue));
//        viewModel.getLiveData().observe(this, new Observer<String>() {
//            @Override
//            public void onChanged(String s) {
//                getToolbarTitle().setText(s);
//            }
//        });
//    }
//
//    @Override
//    protected void initData() {
//        Intent intent =getIntent();
//        HRFaceInfo hrFaceInfo=(HRFaceInfo) intent.getSerializableExtra("HRFaceInfo");
//        viewModel.setUserinfo(baseuserInfo);
//    }
//
//    @Override
//    protected int layoutResId() {
//        return R.layout.activity_upload_face;
//    }
//
//
//}
