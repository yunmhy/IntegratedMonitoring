package com.example.rydw_lib;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.annotation.SuppressLint;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.example.base_lib.activity.BaseActivity;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.ARConstants;
import com.example.base_lib.baen.UserInfo;
import com.google.android.material.bottomnavigation.BottomNavigationView;

// 在支持路由的页面上添加注解(必选)
// 这里的路径需要注意的是至少需要有两级，/xx/xx
@Route(path = ARConstants.APP_RYDW)
public class RydwMainActivity extends BaseActivity {
    private BottomNavigationView navView;
    private NavController navController;
    //    @Autowired(name = "userinfo")
//    UserInfo userInfo;

    @SuppressLint("ResourceType")
    @Override
    protected void initView() {
//        findViewById(R.id.nav_host_fragment_ry).setBackground(new WaterMarkBg(baseuserInfo.getUseridname()));
        getToolbar().setBackground(getDrawable(R.color.blue));
        getToolbarTitle().setTextColor(getResources().getColor(R.color.white));
        setTbM(false, R.id.menu_rydw);
//        setBarColor(true);
        navView = findViewById(R.id.nav_view);
        navView.setItemTextColor(getResources().getColorStateList(R.drawable.nav_menu_text_color));
        navController = Navigation.findNavController(this, R.id.nav_host_fragment_ry);
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        NavigationUI.setupWithNavController(navView, navController);

        initIcon();
        navView.setItemIconTintList(null);
        getToolbar().setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Navigation.findNavController(view).popBackStack();
                onBackPressed();
            }
        });


    }

    @Override
    protected void initData() {
//        Intent intent=getIntent();
//        baseuserInfo = (UserInfo)intent.getSerializableExtra("userinfo");
    }

    @Override
    protected int layoutResId() {
        return R.layout.activity_rydw_main;
    }

    public UserInfo toValue() {
        return baseuserInfo;
    }

    @Override
    protected void onResume() {
        getToolbarTitle().setText("人员定位");
        super.onResume();
    }

    public void initIcon() {
        resetToDefaultIcon();//重置到默认不选中图片
        Log.i("select", navView.getSelectedItemId() + "");
        NavDestination navDestination = navController.getCurrentDestination(); //获取当前目的地的信息
        int selectedItemId = navDestination.getId();
        if (selectedItemId == R.id.ry_navigation_main) {//在这里替换图标
            navView.getMenu().findItem(selectedItemId).setIcon(R.mipmap.list2);
        }
        if (selectedItemId == R.id.ry_navigation_alarm) {//在这里替换图标
            navView.getMenu().findItem(selectedItemId).setIcon(R.mipmap.safealarm2);

        }
        if (selectedItemId == R.id.ry_navigation_history) {//在这里替换图标
            navView.getMenu().findItem(selectedItemId).setIcon(R.mipmap.history2);


        }
        if (selectedItemId == R.id.ry_navigation_list) {//在这里替换图标
            navView.getMenu().findItem(selectedItemId).setIcon(R.mipmap.ry_person_1);


        }
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            resetToDefaultIcon();//重置到默认不选中图片
            NavigationUI.onNavDestinationSelected(item, navController);
            if (item.getItemId() == R.id.ry_navigation_main) {//在这里替换图标
                item.setIcon(R.mipmap.list2);
                return true;
            }
            if (item.getItemId() == R.id.ry_navigation_history) {//在这里替换图标
                item.setIcon(R.mipmap.history2);
                return true;
            }
            if (item.getItemId() == R.id.ry_navigation_alarm) {//在这里替换图标
                item.setIcon(R.mipmap.safealarm2);
                return true;
            }
            if (item.getItemId() == R.id.ry_navigation_list) {//在这里替换图标
                item.setIcon(R.mipmap.ry_person_1);
                return true;
            }

            return false;
        }

    };

    private void resetToDefaultIcon() {
        MenuItem main = navView.getMenu().findItem(R.id.ry_navigation_main);
        main.setIcon(R.mipmap.list1);
        MenuItem list = navView.getMenu().findItem(R.id.ry_navigation_list);
        list.setIcon(R.mipmap.ry_person);
        MenuItem history = navView.getMenu().findItem(R.id.ry_navigation_history);
        history.setIcon(R.mipmap.history1);
        MenuItem alarm = navView.getMenu().findItem(R.id.ry_navigation_alarm);
        alarm.setIcon(R.mipmap.safealarm1);
    }

    @Override
    public void onBackPressed() {
        if (navView.getSelectedItemId() == R.id.ry_navigation_main) {
            super.onBackPressed();
        } else {
            navView.setSelectedItemId(R.id.ry_navigation_main);

        }
    }
}
