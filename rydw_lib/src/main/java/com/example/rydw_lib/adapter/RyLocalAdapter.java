package com.example.rydw_lib.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.rydw_lib.R;
import com.example.rydw_lib.bean.RydwListBean;

import java.util.List;

import androidx.annotation.Nullable;

public class RyLocalAdapter extends BaseQuickAdapter <RydwListBean.DataBean.Locus, BaseViewHolder>{


    public RyLocalAdapter(int layoutResId, @Nullable List<RydwListBean.DataBean.Locus> data) {
        super(layoutResId, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, RydwListBean.DataBean.Locus item) {
        String[] split = item.getLoculist().split("&");
        helper.setText(R.id.local_text,split[0]).setText(R.id.time_text,split[1]);
    }


}
