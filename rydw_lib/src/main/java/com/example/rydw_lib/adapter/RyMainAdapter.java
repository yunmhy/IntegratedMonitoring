package com.example.rydw_lib.adapter;





import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;


import com.example.rydw_lib.R;
import com.example.rydw_lib.bean.MyRyMainData;
import com.example.rydw_lib.bean.RydwMainBean;



public class RyMainAdapter extends BaseQuickAdapter<MyRyMainData,BaseViewHolder>  {


    public RyMainAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, MyRyMainData item) {
//        helper.setText(R.id.text_ryalarm_nick,item.getMinename()).setText(R.id.text_ryalarm_nickcode,item.getMinecode());
        RydwMainBean.DataBean bean = item.getMainBeanList();
        helper.addOnClickListener(R.id.overminenum_ly).addOnClickListener(R.id.overtimenum_ly)
                .addOnClickListener(R.id.realtimenum_ly).addOnClickListener(R.id.callnum_ly)
                .addOnClickListener(R.id.leadernum_ly).addOnClickListener(R.id.specialalarmnum_ly)
                .addOnClickListener(R.id.specianum_ly).addOnClickListener(R.id.sysalarmnum_ly);
        helper.setText(R.id.realtimenum,bean.getRealtimenum()+"")
                .setText(R.id.leadernum,bean.getLeadernum()+"")
                .setText(R.id.specianum,bean.getSpecianum()+"")
                .setText(R.id.specialalarmnum,bean.getSpecialalarmnum()+"")
                .setText(R.id.sysalarmnum,bean.getSysalarmnum()+"")
                .setText(R.id.callnum,bean.getCallnum()+"")
                .setText(R.id.overtimenum,bean.getOvertimenum()+"")
                .setText(R.id.overminenum,bean.getOverminenum()+"");

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerView recyclerView = (RecyclerView) helper.getView(R.id.region_rcy);
        recyclerView.setLayoutManager(layoutManager);
        RegionAdapter moreAdaptr=new RegionAdapter(R.layout.rybyregion_item,item.getRegionDataList());
        recyclerView.setAdapter(moreAdaptr);

    }


}
