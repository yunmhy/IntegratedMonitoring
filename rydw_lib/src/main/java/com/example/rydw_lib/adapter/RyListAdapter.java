package com.example.rydw_lib.adapter;

import android.opengl.Visibility;
import android.view.View;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.example.rydw_lib.R;
import com.example.rydw_lib.bean.RydwListBean;

import java.util.List;

public class RyListAdapter extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {


    /**
     * Same as QuickAdapter#QuickAdapter(Context,int) but with
     * some initialization data.
     *
     * @param data A new list is created out of this one to avoid mutable list
     */
    public RyListAdapter(List<MultiItemEntity> data) {
        super(data);
        addItemType(0, R.layout.rylist_item);
        addItemType(1, R.layout.local_item);
    }

    @Override
    protected void convert(final BaseViewHolder helper, MultiItemEntity multiItemEntity) {

        switch (helper.getItemViewType()) {
            case 0:
                final RydwListBean.DataBean item = (RydwListBean.DataBean) multiItemEntity;
                if (item.isExpanded()) {
                    helper.setText(R.id.show_more,"点击收起");
                    helper.setGone(R.id.ly_more, true);
                } else {
                    helper.setText(R.id.show_more,"点击查看详细信息");
                    helper.setGone(R.id.ly_more, false);
                }
                helper.setText(R.id.ry_name, item.getPersonName()).setText(R.id.ry_state, item.getInOutMineFlag())
                        .setText(R.id.ry_inMineTime, item.getInMineTime()).setText(R.id.ry_outMineTime, item.getOutMineTime())
                        .setText(R.id.ry_areaName, item.getAreaName()).setText(R.id.ry_stationName, item.getStationName());
                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = helper.getAdapterPosition();
                        if (item.isExpanded()) {
                            collapse(pos, false);
                            helper.setText(R.id.show_more,"点击查看详细信息");
                            helper.setGone(R.id.ly_more, false);
                        } else {
                            expand(pos, false);
                            helper.setText(R.id.show_more,"点击收起");
                            helper.setGone(R.id.ly_more, true);
//                            Toast.makeText(mContext, "展开：" + item1.getTitle(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                break;
            case 1:
                final RydwListBean.DataBean.Locus item1 = (RydwListBean.DataBean.Locus) multiItemEntity;
                String[] split = item1.getLoculist().split("&");
                helper.setText(R.id.local_text, split[0]).setText(R.id.time_text, split[1]);
                break;
        }
    }


}
