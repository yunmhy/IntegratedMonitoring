package com.example.rydw_lib.adapter;

import android.content.Context;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.rydw_lib.R;
import com.example.rydw_lib.bean.RegionData;

import java.util.List;

public class RegionAdapter extends BaseQuickAdapter {


    public RegionAdapter(int layoutResId, @Nullable List data) {
        super(layoutResId, data);
    }
    public RegionAdapter(int layoutResId) {
        super(layoutResId);
    }
    @Override
    protected void convert(BaseViewHolder helper, Object item) {
        List<RegionData.DataBean> list = (List<RegionData.DataBean>) item;
        if (list.size()<=0)return;
        helper.setText(R.id.regin_type,list.get(0).getRegiontype());
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        RecyclerView recyclerView = (RecyclerView) helper.getView(R.id.regmore_rcy);
        recyclerView.setLayoutManager(layoutManager);
        RegionMoreAdaptr moreAdaptr=new RegionMoreAdaptr(R.layout.regionmore_item,list);

        recyclerView.setAdapter(moreAdaptr);
    }
    class RegionMoreAdaptr extends BaseQuickAdapter<RegionData.DataBean,BaseViewHolder>{


        public RegionMoreAdaptr(int layoutResId, @Nullable List<RegionData.DataBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, RegionData.DataBean item) {
            helper.setText(R.id.region_name,item.getRegionname()).setText(R.id.region_count,item.getRegioncheckpersonnum()+"")
                    .setText(R.id.region_realy,item.getRegioncount()+"");
        }
    }
}
