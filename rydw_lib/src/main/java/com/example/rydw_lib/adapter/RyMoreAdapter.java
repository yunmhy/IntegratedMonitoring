package com.example.rydw_lib.adapter;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.base_lib.utils.TimeUtils;
import com.example.rydw_lib.R;
import com.example.rydw_lib.bean.RyMoreBean;

public class RyMoreAdapter extends BaseQuickAdapter<RyMoreBean.DataBean, BaseViewHolder> {

    public RyMoreAdapter(int layoutResId) {
        super(layoutResId);
    }

    @Override
    protected void convert(BaseViewHolder helper, RyMoreBean.DataBean item) {
        helper.getView(R.id.img_ryshow).setRotation(180);
        helper.setGone(R.id.ry_department_ly,false);
       helper.addOnClickListener(R.id.ry_show_more);
       helper.setText(R.id.ry_regionnum,item.getRegionnum()).setText(R.id.ry_regionname,item.getRegionname())
               .setText(R.id.ry_person_name,item.getPersonname()+"-"+item.getPersonPost()).setText(R.id.ry_person_code,item.getCardnumber())
               .setText(R.id.ry_department,item.getDepartment())
               .setText(R.id.ry_onoutwellstate,(item.getOnoutwellstate().equals("1")?"入井":"出井"))
               .setText(R.id.ry_onwelltime, TimeUtils.CurveTimeFarmat(item.getOnwelltime()))
               .setText(R.id.ry_inarea_time,TimeUtils.CurveTimeFarmat(item.getOnregiontime()))
               .setText(R.id.ry_station_code,item.getStationnum()).setText(R.id.ry_station_name,item.getStationname())
               .setText(R.id.ry_instation_time,TimeUtils.CurveTimeFarmat(item.getOnstationtime()));
    }
}
