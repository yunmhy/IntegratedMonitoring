package com.example.rydw_lib.adapter;

import android.graphics.Color;
import android.text.TextPaint;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.base_lib.utils.TimeUtils;
import com.example.rydw_lib.R;
import com.example.rydw_lib.bean.RyAlarmBean;

public class RyMainAlarmAdapter extends BaseQuickAdapter {
    private int layoutResId;

    public RyMainAlarmAdapter(int layoutResId) {
        super(layoutResId);
        this.layoutResId = layoutResId;
    }

    @Override
    protected void convert(BaseViewHolder helper, Object item) {
        if (layoutResId == R.layout.specialperson_item) {


            RyAlarmBean.DataBean.SpecialpersonabnormaBean bean = (RyAlarmBean.DataBean.SpecialpersonabnormaBean) item;
            helper.setText(R.id.ry_alarm_special_name, bean.getPersonname()).setText(R.id.ry_alarm_special_code, bean.getCardnumber())
                    .setText(R.id.ry_alarm_special_shouldtime, TimeUtils.CurveTimeFarmat(bean.getShouldtime())).
                    setText(R.id.ry_alarm_special_realartime, TimeUtils.CurveTimeFarmat(bean.getRealarrivetime()))
                    .setText(R.id.ry_alarm_special_state, bean.getState());
        }
        if (layoutResId == R.layout.personalarm_item) {

            RyAlarmBean.DataBean.PersoncallalarmBean bean = (RyAlarmBean.DataBean.PersoncallalarmBean) item;
            TextView view = helper.getView(R.id.ry_alarm_person_endtime);
            TextPaint tp = view.getPaint();
            String endtime;
            if (bean.getAlarmendtime() == null) {
                tp.setFakeBoldText(true);
                view.setTextColor(Color.parseColor("#FF0000"));
                endtime = "正在报警";
            } else {
                endtime = TimeUtils.CurveTimeFarmat(bean.getAlarmendtime());
            }
            helper.setText(R.id.ry_alarm_person_name, bean.getPersonname()).setText(R.id.ry_alarm_person_code, bean.getCardnumber())
                    .setText(R.id.ry_alarm_person_starttime, TimeUtils.CurveTimeFarmat(bean.getAlarmstarttime()))
                    .setText(R.id.ry_alarm_person_endtime, endtime);

        }
        if (layoutResId == R.layout.overtimealarm_item) {
            RyAlarmBean.DataBean.OvertimealarmBean bean = (RyAlarmBean.DataBean.OvertimealarmBean) item;
            TextView view = helper.getView(R.id.ry_alarm_overtime_endtime);
            TextPaint tp = view.getPaint();
            String endtime;
            if (bean.getAlarmendtime() == null) {
                tp.setFakeBoldText(true);
                view.setTextColor(Color.parseColor("#FF0000"));
                endtime = "正在报警";
            } else {
                endtime = TimeUtils.CurveTimeFarmat(bean.getAlarmendtime());
            }
            helper.setText(R.id.ry_alarm_overtime_name, bean.getPersonname()).setText(R.id.ry_alarm_overtime_code, bean.getCardnumber())
                    .setText(R.id.ry_alarm_overtime_starttime, TimeUtils.CurveTimeFarmat(bean.getAlarmstarttime()))
                    .setText(R.id.ry_alarm_overtime_endtime,endtime)
                    .setText(R.id.ry_overtime_ontime, TimeUtils.CurveTimeFarmat(bean.getOnwelltime()))
                    .setText(R.id.ry_overtime_inarea_time, TimeUtils.CurveTimeFarmat(bean.getOnregiontime()))
                    .setText(R.id.ry_overtime_instation_time, TimeUtils.CurveTimeFarmat(bean.getOnstationtime()))
                    .setText(R.id.ry_overtime_station_code, bean.getStationnum())
                    .setText(R.id.ry_overtime_station_name, bean.getStationname())
                    .setText(R.id.ry_overtime_regionname, bean.getRegionname()).setText(R.id.ry_overtime_regionnum, bean.getRegionnum());
        }
        if (layoutResId == R.layout.overpersonalarm_item) {
            RyAlarmBean.DataBean.OverpersonalarmBean bean = (RyAlarmBean.DataBean.OverpersonalarmBean) item;

            TextView view = helper.getView(R.id.ry_alarm_overperson_endtime);
            TextPaint tp = view.getPaint();
            String endtime;
            if (bean.getAlarmendtime() == null) {
                tp.setFakeBoldText(true);
                view.setTextColor(Color.parseColor("#FF0000"));
                endtime = "正在报警";
            } else {
                endtime = TimeUtils.CurveTimeFarmat(bean.getAlarmendtime());
            }
            helper.setText(R.id.ry_alarm_overperson_area, bean.getRegionname()).setText(R.id.ry_alarm_overperson_areacode, bean.getRegionnum()+"")
                    .setText(R.id.ry_alarm_overperson_starttime, TimeUtils.CurveTimeFarmat(bean.getAlarmstarttime())).
                    setText(R.id.ry_alarm_overperson_endtime, endtime)
                    .setText(R.id.ry_alarm_overperson_type, bean.getOverpersontype())
                    .setText(R.id.ry_overperson_total, bean.getTotalpersonnum() + "")
                    .setText(R.id.ry_overperson_required, bean.getRequiredpersonnum() + "");
        }
        if (layoutResId == R.layout.sysalarm_item) {

            RyAlarmBean.DataBean.SysalarmBean bean = (RyAlarmBean.DataBean.SysalarmBean) item;
            TextView view = helper.getView(R.id.ry_alarm_sys_endtime);
            TextPaint tp = view.getPaint();
            String endtime;
            if (bean.getAbnormalendtime() == null) {
                tp.setFakeBoldText(true);
                view.setTextColor(Color.parseColor("#FF0000"));
                endtime = "正在报警";
            } else {
                endtime = TimeUtils.CurveTimeFarmat(bean.getAbnormalendtime());
            }
            helper.setText(R.id.ry_alarm_sys_sname, bean.getStaionname()).setText(R.id.ry_alarm_sys_scode, bean.getStationnum())
                    .setText(R.id.ry_alarm_sys_starttime, TimeUtils.CurveTimeFarmat(bean.getAbnormalstarttime())).
                    setText(R.id.ry_alarm_sys_endtime, endtime)
                    .setText(R.id.ry_alarm_sys_operamsg, bean.getOperatetext())
                    .setText(R.id.ry_alarm_sys_errmsg, bean.getAbnormaltext());
        }
    }


}
