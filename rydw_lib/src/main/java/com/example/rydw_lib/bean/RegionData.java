package com.example.rydw_lib.bean;

import java.util.List;

public class RegionData {

    /**
     * msg : 查询成功
     * code : 0
     * data : [{"regiontype":"井口区域","regionnum":"01","regionname":"井口区域","regioncheckpersonnum":120,"regioncount":null},{"regiontype":"重点区域","regionnum":"02","regionname":"重点区域","regioncheckpersonnum":120,"regioncount":80},{"regiontype":"限制区域","regionnum":"03","regionname":"限制区域","regioncheckpersonnum":0,"regioncount":null},{"regiontype":"其它区域","regionnum":"04","regionname":"其他区域","regioncheckpersonnum":120,"regioncount":84}]
     */

    private String msg;
    private int code;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * regiontype : 井口区域
         * regionnum : 01
         * regionname : 井口区域
         * regioncheckpersonnum : 120
         * regioncount : null
         */

        private String regiontype;
        private String regionnum;
        private String regionname;
        private int regioncheckpersonnum;
        private int regioncount;

        public String getRegiontype() {
            return regiontype;
        }

        public void setRegiontype(String regiontype) {
            this.regiontype = regiontype;
        }

        public String getRegionnum() {
            return regionnum;
        }

        public void setRegionnum(String regionnum) {
            this.regionnum = regionnum;
        }

        public String getRegionname() {
            return regionname;
        }

        public void setRegionname(String regionname) {
            this.regionname = regionname;
        }

        public int getRegioncheckpersonnum() {
            return regioncheckpersonnum;
        }

        public void setRegioncheckpersonnum(int regioncheckpersonnum) {
            this.regioncheckpersonnum = regioncheckpersonnum;
        }

        public int getRegioncount() {
            return regioncount;
        }

        public void setRegioncount(int regioncount) {
            this.regioncount = regioncount;
        }
    }
}
