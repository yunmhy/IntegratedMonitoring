package com.example.rydw_lib.bean;

import java.io.Serializable;

public class RydwMainBean implements Serializable {

    /**
     * msg : 查询成功
     * code : 0
     * data : {"minecode":"140121B0014000000001","minename":null,"checkpersonnum":0,"maxworktime":0,"classtime":0,"realtimenum":188,"leadernum":9,"specianum":11,"overtimenum":6,"overminenum":0,"specialalarmnum":0,"datatime":"2020-03-20T10:52:31.000+0800","callnum":0,"minenum":0,"sysalarmnum":29}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * minecode : 140121B0014000000001
         * minename : null
         * checkpersonnum : 0
         * maxworktime : 0
         * classtime : 0
         * realtimenum : 188
         * leadernum : 9
         * specianum : 11
         * overtimenum : 6
         * overminenum : 0
         * specialalarmnum : 0
         * datatime : 2020-03-20T10:52:31.000+0800
         * callnum : 0
         * minenum : 0
         * sysalarmnum : 29
         */

        private String minecode;
        private String minename;
        private int checkpersonnum;
        private int maxworktime;
        private int classtime;
        private int realtimenum;
        private int leadernum;
        private int specianum;
        private int overtimenum;
        private int overminenum;
        private int specialalarmnum;
        private String datatime;
        private int callnum;
        private int minenum;
        private int sysalarmnum;

        public String getMinecode() {
            return minecode;
        }

        public void setMinecode(String minecode) {
            this.minecode = minecode;
        }

        public String getMinename() {
            return minename;
        }

        public void setMinename(String minename) {
            this.minename = minename;
        }

        public int getCheckpersonnum() {
            return checkpersonnum;
        }

        public void setCheckpersonnum(int checkpersonnum) {
            this.checkpersonnum = checkpersonnum;
        }

        public int getMaxworktime() {
            return maxworktime;
        }

        public void setMaxworktime(int maxworktime) {
            this.maxworktime = maxworktime;
        }

        public int getClasstime() {
            return classtime;
        }

        public void setClasstime(int classtime) {
            this.classtime = classtime;
        }

        public int getRealtimenum() {
            return realtimenum;
        }

        public void setRealtimenum(int realtimenum) {
            this.realtimenum = realtimenum;
        }

        public int getLeadernum() {
            return leadernum;
        }

        public void setLeadernum(int leadernum) {
            this.leadernum = leadernum;
        }

        public int getSpecianum() {
            return specianum;
        }

        public void setSpecianum(int specianum) {
            this.specianum = specianum;
        }

        public int getOvertimenum() {
            return overtimenum;
        }

        public void setOvertimenum(int overtimenum) {
            this.overtimenum = overtimenum;
        }

        public int getOverminenum() {
            return overminenum;
        }

        public void setOverminenum(int overminenum) {
            this.overminenum = overminenum;
        }

        public int getSpecialalarmnum() {
            return specialalarmnum;
        }

        public void setSpecialalarmnum(int specialalarmnum) {
            this.specialalarmnum = specialalarmnum;
        }

        public String getDatatime() {
            return datatime;
        }

        public void setDatatime(String datatime) {
            this.datatime = datatime;
        }

        public int getCallnum() {
            return callnum;
        }

        public void setCallnum(int callnum) {
            this.callnum = callnum;
        }

        public int getMinenum() {
            return minenum;
        }

        public void setMinenum(int minenum) {
            this.minenum = minenum;
        }

        public int getSysalarmnum() {
            return sysalarmnum;
        }

        public void setSysalarmnum(int sysalarmnum) {
            this.sysalarmnum = sysalarmnum;
        }
    }
}
