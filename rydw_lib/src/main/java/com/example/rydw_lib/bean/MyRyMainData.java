package com.example.rydw_lib.bean;

import android.widget.LinearLayout;

import java.util.List;

public class MyRyMainData {


    RydwMainBean.DataBean mainBeanList;
    List<List<RegionData.DataBean>>regionDataList;

    public MyRyMainData(RydwMainBean.DataBean mainBeanList, List<List<RegionData.DataBean>> regionDataList) {
        this.mainBeanList = mainBeanList;
        this.regionDataList = regionDataList;
    }

    public RydwMainBean.DataBean getMainBeanList() {
        return mainBeanList;
    }

    public void setMainBeanList(RydwMainBean.DataBean mainBeanList) {
        this.mainBeanList = mainBeanList;
    }

    public List<List<RegionData.DataBean>> getRegionDataList() {
        return regionDataList;
    }

    public void setRegionDataList(List<List<RegionData.DataBean>> regionDataList) {
        this.regionDataList = regionDataList;
    }
}
