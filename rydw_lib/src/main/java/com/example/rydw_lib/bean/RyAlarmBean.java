package com.example.rydw_lib.bean;

import java.util.List;

public class RyAlarmBean {


    /**
     * msg : 查询成功
     * code : 0
     * data : {"personcallalarm":[{"id":3241,"minecode":"140121B0014000000001","minename":null,"cardnumber":"03063","personname":"张向东","alarmstarttime":"2020-03-23T08:19:33.000+0800","alarmendtime":"2020-03-23T08:19:53.000+0800","inserttime":"2020-03-23T08:20:04.000+0800","Duration":null,"dataTime":"2020-03-23T08:19:53.000+0800"}],"overtimealarm":[{"id":38724,"cardnumber":"06825","personname":"秦素锋","worktype":null,"minecode":"140121B0014000000001","minename":null,"onwelltime":"2020-03-22T22:32:57.000+0800","alarmstarttime":"2020-03-23T09:33:02.000+0800","alarmendtime":null,"regionnum":"04","regionname":null,"onregiontime":"2020-03-23T09:28:36.000+0800","stationnum":"0403","stationname":null,"onstationtime":"2020-03-23T09:28:36.000+0800","inserttime":"2020-03-23T10:01:54.000+0800","Duration":null,"dataTime":"2020-03-23T10:01:40.000+0800"},{"id":38725,"cardnumber":"06823","personname":"王海林","worktype":null,"minecode":"140121B0014000000001","minename":null,"onwelltime":"2020-03-22T22:34:50.000+0800","alarmstarttime":"2020-03-23T09:34:55.000+0800","alarmendtime":null,"regionnum":"04","regionname":null,"onregiontime":"2020-03-23T09:33:56.000+0800","stationnum":"0416","stationname":null,"onstationtime":"2020-03-23T09:33:56.000+0800","inserttime":"2020-03-23T10:01:54.000+0800","Duration":null,"dataTime":"2020-03-23T10:01:40.000+0800"},{"id":38726,"cardnumber":"02192","personname":"郭庆堂","worktype":null,"minecode":"140121B0014000000001","minename":null,"onwelltime":"2020-03-22T22:35:27.000+0800","alarmstarttime":"2020-03-23T09:35:31.000+0800","alarmendtime":null,"regionnum":"04","regionname":null,"onregiontime":"2020-03-23T09:29:01.000+0800","stationnum":"0416","stationname":null,"onstationtime":"2020-03-23T09:29:01.000+0800","inserttime":"2020-03-23T10:01:54.000+0800","Duration":null,"dataTime":"2020-03-23T10:01:40.000+0800"},{"id":38727,"cardnumber":"02819","personname":"张达科","worktype":null,"minecode":"140121B0014000000001","minename":null,"onwelltime":"2020-03-22T22:33:38.000+0800","alarmstarttime":"2020-03-23T09:33:43.000+0800","alarmendtime":null,"regionnum":"04","regionname":null,"onregiontime":"2020-03-23T09:33:37.000+0800","stationnum":"0416","stationname":null,"onstationtime":"2020-03-23T09:33:37.000+0800","inserttime":"2020-03-23T10:01:54.000+0800","Duration":null,"dataTime":"2020-03-23T10:01:40.000+0800"},{"id":38728,"cardnumber":"06290","personname":"范东升","worktype":null,"minecode":"140121B0014000000001","minename":null,"onwelltime":"2020-03-22T22:33:31.000+0800","alarmstarttime":"2020-03-23T09:33:33.000+0800","alarmendtime":null,"regionnum":"04","regionname":null,"onregiontime":"2020-03-23T09:29:44.000+0800","stationnum":"0416","stationname":null,"onstationtime":"2020-03-23T09:29:44.000+0800","inserttime":"2020-03-23T10:01:54.000+0800","Duration":null,"dataTime":"2020-03-23T10:01:40.000+0800"},{"id":38729,"cardnumber":"03259","personname":"王建伟","worktype":null,"minecode":"140121B0014000000001","minename":null,"onwelltime":"2020-03-22T22:35:27.000+0800","alarmstarttime":"2020-03-23T09:35:31.000+0800","alarmendtime":null,"regionnum":"04","regionname":null,"onregiontime":"2020-03-23T09:29:49.000+0800","stationnum":"0416","stationname":null,"onstationtime":"2020-03-23T09:29:49.000+0800","inserttime":"2020-03-23T10:01:54.000+0800","Duration":null,"dataTime":"2020-03-23T10:01:40.000+0800"},{"id":38730,"cardnumber":"06268","personname":"李国祥","worktype":null,"minecode":"140121B0014000000001","minename":null,"onwelltime":"2020-03-22T22:35:42.000+0800","alarmstarttime":"2020-03-23T09:35:46.000+0800","alarmendtime":null,"regionnum":"04","regionname":null,"onregiontime":"2020-03-23T09:32:30.000+0800","stationnum":"0416","stationname":null,"onstationtime":"2020-03-23T09:32:30.000+0800","inserttime":"2020-03-23T10:01:54.000+0800","Duration":null,"dataTime":"2020-03-23T10:01:40.000+0800"}],"sysalarm":[{"id":25166,"minecode":"140121B0014000000001","minename":null,"stationnum":null,"staionname":null,"abnormaltext":"12号读卡器通信故障","operatetext":null,"abnormalstarttime":"2020-03-23T00:00:00.000+0800","abnormalendtime":null,"inserttime":"2020-03-23T10:01:54.000+0800","Duration":null,"dataTime":"2020-03-23T10:01:40.000+0800"},{"id":25167,"minecode":"140121B0014000000001","minename":null,"stationnum":null,"staionname":null,"abnormaltext":"17号读卡器通信故障","operatetext":null,"abnormalstarttime":"2020-03-23T00:00:00.000+0800","abnormalendtime":null,"inserttime":"2020-03-23T10:01:54.000+0800","Duration":null,"dataTime":"2020-03-23T10:01:40.000+0800"}],"overpersonalarm":[],"specialpersonabnorma":[]}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<PersoncallalarmBean> personcallalarm;
        private List<OvertimealarmBean> overtimealarm;
        private List<SysalarmBean> sysalarm;
        private List<OverpersonalarmBean> overpersonalarm;
        private List<SpecialpersonabnormaBean> specialpersonabnorma;

        public List<PersoncallalarmBean> getPersoncallalarm() {
            return personcallalarm;
        }

        public void setPersoncallalarm(List<PersoncallalarmBean> personcallalarm) {
            this.personcallalarm = personcallalarm;
        }

        public List<OvertimealarmBean> getOvertimealarm() {
            return overtimealarm;
        }

        public void setOvertimealarm(List<OvertimealarmBean> overtimealarm) {
            this.overtimealarm = overtimealarm;
        }

        public List<SysalarmBean> getSysalarm() {
            return sysalarm;
        }

        public void setSysalarm(List<SysalarmBean> sysalarm) {
            this.sysalarm = sysalarm;
        }

        public List<OverpersonalarmBean> getOverpersonalarm() {
            return overpersonalarm;
        }

        public void setOverpersonalarm(List<OverpersonalarmBean> overpersonalarm) {
            this.overpersonalarm = overpersonalarm;
        }

        public List<SpecialpersonabnormaBean> getSpecialpersonabnorma() {
            return specialpersonabnorma;
        }

        public void setSpecialpersonabnorma(List<SpecialpersonabnormaBean> specialpersonabnorma) {
            this.specialpersonabnorma = specialpersonabnorma;
        }

        public static class PersoncallalarmBean {
            /**
             * id : 3241
             * minecode : 140121B0014000000001
             * minename : null
             * cardnumber : 03063
             * personname : 张向东
             * alarmstarttime : 2020-03-23T08:19:33.000+0800
             * alarmendtime : 2020-03-23T08:19:53.000+0800
             * inserttime : 2020-03-23T08:20:04.000+0800
             * Duration : null
             * dataTime : 2020-03-23T08:19:53.000+0800
             */

            private int id;
            private String minecode;
            private String minename;
            private String cardnumber;
            private String personname;
            private String alarmstarttime;
            private String alarmendtime;
            private String inserttime;
            private int Duration;
            private String dataTime;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getMinecode() {
                return minecode;
            }

            public void setMinecode(String minecode) {
                this.minecode = minecode;
            }

            public String getMinename() {
                return minename;
            }

            public void setMinename(String minename) {
                this.minename = minename;
            }

            public String getCardnumber() {
                return cardnumber;
            }

            public void setCardnumber(String cardnumber) {
                this.cardnumber = cardnumber;
            }

            public String getPersonname() {
                return personname;
            }

            public void setPersonname(String personname) {
                this.personname = personname;
            }

            public String getAlarmstarttime() {
                return alarmstarttime;
            }

            public void setAlarmstarttime(String alarmstarttime) {
                this.alarmstarttime = alarmstarttime;
            }

            public String getAlarmendtime() {
                return alarmendtime;
            }

            public void setAlarmendtime(String alarmendtime) {
                this.alarmendtime = alarmendtime;
            }

            public String getInserttime() {
                return inserttime;
            }

            public void setInserttime(String inserttime) {
                this.inserttime = inserttime;
            }

            public int getDuration() {
                return Duration;
            }

            public void setDuration(int Duration) {
                this.Duration = Duration;
            }

            public String getDataTime() {
                return dataTime;
            }

            public void setDataTime(String dataTime) {
                this.dataTime = dataTime;
            }
        }

        public static class OvertimealarmBean {
            /**
             * id : 38724
             * cardnumber : 06825
             * personname : 秦素锋
             * worktype : null
             * minecode : 140121B0014000000001
             * minename : null
             * onwelltime : 2020-03-22T22:32:57.000+0800
             * alarmstarttime : 2020-03-23T09:33:02.000+0800
             * alarmendtime : null
             * regionnum : 04
             * regionname : null
             * onregiontime : 2020-03-23T09:28:36.000+0800
             * stationnum : 0403
             * stationname : null
             * onstationtime : 2020-03-23T09:28:36.000+0800
             * inserttime : 2020-03-23T10:01:54.000+0800
             * Duration : null
             * dataTime : 2020-03-23T10:01:40.000+0800
             */

            private int id;
            private String cardnumber;
            private String personname;
            private String worktype;
            private String minecode;
            private String minename;
            private String onwelltime;
            private String alarmstarttime;
            private String alarmendtime;
            private String regionnum;
            private String regionname;
            private String onregiontime;
            private String stationnum;
            private String stationname;
            private String onstationtime;
            private String inserttime;
            private int Duration;
            private String dataTime;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCardnumber() {
                return cardnumber;
            }

            public void setCardnumber(String cardnumber) {
                this.cardnumber = cardnumber;
            }

            public String getPersonname() {
                return personname;
            }

            public void setPersonname(String personname) {
                this.personname = personname;
            }

            public String getWorktype() {
                return worktype;
            }

            public void setWorktype(String worktype) {
                this.worktype = worktype;
            }

            public String getMinecode() {
                return minecode;
            }

            public void setMinecode(String minecode) {
                this.minecode = minecode;
            }

            public String getMinename() {
                return minename;
            }

            public void setMinename(String minename) {
                this.minename = minename;
            }

            public String getOnwelltime() {
                return onwelltime;
            }

            public void setOnwelltime(String onwelltime) {
                this.onwelltime = onwelltime;
            }

            public String getAlarmstarttime() {
                return alarmstarttime;
            }

            public void setAlarmstarttime(String alarmstarttime) {
                this.alarmstarttime = alarmstarttime;
            }

            public String getAlarmendtime() {
                return alarmendtime;
            }

            public void setAlarmendtime(String alarmendtime) {
                this.alarmendtime = alarmendtime;
            }

            public String getRegionnum() {
                return regionnum;
            }

            public void setRegionnum(String regionnum) {
                this.regionnum = regionnum;
            }

            public String getRegionname() {
                return regionname;
            }

            public void setRegionname(String regionname) {
                this.regionname = regionname;
            }

            public String getOnregiontime() {
                return onregiontime;
            }

            public void setOnregiontime(String onregiontime) {
                this.onregiontime = onregiontime;
            }

            public String getStationnum() {
                return stationnum;
            }

            public void setStationnum(String stationnum) {
                this.stationnum = stationnum;
            }

            public String getStationname() {
                return stationname;
            }

            public void setStationname(String stationname) {
                this.stationname = stationname;
            }

            public String getOnstationtime() {
                return onstationtime;
            }

            public void setOnstationtime(String onstationtime) {
                this.onstationtime = onstationtime;
            }

            public String getInserttime() {
                return inserttime;
            }

            public void setInserttime(String inserttime) {
                this.inserttime = inserttime;
            }

            public int getDuration() {
                return Duration;
            }

            public void setDuration(int Duration) {
                this.Duration = Duration;
            }

            public String getDataTime() {
                return dataTime;
            }

            public void setDataTime(String dataTime) {
                this.dataTime = dataTime;
            }
        }

        public static class SysalarmBean {
            /**
             * id : 25166
             * minecode : 140121B0014000000001
             * minename : null
             * stationnum : null
             * staionname : null
             * abnormaltext : 12号读卡器通信故障
             * operatetext : null
             * abnormalstarttime : 2020-03-23T00:00:00.000+0800
             * abnormalendtime : null
             * inserttime : 2020-03-23T10:01:54.000+0800
             * Duration : null
             * dataTime : 2020-03-23T10:01:40.000+0800
             */

            private int id;
            private String minecode;
            private String minename;
            private String stationnum;
            private String staionname;
            private String abnormaltext;
            private String operatetext;
            private String abnormalstarttime;
            private String abnormalendtime;
            private String inserttime;
            private int Duration;
            private String dataTime;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getMinecode() {
                return minecode;
            }

            public void setMinecode(String minecode) {
                this.minecode = minecode;
            }

            public String getMinename() {
                return minename;
            }

            public void setMinename(String minename) {
                this.minename = minename;
            }

            public String getStationnum() {
                return stationnum;
            }

            public void setStationnum(String stationnum) {
                this.stationnum = stationnum;
            }

            public String getStaionname() {
                return staionname;
            }

            public void setStaionname(String staionname) {
                this.staionname = staionname;
            }

            public String getAbnormaltext() {
                return abnormaltext;
            }

            public void setAbnormaltext(String abnormaltext) {
                this.abnormaltext = abnormaltext;
            }

            public String getOperatetext() {
                return operatetext;
            }

            public void setOperatetext(String operatetext) {
                this.operatetext = operatetext;
            }

            public String getAbnormalstarttime() {
                return abnormalstarttime;
            }

            public void setAbnormalstarttime(String abnormalstarttime) {
                this.abnormalstarttime = abnormalstarttime;
            }

            public String getAbnormalendtime() {
                return abnormalendtime;
            }

            public void setAbnormalendtime(String abnormalendtime) {
                this.abnormalendtime = abnormalendtime;
            }

            public String getInserttime() {
                return inserttime;
            }

            public void setInserttime(String inserttime) {
                this.inserttime = inserttime;
            }

            public int getDuration() {
                return Duration;
            }

            public void setDuration(int Duration) {
                this.Duration = Duration;
            }

            public String getDataTime() {
                return dataTime;
            }

            public void setDataTime(String dataTime) {
                this.dataTime = dataTime;
            }
        }

        public static class OverpersonalarmBean {

           private int id;
           private String minecode;
           private String minename;
           private String overpersontype;
           private int requiredpersonnum;
           private int totalpersonnum;
           private int regionnum;
           private String regionname;
           private String alarmstarttime;
           private String alarmendtime;
           private String regionpersoncard;
           private String department;
           private String inserttime;
           private int Duration;
           private String dataTime;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getMinecode() {
                return minecode;
            }

            public void setMinecode(String minecode) {
                this.minecode = minecode;
            }

            public String getMinename() {
                return minename;
            }

            public void setMinename(String minename) {
                this.minename = minename;
            }

            public String getOverpersontype() {
                return overpersontype;
            }

            public void setOverpersontype(String overpersontype) {
                this.overpersontype = overpersontype;
            }

            public int getRequiredpersonnum() {
                return requiredpersonnum;
            }

            public void setRequiredpersonnum(int requiredpersonnum) {
                this.requiredpersonnum = requiredpersonnum;
            }

            public int getTotalpersonnum() {
                return totalpersonnum;
            }

            public void setTotalpersonnum(int totalpersonnum) {
                this.totalpersonnum = totalpersonnum;
            }

            public int getRegionnum() {
                return regionnum;
            }

            public void setRegionnum(int regionnum) {
                this.regionnum = regionnum;
            }

            public String getRegionname() {
                return regionname;
            }

            public void setRegionname(String regionname) {
                this.regionname = regionname;
            }

            public String getAlarmstarttime() {
                return alarmstarttime;
            }

            public void setAlarmstarttime(String alarmstarttime) {
                this.alarmstarttime = alarmstarttime;
            }

            public String getAlarmendtime() {
                return alarmendtime;
            }

            public void setAlarmendtime(String alarmendtime) {
                this.alarmendtime = alarmendtime;
            }

            public String getRegionpersoncard() {
                return regionpersoncard;
            }

            public void setRegionpersoncard(String regionpersoncard) {
                this.regionpersoncard = regionpersoncard;
            }

            public String getDepartment() {
                return department;
            }

            public void setDepartment(String department) {
                this.department = department;
            }

            public String getInserttime() {
                return inserttime;
            }

            public void setInserttime(String inserttime) {
                this.inserttime = inserttime;
            }

            public int getDuration() {
                return Duration;
            }

            public void setDuration(int duration) {
                Duration = duration;
            }

            public String getDataTime() {
                return dataTime;
            }

            public void setDataTime(String dataTime) {
                this.dataTime = dataTime;
            }
        }

        public static class SpecialpersonabnormaBean {

            private int id;
            private String minecode;
            private String minename;
            private String cardnumber;
            private String personname;
            private int shouldarrivestationnum;
            private String shouldarrivestationname ;
            private String shouldtime;
            private String state;
            private String realarrivetime;
            private String abnormaltime;
            private String spePersonExp;
            private String inserttime;
            private int Duration;
            private String dataTime	;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getMinecode() {
                return minecode;
            }

            public void setMinecode(String minecode) {
                this.minecode = minecode;
            }

            public String getMinename() {
                return minename;
            }

            public void setMinename(String minename) {
                this.minename = minename;
            }

            public String getCardnumber() {
                return cardnumber;
            }

            public void setCardnumber(String cardnumber) {
                this.cardnumber = cardnumber;
            }

            public String getPersonname() {
                return personname;
            }

            public void setPersonname(String personname) {
                this.personname = personname;
            }

            public int getShouldarrivestationnum() {
                return shouldarrivestationnum;
            }

            public void setShouldarrivestationnum(int shouldarrivestationnum) {
                this.shouldarrivestationnum = shouldarrivestationnum;
            }

            public String getShouldarrivestationname() {
                return shouldarrivestationname;
            }

            public void setShouldarrivestationname(String shouldarrivestationname) {
                this.shouldarrivestationname = shouldarrivestationname;
            }

            public String getShouldtime() {
                return shouldtime;
            }

            public void setShouldtime(String shouldtime) {
                this.shouldtime = shouldtime;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getRealarrivetime() {
                return realarrivetime;
            }

            public void setRealarrivetime(String realarrivetime) {
                this.realarrivetime = realarrivetime;
            }

            public String getAbnormaltime() {
                return abnormaltime;
            }

            public void setAbnormaltime(String abnormaltime) {
                this.abnormaltime = abnormaltime;
            }

            public String getSpePersonExp() {
                return spePersonExp;
            }

            public void setSpePersonExp(String spePersonExp) {
                this.spePersonExp = spePersonExp;
            }

            public String getInserttime() {
                return inserttime;
            }

            public void setInserttime(String inserttime) {
                this.inserttime = inserttime;
            }

            public int getDuration() {
                return Duration;
            }

            public void setDuration(int duration) {
                Duration = duration;
            }

            public String getDataTime() {
                return dataTime;
            }

            public void setDataTime(String dataTime) {
                this.dataTime = dataTime;
            }
        }
    }
}
