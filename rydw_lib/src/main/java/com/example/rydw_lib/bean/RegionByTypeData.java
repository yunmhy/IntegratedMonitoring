package com.example.rydw_lib.bean;

import java.util.List;

public class RegionByTypeData {

    /**
     * msg : 查询成功
     * code : 0
     * data : [{"regiontype":"井口区域","regioncount":null},{"regiontype":"其它区域","regioncount":95},{"regiontype":"重点区域","regioncount":81},{"regiontype":"限制区域","regioncount":null}]
     */

    private String msg;
    private int code;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * regiontype : 井口区域
         * regioncount : null
         */

        private String regiontype;
        private int regioncount;

        public String getRegiontype() {
            return regiontype;
        }

        public void setRegiontype(String regiontype) {
            this.regiontype = regiontype;
        }

        public int getRegioncount() {
            return regioncount;
        }

        public void setRegioncount(int regioncount) {
            this.regioncount = regioncount;
        }
    }
}
