package com.example.rydw_lib.ui.listui;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.LifecycleService;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.fragment.BaseFragment;
import com.example.rydw_lib.R;
import com.example.rydw_lib.RydwMainActivity;
import com.example.rydw_lib.adapter.RyListAdapter;
import com.example.rydw_lib.adapter.RyMoreAdapter;
import com.example.rydw_lib.bean.RyMoreBean;
import com.example.rydw_lib.bean.RydwListBean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ListFragment extends BaseFragment implements View.OnClickListener {
    List<MultiItemEntity> list;
    private ListViewModel listViewModel;
    private SwipeRefreshLayout refreshLayout;
    private RyMoreAdapter ryListAdapter;
    private RecyclerView listRcy;
    private UserInfo userInfo;
    private String type = "personlist";
    private OptionsPickerView pvNick;
    private LinearLayout lyRyList;
    private TextView tPersonNum;
    private TextView tSpecialNum;
    private TextView tLeaderNum;
    private TextView textRylistCount;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userInfo = ((RydwMainActivity) context).toValue();
    }

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        listViewModel =
                ViewModelProviders.of(this).get(ListViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }

    @Override
    protected void initData() {
        View empty = LayoutInflater.from(getContext()).inflate(R.layout.empty_item, null, false);
        ryListAdapter.setEmptyView(empty);
        final TextView textView = empty.findViewById(R.id.txt_errmsg);
        final TextView msg = empty.findViewById(R.id.text_empty);
        final ImageView imageView = empty.findViewById(R.id.empty_img);
        final ConstraintLayout errlay = empty.findViewById(R.id.err_lay);
        errlay.setVisibility(View.INVISIBLE);
        final Button btn_rest = empty.findViewById(R.id.btn_rest);
        btn_rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh(type);
            }
        });
        listViewModel.set(userInfo, this.getActivity());
        listViewModel.getData().observe(this, new Observer<RyMoreBean>() {
            @Override
            public void onChanged(RyMoreBean rydwListBean) {
                if (rydwListBean.getCode()==0&&rydwListBean.getData()!=null&&rydwListBean.getData().size()>0){
                    textRylistCount.setText("共"+rydwListBean.getData().size()+"人");
                    ryListAdapter.setNewData(rydwListBean.getData());
                }else {
                    errlay.setVisibility(View.VISIBLE);
                    btn_rest.setVisibility(View.INVISIBLE);
                    textView.setVisibility(View.INVISIBLE);
                    imageView.setVisibility(View.INVISIBLE);
                    msg.setText("暂无数据");
                }


            }
        });
        listViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)) {
                    textView.setText(s);
                    errlay.setVisibility(View.VISIBLE);
                    Toast.makeText(getActivity().getApplicationContext(), "获取数据失败", Toast.LENGTH_LONG).show();
                }
                refreshLayout.setRefreshing(false);
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        listRcy.postDelayed(new Runnable() {
            @Override
            public void run() {
                //这个方法是让一进入页面的时候实现网络请求，有个缓冲的效果
                refresh(type);
            }
        }, 500);
    }

    @Override
    protected void initView(View view) {

        textRylistCount = (TextView) view.findViewById(R.id.text_rylist_count);
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.rylistfefer);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh(type);
            }
        });
        view.findViewById(R.id.ly_person_num).setOnClickListener(this);
        view.findViewById(R.id.ly_special_num).setOnClickListener(this);
        view.findViewById(R.id.ly_leader_num).setOnClickListener(this);

        tPersonNum = (TextView) view.findViewById(R.id.t_person_num);
        tSpecialNum = (TextView) view.findViewById(R.id.t_special_num);
        tLeaderNum = (TextView) view.findViewById(R.id.t_leader_num);


        listRcy = (RecyclerView) view.findViewById(R.id.list_rcy);
        lyRyList = (LinearLayout) view.findViewById(R.id.ly_ry_list);
        listRcy.setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));

        lyRyList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pvNick.show();
            }
        });
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listRcy.setLayoutManager(layoutManager);
        ryListAdapter = new RyMoreAdapter(R.layout.ry_more_item);
        listRcy.setAdapter(ryListAdapter);
//        initNickPicker(getContext(),"选择人员类型");
        ryListAdapter.bindToRecyclerView(listRcy);
        ryListAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                RyMoreBean.DataBean item = (RyMoreBean.DataBean) adapter.getItem(position);
                View viewByPosition = adapter.getViewByPosition(position, R.id.ry_department_ly);
                View img = adapter.getViewByPosition(position, R.id.img_ryshow);
                if (!item.flag) {
                    item.flag = true;
                    img.setRotation(0);
                    viewByPosition.setVisibility(View.VISIBLE);
                } else {
                    item.flag = false;
                    img.setRotation(180);
                    viewByPosition.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_list_ry;
    }

    private void refresh(String type) {
        tPersonNum.setTextColor(Color.parseColor("#66ffffff"));
        tSpecialNum.setTextColor(Color.parseColor("#66ffffff"));
        tLeaderNum.setTextColor(Color.parseColor("#66ffffff"));
        if (type.equals("personlist"))tPersonNum.setTextColor(Color.parseColor("#ffffff"));
        if (type.equals("speciallist"))tSpecialNum.setTextColor(Color.parseColor("#ffffff"));
        if (type.equals("leaderlist"))tLeaderNum.setTextColor(Color.parseColor("#ffffff"));
        refreshLayout.setRefreshing(true);
        listViewModel.getListData(type);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ly_person_num) {
            type="personlist";
        }
        if (v.getId() == R.id.ly_special_num) {
            type="speciallist";
        }
        if (v.getId() == R.id.ly_leader_num) {
            type="leaderlist";
        }
        refresh(type);
    }

//    private void initNickPicker(final Context context, final String title) {
//        final List<String>Typelist=new ArrayList<>();
//        Typelist.add("全部人员");
//        Typelist.add("带班领导");
//        Typelist.add("特种人员");
//        choseryType.setText(Typelist.get(0));
//        pvNick = new OptionsPickerBuilder(context, new OnOptionsSelectListener() {
//            @Override
//            public void onOptionsSelect(int options1, int options2, int options3, View v) {
//                choseryType.setText(Typelist.get(options1));
//                switch (options1){
//                    case 0:
//                        type="personlist";
//                        break;
//                    case 1:
//                        type="leaderlist";
//                        break;
//                        default:
//                            type="speciallist";
//                            break;
//                }
//
//                refresh(type);
//            }
//
//        })
//                .isDialog(true)
//                .setContentTextSize(18)
//                .setLayoutRes(R.layout.pickerview_custom_nick, new CustomListener() {
//                    @Override
//                    public void customLayout(View v) {
//                        final TextView tvTitle = (TextView) v.findViewById(R.id.nick_title);
//                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
//                        final TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
//                        tvTitle.setText(title);
//                        tvSubmit.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                pvNick.returnData();
//                                pvNick.dismiss();
//                            }
//                        });
//                        ivCancel.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                pvNick.dismiss();
//                            }
//                        });
//                    }
//                })
//                .setTextColorCenter(getResources().getColor(R.color.blue))
//                .setItemVisibleCount(9)
//                .setLineSpacingMultiplier((float) 2.6)
//                .setBackgroundId(R.color.black)
//                .build();
//
//        pvNick.setPicker(Typelist);//添加数据源
//
//    }
}