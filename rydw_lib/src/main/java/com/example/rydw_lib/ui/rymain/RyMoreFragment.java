package com.example.rydw_lib.ui.rymain;


import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.fragment.BaseFragment;
import com.example.rydw_lib.R;
import com.example.rydw_lib.adapter.RyMoreAdapter;
import com.example.rydw_lib.bean.RyMoreBean;

/**
 * A simple {@link Fragment} subclass.
 */
public class RyMoreFragment extends BaseFragment {
    private TextView textRymoreCount;
    private SwipeRefreshLayout ryMoreSrl;
    private RecyclerView ryMoreRcy;
    private RyMoreAdapter ryMoreAdapter;
    private MainViewModel mainViewModel;
    private String type;

    public RyMoreFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        mainViewModel =
                ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }
    @Override
    protected void initData() {
        Bundle bundle =getArguments();
        assert bundle != null;
        type = bundle.getString("type");

        View empty = LayoutInflater.from(getContext()).inflate(R.layout.empty_item, null, false);
        ryMoreAdapter.setEmptyView(empty);
        final TextView textView = empty.findViewById(R.id.txt_errmsg);
        final TextView msg=empty.findViewById(R.id.text_empty);
        final ImageView imageView=empty.findViewById(R.id.empty_img);
        final ConstraintLayout errlay =empty.findViewById(R.id.err_lay);
        errlay.setVisibility(View.INVISIBLE);
        empty.findViewById(R.id.btn_rest).setVisibility(View.INVISIBLE);
        mainViewModel.getMoreData().observe(this, new Observer<RyMoreBean>() {
            @Override
            public void onChanged(RyMoreBean alarmBeans) {
                if(alarmBeans.getData()!=null){
                    if (type.equals("personlist")){
                        textRymoreCount.setText("井下人员共"+alarmBeans.getData().size()+"人");
                    }
                    if (type.equals("leaderlist")){
                        textRymoreCount.setText("井下跟班领导共"+alarmBeans.getData().size()+"人");
                    }
                    if (type.equals("speciallist")){
                        textRymoreCount.setText("井下特种人员共"+alarmBeans.getData().size()+"人");
                    }
                    ryMoreAdapter.setNewData(alarmBeans.getData());
                }else {
                    errlay.setVisibility(View.VISIBLE);
                    textView.setVisibility(View.INVISIBLE);
                    imageView.setVisibility(View.INVISIBLE);
                    msg.setText("暂无数据");
                }

            }
        });
        mainViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)){
                    errlay.setVisibility(View.VISIBLE);
                    textView.setText(s);
                    Toast.makeText(getActivity().getApplicationContext(),"获取数据失败",Toast.LENGTH_LONG).show();
                }
                ryMoreSrl.setRefreshing(false);
            }

        });
    }

    private void refresh(String type) {
        ryMoreSrl.setRefreshing(true);
        mainViewModel.getRyMoreData(type);
    }

    @Override
    public void onStart() {
        super.onStart();
        ryMoreRcy.postDelayed(new Runnable() {
            @Override
            public void run() {
                refresh(type);
            }
        },500);
    }

    @Override
    protected void initView(View view) {
        textRymoreCount = (TextView) view.findViewById(R.id.text_rymore_count);
        ryMoreSrl = (SwipeRefreshLayout) view.findViewById(R.id.ry_more_srl);
        ryMoreSrl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh(type);
            }
        });
        ryMoreRcy = (RecyclerView) view.findViewById(R.id.ry_more_rcy);
        view.findViewById(R.id.ry_more_b).setBackground(new WaterMarkBg(mainViewModel.userInfo.getUseridname()+"-"+mainViewModel.userInfo.getUsername()));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ryMoreRcy.setLayoutManager(layoutManager);
        ryMoreAdapter = new RyMoreAdapter(R.layout.ry_more_item);
        ryMoreRcy.setAdapter(ryMoreAdapter);
        ryMoreAdapter.bindToRecyclerView(ryMoreRcy);
        ryMoreAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                RyMoreBean.DataBean item = (RyMoreBean.DataBean) adapter.getItem(position);
                View viewByPosition = adapter.getViewByPosition(position, R.id.ry_department_ly);
                View img=adapter.getViewByPosition(position,R.id.img_ryshow);
                if(!item.flag){
                    item.flag=true;
                    img.setRotation(0);
                    viewByPosition.setVisibility(View.VISIBLE);
                }else{
                    item.flag=false;
                    img.setRotation(180);
                    viewByPosition.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_ry_more;
    }

}
