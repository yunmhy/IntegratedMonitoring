package com.example.rydw_lib.ui.rymain;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;


import com.chad.library.adapter.base.BaseQuickAdapter;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.fragment.BaseFragment;
import com.example.rydw_lib.R;
import com.example.rydw_lib.RydwMainActivity;
import com.example.rydw_lib.adapter.RegionAdapter;
import com.example.rydw_lib.adapter.RyMainAdapter;
import com.example.rydw_lib.bean.MyRyMainData;
import com.example.rydw_lib.bean.RegionData;
import com.example.rydw_lib.bean.RydwMainBean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainFragment extends BaseFragment {

    private SwipeRefreshLayout refreshLayout;
    private MainViewModel mainViewModel;
    private RyMainAdapter ryMainAdapter;
    private RecyclerView alarmRcy;
    private UserInfo userInfo;
    private TextView txt_nickname;
    private Timer timer;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userInfo = ((RydwMainActivity) context).toValue();
    }

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        mainViewModel =
                ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }

    @Override
    protected void initData() {
        final View empty = LayoutInflater.from(getContext()).inflate(R.layout.empty_item, null, false);
        ryMainAdapter.setEmptyView(empty);
        final TextView textView = empty.findViewById(R.id.txt_errmsg);
        final ConstraintLayout errlay = empty.findViewById(R.id.err_lay);
        errlay.setVisibility(View.INVISIBLE);
        final Button btn_rest = empty.findViewById(R.id.btn_rest);
        final TextView msg = empty.findViewById(R.id.text_empty);
        final ImageView imageView = empty.findViewById(R.id.empty_img);
        btn_rest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refresh();
            }
        });
        txt_nickname.setText(userInfo.getUnitinfo().get(0).getNickname());
        mainViewModel.set(userInfo, this.getActivity());
        mainViewModel.getData().observe(this, new Observer<MyRyMainData>() {
            @Override
            public void onChanged(MyRyMainData ryMainData) {
                if (ryMainData.getRegionDataList().size()>0||ryMainData.getMainBeanList()!= null) {
                    List<MyRyMainData> list = new ArrayList<>();
                    list.add(ryMainData);
                    ryMainAdapter.setNewData(list);
                } else {
                    errlay.setVisibility(View.VISIBLE);
                    btn_rest.setVisibility(View.INVISIBLE);
                    textView.setVisibility(View.INVISIBLE);
                    imageView.setVisibility(View.INVISIBLE);
                    msg.setText("暂无数据");
                }

            }
        });
        mainViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)) {
                    errlay.setVisibility(View.VISIBLE);
                    textView.setText(s);
                    Toast.makeText(getActivity().getApplicationContext(), "获取数据失败", Toast.LENGTH_LONG).show();
                }
                refreshLayout.setRefreshing(false);
            }

        });

    }

    @Override
    public void onStart() {
        super.onStart();
        timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                refresh();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        refreshLayout.setRefreshing(true);
                    }
                });
            }
        },500,30000);
    }

    @Override
    protected void initView(View view) {

        RydwMainActivity activity = (RydwMainActivity) getActivity();
        activity.initIcon();
        refreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.mswipeRefreshLayout);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        alarmRcy = (RecyclerView) view.findViewById(R.id.alarm_rcy);
        alarmRcy.setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));
        txt_nickname = view.findViewById(R.id.mian_nick_name);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        alarmRcy.setLayoutManager(layoutManager);
        ryMainAdapter = new RyMainAdapter(R.layout.ryalarm_itm);
        alarmRcy.setAdapter(ryMainAdapter);
        ryMainAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                Bundle bundle = new Bundle();
                if (view.getId() == R.id.leadernum_ly) {
                    bundle.putString("type", "leaderlist");
                    Navigation.findNavController(view).navigate(R.id.action_navigation_ry_alarm_to_ry_more, bundle);
                }
                if (view.getId() == R.id.realtimenum_ly) {
                    bundle.putString("type", "personlist");
                    Navigation.findNavController(view).navigate(R.id.action_navigation_ry_alarm_to_ry_more, bundle);
                }
                if (view.getId() == R.id.specianum_ly) {
                    bundle.putString("type", "speciallist");
                    Navigation.findNavController(view).navigate(R.id.action_navigation_ry_alarm_to_ry_more, bundle);
                }
                if (view.getId() == R.id.overminenum_ly) {
                    bundle.putInt("type", 2);
                    Navigation.findNavController(view).navigate(R.id.action_ry_navigation_main_to_navigation_ry_mian_alarm, bundle);
                }
                if (view.getId() == R.id.overtimenum_ly) {
                    bundle.putInt("type", 3);
                    Navigation.findNavController(view).navigate(R.id.action_ry_navigation_main_to_navigation_ry_mian_alarm, bundle);
                }
                if (view.getId() == R.id.sysalarmnum_ly) {
                    bundle.putInt("type", 4);
                    Navigation.findNavController(view).navigate(R.id.action_ry_navigation_main_to_navigation_ry_mian_alarm, bundle);
                }
                if (view.getId() == R.id.specialalarmnum_ly) {
                    bundle.putInt("type", 5);
                    Navigation.findNavController(view).navigate(R.id.action_ry_navigation_main_to_navigation_ry_mian_alarm, bundle);
                }
                if (view.getId() == R.id.callnum_ly) {
                    bundle.putInt("type", 1);
                    Navigation.findNavController(view).navigate(R.id.action_ry_navigation_main_to_navigation_ry_mian_alarm, bundle);
                }
            }
        });
    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_main_ry;
    }

    private void refresh() {
//        refreshLayout.setRefreshing(true);
        mainViewModel.getMainData();

    }

    @Override
    public void onStop() {
        super.onStop();
        timer.cancel();
    }
}