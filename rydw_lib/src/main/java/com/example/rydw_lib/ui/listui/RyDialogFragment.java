package com.example.rydw_lib.ui.listui;

import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.example.base_lib.fragment.BaseDialogFragment;
import com.example.rydw_lib.R;
import com.example.rydw_lib.adapter.RyLocalAdapter;
import com.example.rydw_lib.bean.RydwListBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class RyDialogFragment extends BaseDialogFragment {


    private TextView ryName1;
    private TextView ryType;
    private RecyclerView recyclerView;
    RydwListBean.DataBean locusList;
    private List<RydwListBean.DataBean.Locus> asList;
    private RyLocalAdapter ryLocalAdapter;

    public RyDialogFragment(RydwListBean.DataBean list) {
        this.locusList=list;
        super.touchOutside=true;
    }

    @Override
    protected void initData() {
        ryName1.setText(locusList.getPersonName());
        ryType.setText(locusList.getInOutMineFlag());

//        asList = locusList.getLocusList();
        ryLocalAdapter = new RyLocalAdapter(R.layout.local_item,asList);
        recyclerView.setAdapter(ryLocalAdapter);
    }

    @Override
    protected void initView(View roomView) {

        ryName1 = (TextView) roomView.findViewById(R.id.ry_name1);
        ryType = (TextView) roomView.findViewById(R.id.ry_type);
        recyclerView=(RecyclerView) roomView.findViewById(R.id.local_rcy);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected int getContentLayout() {
        return R.layout.dialog_more;
    }

    @Override
    protected void setStyle(int gravity) {
        super.setStyle(Gravity.CENTER);
    }
}
