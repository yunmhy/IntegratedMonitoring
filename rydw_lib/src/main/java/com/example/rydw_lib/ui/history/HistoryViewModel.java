package com.example.rydw_lib.ui.history;

import android.app.Activity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.rydw_lib.bean.RegionByTypeData;
import com.example.rydw_lib.bean.RegionData;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

import java.util.List;

public class HistoryViewModel extends ViewModel {

    private MutableLiveData<RegionByTypeData> plivedata=new MutableLiveData<>();
    private MutableLiveData<RegionData> regionlivedata=new MutableLiveData<>();
    UserInfo userInfo;
    Activity activity;
    private MutableLiveData<String> isErr = new MutableLiveData<>();
    public void set(UserInfo userInfo, Activity activity) {
        this.userInfo = userInfo;
        this.activity = activity;
    }
    public HistoryViewModel() {

    }
    public void getData() {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/person/personbyregiontype")
                .cls(RegionByTypeData.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .dialogshow(true)
                .callback(new MyCallback<RegionByTypeData>() {
                    @Override
                    public void onSuccess(RegionByTypeData regionData, String code) {
//                        isErr.postValue(code);
                        plivedata.postValue(regionData);
                        if(regionData.getData().size()>0){
                            getRegionData();
                        }

                    }
                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }

                }).build();
    }
    public void getRegionData() {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/person/personbyregion")
                .cls(RegionData.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .dialogshow(false)
                .callback(new MyCallback<RegionData>() {
                    @Override
                    public void onSuccess(RegionData regionData, String code) {
                        isErr.postValue(code);
                        regionlivedata.postValue(regionData);
                    }

                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }

                }).build();
    }

    public LiveData<RegionByTypeData> getpiedata(){
        return plivedata;
    }
    public LiveData<RegionData>getRegion(){return regionlivedata;}
    public LiveData<String> getErr() {
        return isErr;
    }
}