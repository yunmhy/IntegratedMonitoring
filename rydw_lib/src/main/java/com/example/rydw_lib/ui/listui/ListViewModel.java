package com.example.rydw_lib.ui.listui;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.rydw_lib.bean.RyMoreBean;
import com.example.rydw_lib.bean.RydwListBean;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

public class ListViewModel extends ViewModel {

    private MutableLiveData<String> isErr = new MutableLiveData<>();
    private MutableLiveData<RyMoreBean> mutableLiveData = new MutableLiveData<>();
    private Application application;
    UserInfo userInfo;
    Activity activity;

    public ListViewModel() {

    }

    public MutableLiveData<RyMoreBean> getData() {
        return mutableLiveData;
    }


    public void getListData(String type) {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/person/"+type)
                .cls(RyMoreBean.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .dialogshow(false)
                .callback(new MyCallback<RyMoreBean>() {
                    @Override
                    public void onSuccess(RyMoreBean alarmBean, String code) {
                        isErr.postValue(code);

                        mutableLiveData.postValue(alarmBean);
                    }

                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }

                }).build();

    }


    public LiveData<String> getErr() {
        return isErr;
    }

    public void set(UserInfo userInfo, Activity activity) {
        this.userInfo = userInfo;
        this.activity =activity;
    }
}