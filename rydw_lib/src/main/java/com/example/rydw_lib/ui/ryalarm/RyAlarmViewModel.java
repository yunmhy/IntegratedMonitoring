package com.example.rydw_lib.ui.ryalarm;

import android.app.Activity;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.rydw_lib.bean.RyAlarmBean;
import com.example.rydw_lib.bean.RyMoreBean;
import com.example.rydw_lib.bean.RydwMainBean;
import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

public class RyAlarmViewModel extends ViewModel {
    private MutableLiveData<String> isErr = new MutableLiveData<>();
    private MutableLiveData<RyAlarmBean> alarmLiveData = new MutableLiveData<>();
    UserInfo userInfo;
    Activity activity;

    public void set(UserInfo userInfo, Activity activity) {
        this.userInfo = userInfo;
        this.activity = activity;
    }

    public void getRyAlalrmData() {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/person/alarms")
                .cls(RyAlarmBean.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .dialogshow(false)
                .callback(new MyCallback<RyAlarmBean>() {
                    @Override
                    public void onSuccess(RyAlarmBean alarmBean, String code) {
                        isErr.postValue(code);
                        alarmLiveData.postValue(alarmBean);
                    }

                    @Override
                    public void onError(String e, String code) {

                        isErr.postValue(e);
                    }

                }).build();
    }

    public LiveData<RyAlarmBean> getAlarmData() {
        return alarmLiveData;
    }

    public LiveData<String> getErr() {
        return isErr;
    }
}
