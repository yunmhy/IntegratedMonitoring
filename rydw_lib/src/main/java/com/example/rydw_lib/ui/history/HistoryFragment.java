package com.example.rydw_lib.ui.history;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.fragment.BaseFragment;
import com.example.rydw_lib.R;
import com.example.rydw_lib.RydwMainActivity;
import com.example.rydw_lib.adapter.RegionAdapter;
import com.example.rydw_lib.bean.RegionByTypeData;
import com.example.rydw_lib.bean.RegionData;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class HistoryFragment extends BaseFragment {

    private HistoryViewModel historyViewModel;
    private PieChart ryPiechart;
    private UserInfo userInfo;
    private RecyclerView recyclerView;
    private RegionAdapter moreAdaptr;
    private ConstraintLayout ecly;

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        historyViewModel =
                ViewModelProviders.of(this).get(HistoryViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userInfo = ((RydwMainActivity) context).toValue();
    }

    @Override
    protected void initData() {
        historyViewModel.set(userInfo, getActivity());

        historyViewModel.getpiedata().observe(this, new Observer<RegionByTypeData>() {
            @Override
            public void onChanged(RegionByTypeData regionData) {
                if(regionData.getData().size()>0){
                    ecly.setVisibility(View.GONE);
                    setData(regionData);
                }else {
                    ecly.setVisibility(View.VISIBLE);
                }

            }
        });
        historyViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (s.equals(MyConstants.FAILED)) {
                    Toast.makeText(getContext(), s, Toast.LENGTH_SHORT).show();
                }

            }
        });
        historyViewModel.getRegion().observe(this, new Observer<RegionData>() {
            @Override
            public void onChanged(RegionData regionData) {
                List<List<RegionData.DataBean>> lists = new ArrayList<>();
                Iterator<RegionData.DataBean> iterator = regionData.getData().iterator();
                List<RegionData.DataBean> list1 = new ArrayList<>();
                List<RegionData.DataBean> list2 = new ArrayList<>();
                List<RegionData.DataBean> list3 = new ArrayList<>();
                List<RegionData.DataBean> list4 = new ArrayList<>();
                while (iterator.hasNext()) {
                    RegionData.DataBean bean = iterator.next();
                    if (bean.getRegiontype().equals("井口区域")) {
                        list1.add(bean);
                        continue;
                    }
                    if (bean.getRegiontype().equals("重点区域")) {
                        list2.add(bean);
                        continue;
                    }
                    if (bean.getRegiontype().equals("限制区域")) {
                        list3.add(bean);
                        continue;
                    }
                    if (bean.getRegiontype().equals("其它区域")) {
                        list4.add(bean);
                        continue;
                    }
                }

                if (list1.size() > 0)
                    lists.add(list1);
                if (list2.size() > 0)
                    lists.add(list2);
                if (list3.size() > 0)
                    lists.add(list3);
                if (list4.size() > 0)
                    lists.add(list4);

                moreAdaptr.setNewData(lists);
            }
        });
    }

    @Override
    protected void initView(View view) {

        ryPiechart = (PieChart) view.findViewById(R.id.ry_piechart);
        ryPiechart.setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));
        ecly = view.findViewById(R.id.emty_cly);
        initPieChart();
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView = (RecyclerView) view.findViewById(R.id.history_region_rcy);
        recyclerView.setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));
        recyclerView.setLayoutManager(layoutManager);
        moreAdaptr = new RegionAdapter(R.layout.rybyregion_item);
        recyclerView.setAdapter(moreAdaptr);
    }

    @Override
    public void onStart() {
        super.onStart();
        ryPiechart.postDelayed(new Runnable() {
            @Override
            public void run() {
                historyViewModel.getData();
            }
        }, 500);
    }

    private void initPieChart() {
// 设置 pieChart 图表基本属性
        ryPiechart.setUsePercentValues(true);            //使用百分比显示
        ryPiechart.getDescription().setEnabled(false);    //设置pieChart图表的描述
        ryPiechart.setBackgroundColor(Color.WHITE);      //设置pieChart图表背景色
        ryPiechart.setExtraOffsets(35, 25, 40, 10);        //设置pieChart图表上下左右的偏移，类似于外边距
        ryPiechart.setDragDecelerationFrictionCoef(0.8f);//设置pieChart图表转动阻力摩擦系数[0,1]
        ryPiechart.setRotationAngle(0);                   //设置pieChart图表起始角度
        ryPiechart.setRotationEnabled(true);              //设置pieChart图表是否可以手动旋转
        ryPiechart.setHighlightPerTapEnabled(true);       //设置piecahrt图表点击Item高亮是否可用
        ryPiechart.animateY(1400, Easing.EasingOption.EaseInOutQuad);// 设置pieChart图表展示动画效果

// 设置 pieChart 图表Item文本属性
        ryPiechart.setDrawEntryLabels(false);              //设置pieChart是否只显示饼图上百分比不显示文字（true：下面属性才有效果）
        ryPiechart.setEntryLabelColor(Color.WHITE);       //设置pieChart图表文本字体颜色
//        ryPiechart.setEntryLabelTypeface(mTfRegular);     //设置pieChart图表文本字体样式
        ryPiechart.setEntryLabelTextSize(16f);            //设置pieChart图表文本字体大小

// 设置 pieChart 内部圆环属性
        ryPiechart.setDrawHoleEnabled(true);              //是否显示PieChart内部圆环(true:下面属性才有意义)
        ryPiechart.setHoleRadius(60f);                    //设置PieChart内部圆的半径(这里设置28.0f)
        ryPiechart.setTransparentCircleRadius(63f);       //设置PieChart内部透明圆的半径(这里设置31.0f)
        ryPiechart.setTransparentCircleColor(Color.WHITE);//设置PieChart内部透明圆与内部圆间距(31f-28f)填充颜色
        ryPiechart.setTransparentCircleAlpha(50);         //设置PieChart内部透明圆与内部圆间距(31f-28f)透明度[0~255]数值越小越透明
        ryPiechart.setHoleColor(Color.WHITE);             //设置PieChart内部圆的颜色
        ryPiechart.setDrawCenterText(true);               //是否绘制PieChart内部中心文本（true：下面属性才有意义）
//        ryPiechart.setCenterTextTypeface(mTfLight);       //设置PieChart内部圆文字的字体样式
//        ryPiechart.setCenterText("Test");                 //设置PieChart内部圆文字的内容
//        ryPiechart.setCenterTextSize(10f);                //设置PieChart内部圆文字的大小
//        ryPiechart.setCenterTextColor(Color.RED);         //设置PieChart内部圆文字的颜色

    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_history_ry;
    }


    private void setData(final RegionByTypeData regionData) {
        final ArrayList<PieEntry> pieEntryList = new ArrayList<PieEntry>();
        ArrayList<Integer> colors = new ArrayList<Integer>();
        colors.add(Color.parseColor("#fe464d"));
        colors.add(Color.parseColor("#ffae36"));
        colors.add(Color.parseColor("#0ab3ee"));
        colors.add(Color.parseColor("#48c099"));
        int count = 0;
        for (RegionByTypeData.DataBean region : regionData.getData()) {
            count = +region.getRegioncount();
        }

        //饼图实体 PieEntry
        for (RegionByTypeData.DataBean dataBean : regionData.getData()) {
            float f = (float) dataBean.getRegioncount() / count;
            // 参数1为 value，参数2为 data。
            // 如 PieEntry(0.15F, "90分以上");  表示90分以上的人占比15%。
            PieEntry pieEntry = new PieEntry(f, dataBean.getRegiontype() + "\n" + dataBean.getRegioncount());
//            pieEntry.setX("float类型数字");
            pieEntryList.add(pieEntry);
        }

        PieDataSet pieDataSet = new PieDataSet(pieEntryList, "");

        pieDataSet.setSliceSpace(3f);           //设置饼状Item之间的间隙
        pieDataSet.setSelectionShift(10f);      //设置饼状Item被选中时变化的距离
        pieDataSet.setColors(colors);           //为DataSet中的数据匹配上颜色集(饼图Item颜色)
        //数据连接线距图形片内部边界的距离，为百分数
//        pieDataSet.setValueLinePart1OffsetPercentage(80f);
        //设置连接线的颜色
        pieDataSet.setValueLineColor(Color.BLACK);
        pieDataSet.setValueLinePart1Length(0.2f);
        pieDataSet.setValueLinePart2Length(0.2f);
        pieDataSet.setValueLineVariableLength(false);
//        // 连接线在饼状图外面
        pieDataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
//        ryPiechart.setExtraOffsets(50, 15, 50, 15);

        //最终数据 PieData
        PieData pieData = new PieData(pieDataSet);

        pieData.setDrawValues(true);            //设置是否显示数据实体(百分比，true:以下属性才有意义)
        pieData.setValueFormatter(new IValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                PieEntry pieEntry = (PieEntry) entry;
                return pieEntry.getLabel();
            }
        });
        pieData.setValueTextColor(Color.parseColor("#222222"));  //设置所有DataSet内数据实体（百分比）的文本颜色
        pieData.setValueTextSize(12f);          //设置所有DataSet内数据实体（百分比）的文本字体大小
//        pieData.setValueTypeface(mTfLight);     //设置所有DataSet内数据实体（百分比）的文本字体样式
//        pieData.setValueFormatter(new );//设置所有DataSet内数据实体（百分比）的文本字体格式
        ryPiechart.setData(pieData);
        ryPiechart.highlightValues(null);
        ryPiechart.invalidate();

        // 获取pieCahrt图列
        Legend l = ryPiechart.getLegend();
        l.setEnabled(true);                    //是否启用图列（true：下面属性才有意义）

    }


}