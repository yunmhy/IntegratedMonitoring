package com.example.rydw_lib.ui.rymain;


import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.fragment.BaseFragment;
import com.example.rydw_lib.R;
import com.example.rydw_lib.adapter.RyMainAlarmAdapter;
import com.example.rydw_lib.bean.RyAlarmBean;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainAlarmFragment extends BaseFragment {
    private MainViewModel mainViewModel;
    private SwipeRefreshLayout ryMalarmSrl;
    private RecyclerView ryMalarmRcy;
    private RyMainAlarmAdapter ryAlarmAdapter;
    private int type;
    private TextView textRymalarmCount;
    public MainAlarmFragment() {
        // Required empty public constructor
    }
    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        mainViewModel =
                ViewModelProviders.of(getActivity()).get(MainViewModel.class);
        super.initBeforeCreateView(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        ryMalarmRcy.postDelayed(new Runnable() {
            @Override
            public void run() {
                //这个方法是让一进入页面的时候实现网络请求，有个缓冲的效果
//                refreshLayout.setRefreshing(true);

                refresh();
            }
        }, 500);

    }

    @Override
    protected void initData() {
        View empty = LayoutInflater.from(getContext()).inflate(R.layout.empty_item, null, false);
        ryAlarmAdapter.setEmptyView(empty);
        final TextView textView = empty.findViewById(R.id.txt_errmsg);
        final ConstraintLayout errlay =empty.findViewById(R.id.err_lay);
        final TextView msg=empty.findViewById(R.id.text_empty);
        final ImageView imageView=empty.findViewById(R.id.empty_img);
        errlay.setVisibility(View.INVISIBLE);
        final Button btn_rest=empty.findViewById(R.id.btn_rest);
        btn_rest.setVisibility(View.INVISIBLE);
        ryMalarmRcy.setAdapter(ryAlarmAdapter);
        mainViewModel.getAlarmData().observe(this, new Observer<RyAlarmBean>() {
            @Override
            public void onChanged(RyAlarmBean alarmBeans) {
                if(alarmBeans.getData()!=null){
                    switch (type){
                        case 1:
                            if (alarmBeans.getData().getPersoncallalarm().size()>0){
                                ryAlarmAdapter.setNewData(alarmBeans.getData().getPersoncallalarm());
                            }else {

                                showempty();
                            }

                            break;
                        case 2:

                            if (alarmBeans.getData().getOverpersonalarm().size()>0){
                                ryAlarmAdapter.setNewData(alarmBeans.getData().getOverpersonalarm());
                            }else {
                                showempty();
                            }

                            break;
                        case 3:
                            if (alarmBeans.getData().getOvertimealarm().size()>0){
                                ryAlarmAdapter.setNewData(alarmBeans.getData().getOvertimealarm());
                            }else {
                                showempty();
                            }

                            break;
                        case 4:
                            if (alarmBeans.getData().getSysalarm().size()>0){
                                ryAlarmAdapter.setNewData(alarmBeans.getData().getSysalarm());
                            }else {
                                showempty();
                            }

                            break;
                        case 5:
                            if (alarmBeans.getData().getSpecialpersonabnorma().size()>0){
                                ryAlarmAdapter.setNewData(alarmBeans.getData().getSpecialpersonabnorma());
                            }else {
                                showempty();
                            }

                            break;
                        default:
                            break;
                    }

                }else {
                    showempty();
                }

            }

            private void showempty() {
                errlay.setVisibility(View.VISIBLE);
                btn_rest.setVisibility(View.INVISIBLE);
                textView.setVisibility(View.INVISIBLE);
                imageView.setVisibility(View.INVISIBLE);
                msg.setText("暂无数据");
            }
        });
        mainViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)){
                    errlay.setVisibility(View.VISIBLE);
                    textView.setText(s);
                    Toast.makeText(getActivity().getApplicationContext(),"获取数据失败",Toast.LENGTH_LONG).show();
                }
                ryMalarmSrl.setRefreshing(false);
            }

        });

    }
    private void refresh() {
        ryMalarmSrl.setRefreshing(true);
        mainViewModel.getRyMalalrmData();
    }
    @Override
    protected void initView(View view) {
        ryMalarmSrl = (SwipeRefreshLayout) view.findViewById(R.id.ry_malarm_srl);
        ryMalarmSrl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        ryMalarmRcy = (RecyclerView) view.findViewById(R.id.ry_malarm_rcy);
        ryMalarmRcy.setBackground(new WaterMarkBg(mainViewModel.userInfo.getUseridname()+"-"+mainViewModel.userInfo.getUsername()));
        textRymalarmCount = (TextView) view.findViewById(R.id.text_rymalarm_count);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ryMalarmRcy.setLayoutManager(layoutManager);
        Bundle bundle =getArguments();
        assert bundle != null;
        type = bundle.getInt("type");
        switch (type){
            case 1:
                textRymalarmCount.setText("人员呼救");
                ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.personalarm_item);
                break;
            case 2:
                textRymalarmCount.setText("人员超员");
                ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.overpersonalarm_item);
                break;
            case 3:
                textRymalarmCount.setText("人员超时");
                ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.overtimealarm_item);
                break;
            case 4:
                textRymalarmCount.setText("系统异常");
                ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.sysalarm_item);
                break;
            case 5:
                textRymalarmCount.setText("特种人员异常");
                ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.specialperson_item);
                break;
            default:
                break;
        }

    }

    @Override
    protected int contentLayout() {
        return R.layout.fragment_main_alarm;
    }

}
