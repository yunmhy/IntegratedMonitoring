package com.example.rydw_lib.ui.ryalarm;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bigkoo.pickerview.builder.OptionsPickerBuilder;
import com.bigkoo.pickerview.listener.CustomListener;
import com.bigkoo.pickerview.listener.OnOptionsSelectListener;
import com.bigkoo.pickerview.view.OptionsPickerView;
import com.example.base_lib.activity.WaterMarkBg;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.base_lib.fragment.BaseFragment;
import com.example.rydw_lib.R;
import com.example.rydw_lib.RydwMainActivity;
import com.example.rydw_lib.adapter.RyMainAlarmAdapter;
import com.example.rydw_lib.bean.RyAlarmBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class RyAlarmFragment extends BaseFragment implements View.OnClickListener {

    private RyAlarmViewModel mViewModel;
    private LinearLayout textAlarmType;
    private SwipeRefreshLayout ryAlarmSrl;
    private RecyclerView ryAlarmRcy;
    private UserInfo userInfo;
    private RyMainAlarmAdapter ryAlarmAdapter;
    private RyAlarmBean dataBean;
    private int type = 0;
    private View empty;
    private ConstraintLayout errlay;
    private TextView textView;
    //    private OptionsPickerView pvNick;
    private Timer timer;
    private TextView tOvertime;
    private TextView tOverperson;
    private TextView tCall;
    private TextView tSpecial;
    private TextView tSysalarm;
    private TextView msg;
    private ImageView imageView;

    @Override
    protected void initBeforeCreateView(Bundle savedInstanceState) {
        super.initBeforeCreateView(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RyAlarmViewModel.class);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        userInfo = ((RydwMainActivity) context).toValue();
    }

    public RyAlarmFragment() {
    }

    @Override
    protected void initData() {
        mViewModel.set(userInfo, getActivity());
        mViewModel.getAlarmData().observe(this, new Observer<RyAlarmBean>() {
            @Override
            public void onChanged(RyAlarmBean alarmBeans) {
                dataBean = alarmBeans;
                setData(type);


            }
        });
        mViewModel.getErr().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                if (!s.equals(MyConstants.SUCCESS)) {
                    errlay.setVisibility(View.VISIBLE);
                    textView.setText(s);
                    Toast.makeText(getActivity().getApplicationContext(), "获取数据失败", Toast.LENGTH_LONG).show();
                }
                ryAlarmSrl.setRefreshing(false);
            }

        });
    }


    @Override
    public void onStart() {
        super.onStart();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                refresh();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ryAlarmSrl.setRefreshing(true);
                    }
                });
            }
        }, 500, 30000);
    }

    private void setData(int type) {
        tCall.setTextColor(Color.parseColor("#66ffffff"));
        tOverperson.setTextColor(Color.parseColor("#66ffffff"));
        tOvertime.setTextColor(Color.parseColor("#66ffffff"));
        tSpecial.setTextColor(Color.parseColor("#66ffffff"));
        tSysalarm.setTextColor(Color.parseColor("#66ffffff"));

        if (dataBean.getData() != null) {
            RyAlarmBean.DataBean data = dataBean.getData();
            switch (type) {
                case 0:
                    ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.overtimealarm_item);
                    ryAlarmAdapter.setNewData(data.getOvertimealarm());
                    tOvertime.setTextColor(Color.parseColor("#ffffff"));
                    break;
                case 1:
                    ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.overpersonalarm_item);
                    ryAlarmAdapter.setNewData(data.getOverpersonalarm());
                    tOverperson.setTextColor(Color.parseColor("#ffffff"));
                    break;
                case 2:
                    tCall.setTextColor(Color.parseColor("#ffffff"));
                    ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.personalarm_item);
                    ryAlarmAdapter.setNewData(data.getPersoncallalarm());
                    break;
                case 3:
                    tSpecial.setTextColor(Color.parseColor("#ffffff"));
                    ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.specialperson_item);
                    ryAlarmAdapter.setNewData(data.getSpecialpersonabnorma());
                    break;
                case 4:
                    tSysalarm.setTextColor(Color.parseColor("#ffffff"));
                    ryAlarmAdapter = new RyMainAlarmAdapter(R.layout.sysalarm_item);
                    ryAlarmAdapter.setNewData(data.getSysalarm());
                    break;
                default:
                    break;
            }
        }
        empty = LayoutInflater.from(getContext()).inflate(R.layout.empty_item, null, false);
        textView = empty.findViewById(R.id.txt_errmsg);
        errlay = empty.findViewById(R.id.err_lay);
        msg = empty.findViewById(R.id.text_empty);
        imageView = empty.findViewById(R.id.empty_img);
        errlay.setVisibility(View.VISIBLE);
        final Button btn_rest = empty.findViewById(R.id.btn_rest);
        btn_rest.setVisibility(View.INVISIBLE);
        textView.setVisibility(View.INVISIBLE);
        imageView.setVisibility(View.INVISIBLE);
        msg.setText("暂无数据");
        ryAlarmAdapter.setEmptyView(empty);
        ryAlarmRcy.removeAllViews();
        ryAlarmRcy.setAdapter(ryAlarmAdapter);

    }

    private void refresh() {
//        ryAlarmSrl.setRefreshing(true);
        ryAlarmRcy.postDelayed(new Runnable() {
            @Override
            public void run() {
                mViewModel.getRyAlalrmData();
            }
        },500);

    }

    @Override
    protected void initView(View view) {

        view.findViewById(R.id.ly_overtime).setOnClickListener(this);
        view.findViewById(R.id.ly_overperson).setOnClickListener(this);
        view.findViewById(R.id.ly_call).setOnClickListener(this);
        view.findViewById(R.id.ly_special).setOnClickListener(this);
        view.findViewById(R.id.ly_sysalarm).setOnClickListener(this);

        tOvertime = (TextView) view.findViewById(R.id.t_overtime);
        tOverperson = (TextView) view.findViewById(R.id.t_overperson);
        tCall = (TextView) view.findViewById(R.id.t_call);
        tSpecial = (TextView) view.findViewById(R.id.t_special);
        tSysalarm = (TextView) view.findViewById(R.id.t_sysalarm);
        textAlarmType = (LinearLayout) view.findViewById(R.id.text_alarm_type);
        ryAlarmSrl = (SwipeRefreshLayout) view.findViewById(R.id.ry_alarm_srl);
        ryAlarmSrl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
        ryAlarmRcy = (RecyclerView) view.findViewById(R.id.ry_alarm_rcy);
        ryAlarmRcy.setBackground(new WaterMarkBg(userInfo.getUseridname()+"-"+userInfo.getUsername()));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        ryAlarmRcy.setLayoutManager(layoutManager);
//        initNickPicker(getContext(),"报警类型");
//        textAlarmType.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                pvNick.show();
//            }
//        });
    }

    //    private void initNickPicker(final Context context, final String title) {
//        final List<String>Typelist=new ArrayList<>();
//        Typelist.add("人员呼救");
//        Typelist.add("人员超员");
//        Typelist.add("人员超时");
//        Typelist.add("系统异常");
//        Typelist.add("特种人员报警");
//        choseType.setText(Typelist.get(0));
//        pvNick = new OptionsPickerBuilder(context, new OnOptionsSelectListener() {
//            @Override
//            public void onOptionsSelect(int options1, int options2, int options3, View v) {
//                choseType.setText(Typelist.get(options1));
//                type=options1;
//                setData(type);
//            }
//
//        })
//                .isDialog(true)
//                .setContentTextSize(18)
//                .setLayoutRes(R.layout.pickerview_custom_nick, new CustomListener() {
//                    @Override
//                    public void customLayout(View v) {
//                        final TextView tvTitle = (TextView) v.findViewById(R.id.nick_title);
//                        final TextView tvSubmit = (TextView) v.findViewById(R.id.tv_finish);
//                        final TextView ivCancel = (TextView) v.findViewById(R.id.iv_cancel);
//                        tvTitle.setText(title);
//                        tvSubmit.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                pvNick.returnData();
//                                pvNick.dismiss();
//                            }
//                        });
//                        ivCancel.setOnClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                pvNick.dismiss();
//                            }
//                        });
//                    }
//                })
//                .setTextColorCenter(getResources().getColor(R.color.blue))
//                .setItemVisibleCount(9)
//                .setLineSpacingMultiplier((float) 2.6)
//                .setBackgroundId(R.color.black)
//                .build();
//
//        pvNick.setPicker(Typelist);//添加数据源
//
//    }
    @Override
    protected int contentLayout() {
        return R.layout.ry_alarm_fragment;
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.ly_overtime) {
            type = 0;
        }
        if (v.getId() == R.id.ly_overperson) {
            type = 1;
        }
        if (v.getId() == R.id.ly_call) {
            type = 2;
        }
        if (v.getId() == R.id.ly_special) {
            type = 3;
        }
        if (v.getId() == R.id.ly_sysalarm) {
            type = 4;
        }
        setData(type);
    }

    @Override
    public void onStop() {
        super.onStop();
        timer.cancel();
    }
}
