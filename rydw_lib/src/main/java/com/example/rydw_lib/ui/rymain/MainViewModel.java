package com.example.rydw_lib.ui.rymain;

import android.app.Activity;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.base_lib.MyCallback;
import com.example.base_lib.MyOkGo.OkGoBuilder;
import com.example.base_lib.baen.MyConstants;
import com.example.base_lib.baen.UserInfo;
import com.example.rydw_lib.bean.MyRyMainData;
import com.example.rydw_lib.bean.RegionData;
import com.example.rydw_lib.bean.RyAlarmBean;
import com.example.rydw_lib.bean.RyMoreBean;
import com.example.rydw_lib.bean.RydwMainBean;

import com.lzy.okgo.model.HttpHeaders;
import com.lzy.okgo.model.HttpParams;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private MutableLiveData<RegionData> plivedata=new MutableLiveData<>();
    private MutableLiveData<String> isErr = new MutableLiveData<>();
    private MutableLiveData<MyRyMainData> mutableLiveData = new MutableLiveData<>();
    private MutableLiveData<RyMoreBean> moreLiveData = new MutableLiveData<>();
    private MutableLiveData<RyAlarmBean> alarmLiveData = new MutableLiveData<>();
    private Application application;
    UserInfo userInfo;
    Activity activity;
    private List<RegionData.DataBean> regionlist;
    private RydwMainBean.DataBean mainData;

    public MainViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

    }

    public MutableLiveData<MyRyMainData> getData() {
        return mutableLiveData;
    }




    public void getMainData() {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/person/index")
                .cls(RydwMainBean.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .dialogshow(false)
                .callback(new MyCallback<RydwMainBean>() {
                    @Override
                    public void onSuccess(RydwMainBean alarmBean, String code) {
                        mainData = alarmBean.getData();
                        getRegionData();
                    }

                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }

                }).build();

    }
    public void getRyMoreData( String type){
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/person/"+type)
                .cls(RyMoreBean.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .dialogshow(false)
                .callback(new MyCallback<RyMoreBean>() {
                    @Override
                    public void onSuccess(RyMoreBean alarmBean, String code) {
                        isErr.postValue(code);

                        moreLiveData.postValue(alarmBean);
                    }

                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }

                }).build();
    }
    public void getRyMalalrmData(){
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/person/alarms")
                .cls(RyAlarmBean.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .dialogshow(false)
                .callback(new MyCallback<RyAlarmBean>() {
                    @Override
                    public void onSuccess(RyAlarmBean alarmBean, String code) {
                        isErr.postValue(code);

                        alarmLiveData.postValue(alarmBean);
                    }

                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }

                }).build();
    }
    public LiveData<String> getErr() {
        return isErr;
    }

    public void set(UserInfo userInfo, Activity activity) {
        this.userInfo = userInfo;
        this.activity = activity;
    }

    public LiveData<RyMoreBean> getMoreData() {
            return moreLiveData;
    }
    public LiveData<RyAlarmBean> getAlarmData() {
        return alarmLiveData;
    }
    public LiveData<RegionData> getpiedata(){ return plivedata; }
    public void getRegionData() {
        HttpHeaders headers = new HttpHeaders();
        headers.put("token", userInfo.getToken());
        HttpParams paramsGet = new HttpParams();
        paramsGet.put("unitcode", userInfo.getUnitinfo().get(0).getUnitcode());
        OkGoBuilder.getInstance()
                .Builder(activity)
                .baseurl(MyConstants.TestUrl)
                .inurl("unit/person/personbyregion")
                .cls(RegionData.class)
                .method(OkGoBuilder.GET)
                .params(paramsGet)
                .headers(headers)
                .dialogshow(false)
                .callback(new MyCallback<RegionData>() {
                    @Override
                    public void onSuccess(RegionData regionData, String code) {
                        isErr.postValue(code);
                        regionlist = regionData.getData();
                        setRyMainData();
//                        plivedata.postValue(regionData);
                    }

                    @Override
                    public void onError(String e, String code) {
                        isErr.postValue(e);
                    }

                }).build();
    }

    private void setRyMainData() {

        List<List<RegionData.DataBean>> lists = new ArrayList<>();
        Iterator<RegionData.DataBean> iterator = regionlist.iterator();
        List<RegionData.DataBean> list1 = new ArrayList<>();
        List<RegionData.DataBean> list2 = new ArrayList<>();
        List<RegionData.DataBean> list3 = new ArrayList<>();
        List<RegionData.DataBean> list4 = new ArrayList<>();
        while (iterator.hasNext()) {
            RegionData.DataBean bean = iterator.next();
            if (bean.getRegiontype().equals("井口区域")) {
                list1.add(bean);
                continue;
            }
            if (bean.getRegiontype().equals("重点区域")) {
                list2.add(bean);
                continue;
            }
            if (bean.getRegiontype().equals("限制区域")) {
                list3.add(bean);
                continue;
            }
            if (bean.getRegiontype().equals("其它区域")) {
                list4.add(bean);
                continue;
            }
        }
        if (list1.size() > 0)
            lists.add(list1);
        if (list2.size() > 0)
            lists.add(list2);
        if (list3.size() > 0)
            lists.add(list3);
        if (list4.size() > 0)
            lists.add(list4);
        mutableLiveData.postValue(new MyRyMainData(mainData,lists));
    }
}