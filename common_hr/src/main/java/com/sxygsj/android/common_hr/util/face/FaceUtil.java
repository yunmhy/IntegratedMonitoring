package com.sxygsj.android.common_hr.util.face;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @创建者：王春
 * @创建时间：2019/9/3 17:06
 * @描述：
 */
public class FaceUtil {
    public static String getFaceJson(Context context){
        String json=null;
        try {
            InputStream is = context.getResources().getAssets().open("face.json");
            BufferedReader bufr = new BufferedReader(new InputStreamReader(is));
            String line;
            StringBuilder builder = new StringBuilder();
            while ((line = bufr.readLine()) != null) {
                builder.append(line);
            }
            is.close();
            bufr.close();
            json=builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }
}
