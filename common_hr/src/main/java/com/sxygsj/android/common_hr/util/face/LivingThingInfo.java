package com.sxygsj.android.common_hr.util.face;

/**
 * @创建者：王春
 * @创建时间：2019/8/29 15:43
 * @描述：
 */
public class LivingThingInfo {
    private float score;
    private boolean isLiving;

    public LivingThingInfo() {
    }

    public LivingThingInfo(float score, boolean isLiving) {
        this.score = score;
        this.isLiving = isLiving;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    public boolean isLiving() {
        return isLiving;
    }

    public void setLiving(boolean living) {
        isLiving = living;
    }
}
