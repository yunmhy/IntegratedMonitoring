package com.sxygsj.android.common_hr.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @创建者：王春
 * @创建时间：2019/9/4 10:34
 * @描述：
 */
public class DateUtil {
    public static final String getDateTime(){
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateString = formatter.format(currentTime);
        return dateString;
    }
}
