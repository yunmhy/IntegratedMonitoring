package com.sxygsj.android.common_hr.util.face;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.arcsoft.face.ErrorInfo;
import com.arcsoft.face.FaceEngine;
import com.arcsoft.face.FaceFeature;
import com.arcsoft.face.FaceInfo;
import com.arcsoft.face.FaceSimilar;
import com.arcsoft.face.LivenessInfo;
import com.arcsoft.face.util.ImageUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * @创建者：王春
 * @创建时间：2019/8/29 14:21
 * @描述：
 */
public class HRFaceHelper implements IFace<FaceInfo,FaceFeature> {
    public static final long ASF_DETECT_MODE_IMAGE = 4294967295L;
    public static final long ASF_DETECT_MODE_VIDEO = 0L;
    private FaceEngine mFaceEngine;
    private Context mContext;
    private String appId;
    private String key;
    private static final String TAG="HRFaceHelper";
    private HRFaceHelper(Builder builder) {
        mContext = builder.mContext;
        appId = builder.appId;
        key = builder.key;

    }

    public static final class Builder {
        private Context mContext;
        private String appId;
        private String key;

        public Builder context(Context mContext) {
            this.mContext = mContext;
            return this;
        }

        public Builder appId(String appId) {
            this.appId = appId;
            return this;
        }

        public Builder key(String key) {
            this.key = key;
            return this;
        }

        public HRFaceHelper build() {
            return new HRFaceHelper(this);
        }
    }

    @Override
    public int init(long l) {
        mFaceEngine = new FaceEngine();
        int activeCode = mFaceEngine.activeOnline(mContext.getApplicationContext(), appId, key);
        if (activeCode == ErrorInfo.MOK || activeCode == ErrorInfo.MERR_ASF_ALREADY_ACTIVATED) {
            int faceEngineCode = mFaceEngine.init(mContext.getApplicationContext(), l, FaceEngine.ASF_OP_0_ONLY,
                    16, 10,
                    FaceEngine.ASF_FACE_RECOGNITION |
                            FaceEngine.ASF_FACE_DETECT |
                            FaceEngine.ASF_GENDER |
                            FaceEngine.ASF_FACE3DANGLE |
                            FaceEngine.ASF_LIVENESS);
            return faceEngineCode;
        } else {
            return activeCode;
        }


    }

    @Override
    public List<FaceInfo> findFace(Bitmap bitmap) {
        List<FaceInfo> infos = new ArrayList<>();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        final byte[] bgr24 = ImageUtils.bitmapToBgr24(bitmap);
        int i = mFaceEngine.detectFaces(bgr24, width, height, FaceEngine.CP_PAF_BGR24, infos);//检测是否有人脸
        Log.e(TAG,"=="+i);
        return infos;
    }

    @Override
    public FaceFeature getFaceInfo(Bitmap bitmap,List<FaceInfo> infos) {
        if (infos.size()==0||bitmap==null){return null;}
        FaceFeature faceFeature = new FaceFeature();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        final byte[] bgr24 = ImageUtils.bitmapToBgr24(bitmap);
        int faceFeatureCode = mFaceEngine.extractFaceFeature(bgr24, width, height, FaceEngine.CP_PAF_BGR24, infos.get(0), faceFeature);//获得人脸信息
        if (faceFeatureCode == ErrorInfo.MOK) {
            return faceFeature;
        }else {
            return null;
        }

    }

    @Override
    public float face(FaceFeature mainFaceFeature, FaceFeature faceFeature) {
        FaceSimilar faceSimilar = new FaceSimilar();
        int compareResult = mFaceEngine.compareFaceFeature(mainFaceFeature, faceFeature, faceSimilar);
        if (compareResult == ErrorInfo.MOK) {
            final float score = faceSimilar.getScore();
            return score;
        } else {
            return -1;
        }

    }

    @Override
    public boolean livingThing(Bitmap bitmap,List<FaceInfo> faceInfos) {
        List<LivenessInfo> infos = new ArrayList<>();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        final byte[] bgr24 = ImageUtils.bitmapToBgr24(bitmap);
        int process = mFaceEngine.process(bgr24, width, height, FaceEngine.CP_PAF_BGR24, faceInfos, FaceEngine.ASF_LIVENESS);//活体检测
        if (process == ErrorInfo.MOK) {
            int livenesCode = mFaceEngine.getLiveness(infos);
            if (livenesCode == ErrorInfo.MOK && infos.size() > 0) {
                int liveness = infos.get(0).getLiveness();
                if (liveness == LivenessInfo.ALIVE) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }


    }

    @Override
    public void finish() {
        if (mFaceEngine != null) {
            mFaceEngine.unInit();
        }
    }
}
