package com.sxygsj.android.common_hr.util;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import com.arcsoft.face.FaceInfo;
import com.sxygsj.android.common_hr.view.FaceFrameLayout;
import java.util.List;

/**
 * @创建时间：2019/12/26 10:24
 * @描述：
 */
public class DrawHelper {

    public static void drawFaceRect(Canvas canvas, FaceInfo faceInfo, int faceRectThickness, Paint paint) {
        if (canvas == null || faceInfo == null) {
            return;
        }
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(faceRectThickness);
        paint.setColor(Color.GREEN);
        paint.setAntiAlias(true);
        Path mPath = new Path();
        Rect rect = faceInfo.getRect();
        // 左上

        mPath.moveTo(rect.left, rect.top + rect.height() / 4);
        mPath.lineTo(rect.left, rect.top);
        mPath.lineTo(rect.left + rect.width() / 4, rect.top);
        // 右上
        mPath.moveTo(rect.right - rect.width() / 4, rect.top);
        mPath.lineTo(rect.right, rect.top);
        mPath.lineTo(rect.right, rect.top + rect.height() / 4);
        // 右下
        mPath.moveTo(rect.right, rect.bottom - rect.height() / 4);
        mPath.lineTo(rect.right, rect.bottom);
        mPath.lineTo(rect.right - rect.width() / 4, rect.bottom);
        // 左下
        mPath.moveTo(rect.left + rect.width() / 4, rect.bottom);
        mPath.lineTo(rect.left, rect.bottom);
        mPath.lineTo(rect.left, rect.bottom - rect.height() / 4);
        canvas.drawPath(mPath, paint);


    }

    public static void draw(FaceFrameLayout faceRectView, List<FaceInfo> drawInfoList) {
        if (faceRectView == null) {
            return;
        }
        faceRectView.clearFaceInfo();
        if (drawInfoList == null || drawInfoList.size() == 0) {
            return;
        }
        faceRectView.addFaceInfo(drawInfoList);

    }
}
