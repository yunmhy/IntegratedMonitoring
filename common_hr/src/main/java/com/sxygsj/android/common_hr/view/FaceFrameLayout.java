package com.sxygsj.android.common_hr.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.arcsoft.face.FaceInfo;
import com.sxygsj.android.common_hr.util.DrawHelper;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @创建时间：2019/12/26 10:21
 * @描述：
 */
public class FaceFrameLayout extends View {

    private Paint paint;
    private CopyOnWriteArrayList<FaceInfo> drawInfoList = new CopyOnWriteArrayList<>();
    // 默认人脸框厚度
    private static final int DEFAULT_FACE_RECT_THICKNESS = 6;
    public FaceFrameLayout(@NonNull Context context) {
        super(context);
        paint = new Paint();
    }

    public FaceFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        paint = new Paint();
    }

    public FaceFrameLayout(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        paint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Log.e("TAG","==");
        super.onDraw(canvas);
        if (drawInfoList != null && drawInfoList.size() > 0) {
            for (int i = 0; i < drawInfoList.size(); i++) {
                DrawHelper.drawFaceRect(canvas, drawInfoList.get(i), DEFAULT_FACE_RECT_THICKNESS, paint);
            }
        }
    }

    public void clearFaceInfo() {
        drawInfoList.clear();
        postInvalidate();
    }

    public void addFaceInfo(FaceInfo faceInfo) {
        drawInfoList.add(faceInfo);
        postInvalidate();
    }
    public void addFaceInfo(List<FaceInfo> faceInfoList) {
        drawInfoList.addAll(faceInfoList);
        postInvalidate();
    }
}
