package com.sxygsj.android.common_hr.util.face;

import android.graphics.Bitmap;

import com.arcsoft.face.FaceInfo;

import java.util.List;

/**
 *
 * @param <V> 人脸信息  用来在视频或图片中查找人脸
 * @param <T> 人脸特征  用来对比人脸
 */
public interface IFace<V,T> {


    int init(long l);//准备工作


    List<V> findFace(Bitmap bitmap);//查找人脸

    T getFaceInfo(Bitmap bitmap,List<V> data);//获得人脸信息

    float face(T t,T t1);//人脸对比

    boolean livingThing( Bitmap bitmap,List<V> data);//活体检测

    void finish();//释放资源
}
